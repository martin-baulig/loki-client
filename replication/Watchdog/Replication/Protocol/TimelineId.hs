module Watchdog.Replication.Protocol.TimelineId
    ( TimelineId(..)
    , HasTimelineId(..)
    , timelineIdParser
    , timelineIdFromByteString
    , defaultTimeline
    ) where

import           Baulig.Prelude       hiding (take)

import           Data.Aeson
import           Data.Attoparsec.Text hiding (count)
import           Data.Scientific

------------------------------------------------------------------------------
--- TimelineId
------------------------------------------------------------------------------

newtype TimelineId = TimelineId Word32
    deriving stock (Eq, Generic, Ord, Show)
    deriving newtype Binary

instance Display TimelineId where
    display (TimelineId tli) = mconcat ["[Timeline ", display tli, "]"]

instance Default TimelineId where
    def = TimelineId 1

instance FromJSON TimelineId where
    parseJSON =
        withScientific "TimelineId" $ \value -> case toBoundedInteger value of
            Just tli -> pure $ TimelineId tli
            Nothing -> error $ "Invalid TimelineId: " ++ show value

instance ToJSON TimelineId where
    toJSON (TimelineId tli) = toJSON tli

defaultTimeline :: TimelineId
defaultTimeline = TimelineId 1

timelineIdParser :: Parser TimelineId
timelineIdParser = TimelineId <$> decimal

timelineIdFromByteString :: ByteString -> Either String TimelineId
timelineIdFromByteString = parseOnly timelineIdParser . decodeUtf8

------------------------------------------------------------------------------
--- HasTimelineId
------------------------------------------------------------------------------

class HasTimelineId x where
    timelineId :: x -> TimelineId

    rawTimelineId :: x -> Word32
    rawTimelineId = (\(TimelineId id') -> id') . timelineId

instance HasTimelineId TimelineId where
    timelineId = id

instance HasTimelineId (Maybe TimelineId) where
    timelineId (Just tli) = tli
    timelineId Nothing = def

instance HasTimelineId Word32 where
    timelineId = TimelineId


{-# LANGUAGE QuasiQuotes #-}

{-# LANGUAGE TemplateHaskell #-}

module Watchdog.Replication.Protocol.Timestamp
    ( PostgresTimestamp
    , currentTimestamp
    , encodeTimestamp
    , readTimestamp
    , printTimestamp
    ) where

import           Baulig.Prelude

import           Data.Binary.Builder
import           Data.Binary.Get
import           Data.Ratio
import           Data.Time.Clock.POSIX
import           Data.Time.Format.ISO8601

import           Database.Persist.TH

------------------------------------------------------------------------------
--- StatusUpdate
------------------------------------------------------------------------------

newtype PostgresTimestamp = PostgresTimestamp { unPostgresTimestamp :: Word64 }
    deriving stock (Eq, Ord, Generic, Read, Show)
    deriving newtype (FromJSON, ToJSON)

derivePersistField "PostgresTimestamp"

instance Display PostgresTimestamp where
    display this = display $ this <:> unPostgresTimestamp this <+> printTime
      where
        time = unPostgresTimestamp this + postgresEpoch

        printTime = iso8601Show $ posixSecondsToUTCTime utcSeconds

        utcSeconds = fromRational $ fromIntegral time % 1000000

postgresEpoch :: Word64
postgresEpoch = 946684800000000

unixEpoch :: MonadIO m => m Word64
unixEpoch = liftIO $ round . (1e6 *) <$> getPOSIXTime

currentTimestamp :: MonadIO m => m PostgresTimestamp
currentTimestamp = PostgresTimestamp . (-postgresEpoch +) <$> unixEpoch

printTimestamp :: PostgresTimestamp -> Text
printTimestamp = tshow . unPostgresTimestamp

encodeTimestamp :: PostgresTimestamp -> ByteStringBuilder
encodeTimestamp = putWord64be . unPostgresTimestamp

readTimestamp :: Get PostgresTimestamp
readTimestamp = PostgresTimestamp <$> getWord64be

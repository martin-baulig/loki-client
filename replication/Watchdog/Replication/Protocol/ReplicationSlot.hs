module Watchdog.Replication.Protocol.ReplicationSlot
    ( createReplicationSlot
    , createReplicationSlot'
    , dropReplicationSlot
    , readReplicationSlot
    , createReplicationSlotIfNeeded
    , replicationSlotToWalAddress
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.Connection
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- ReplicationSlot
------------------------------------------------------------------------------

createReplicationSlot
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationSlotName
    -> ReplicationTaskT m ReplicationSlot
createReplicationSlot name =
    createReplicationSlot' $ CreateReplicationSlot name False True

createReplicationSlot'
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => CreateReplicationSlot
    -> ReplicationTaskT m ReplicationSlot
createReplicationSlot' = execSingleRowCommand

dropReplicationSlot
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationSlotName
    -> ReplicationTaskT m ()
dropReplicationSlot name = execNoResultCommand $ DropReplicationSlot name True

readReplicationSlot
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , HasReplicationSlotName x
       , HasCallStack
       )
    => x
    -> ReplicationTaskT m (Maybe ReplicationSlot)
readReplicationSlot =
    execSingleRowCommand . ReadReplicationSlot . replicationSlotName

createReplicationSlotIfNeeded
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationSlotName
    -> ReplicationTaskT m ReplicationSlot
createReplicationSlotIfNeeded name =
    readReplicationSlot (replicationSlotName name) >>= \case
        Just slot -> pure slot
        Nothing -> createReplicationSlot name

replicationSlotToWalAddress :: ReplicationSlot -> WalFileAddress
replicationSlotToWalAddress slot =
    walFileAddress ( fromMaybe defaultTimeline $ rsTimeline slot
                   , walAddressFromLSN $ rsRestartLsn slot
                   )

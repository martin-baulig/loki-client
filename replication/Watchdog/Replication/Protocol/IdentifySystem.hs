module Watchdog.Replication.Protocol.IdentifySystem
    ( IdentifySystem(..)
    , identifySystem
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.Connection
import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- IdentifySystem
------------------------------------------------------------------------------

data IdentifySystem =
    IdentifySystem { identifySystemId           :: !ByteString
                   , identifySystemTimeLine     :: !TimelineId
                   , identifySystemLongPosition :: !LongSequenceNumber
                   , identifySystemDatabase     :: !(Maybe ByteString)
                   }
    deriving stock (Eq, Generic, Show)

instance Display IdentifySystem where
    display system = display $ system <:!> identifySystemId system
        <+> identifySystemTimeLine system <+> identifySystemLongPosition system
        <+> identifySystemLongPosition system
        <+!> fromMaybe "Nothing" (identifySystemDatabase system)

data IdentifySystemCommand = IdentifySystemCommand

instance Display IdentifySystemCommand where
    display _ = "[IdentifySystemCommand]"

instance IsProtocolCommand IdentifySystemCommand where
    protocolCommandString _ = "IDENTIFY_SYSTEM"

instance IsTuplesCommand IdentifySystemCommand where
    type CommandResult IdentifySystemCommand = IdentifySystem

    createCommandResultValue _
                             [ Just systemId
                             , Just (timelineIdFromByteString -> Right timeline)
                             , Just (lsnFromByteString -> Right longPos)
                             , dbname
                             ] =
        Right $ IdentifySystem systemId timeline longPos dbname

    createCommandResultValue _ _ =
        Left $ protocolException "Unexpected IDENTIFY_SYSTEM result."

identifySystem :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
               => ReplicationTaskT m IdentifySystem
identifySystem = execSingleRowCommand IdentifySystemCommand

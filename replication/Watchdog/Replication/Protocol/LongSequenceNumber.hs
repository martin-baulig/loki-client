module Watchdog.Replication.Protocol.LongSequenceNumber
    ( LongSequenceNumber(..)
    , lsnFromByteString
    , lsnFromByteStringThrow
    , lsnToByteString
    , lsnFromWord64
    , lsnToWord64
    , printLSN
    ) where

import           Baulig.Prelude

import           Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Builder             as BB

import           Numeric

import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.Utils

------------------------------------------------------------------------------
--- LongSequenceNumber
------------------------------------------------------------------------------

data LongSequenceNumber =
    LongSequenceNumber { lsnFilePart :: !Word32, lsnOffset :: !Word32 }
    deriving stock (Eq, Generic, Show)

instance Display LongSequenceNumber where
    display (LongSequenceNumber file offset) =
        "[LSN " <> printHex file <> " / " <> printHex32 offset <> "]"

printLSN :: LongSequenceNumber -> Text
printLSN (LongSequenceNumber hi lo) = pack (showHex hi "") <> "-"
    <> printHex32 lo

lsnFromWord64 :: Word64 -> LongSequenceNumber
lsnFromWord64 word = LongSequenceNumber (fromIntegral $ word `div` 4294967296)
                                        (fromIntegral $ word `mod` 4294967296)

lsnToWord64 :: LongSequenceNumber -> Word64
lsnToWord64 (LongSequenceNumber hi lo) =
    fromIntegral hi * 4294967296 + fromIntegral lo

lsnFromByteString :: ByteString -> Either String LongSequenceNumber
lsnFromByteString = parseOnly parser
  where
    parser = LongSequenceNumber <$> (hexadecimal <* char '/') <*> hexadecimal

lsnFromByteStringThrow
    :: (MonadThrow m, HasCallStack) => ByteString -> m LongSequenceNumber
lsnFromByteStringThrow input = case lsnFromByteString input of
    Right lsn -> pure lsn
    Left err -> throwM $ protocolException $ fromString err

lsnToByteString :: LongSequenceNumber -> ByteString
lsnToByteString (LongSequenceNumber file offset) =
    toStrict $ BB.toLazyByteString
    $ BB.word32Hex file <> BB.char8 '/' <> BB.word32HexFixed offset

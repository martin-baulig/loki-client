module Watchdog.Replication.Protocol.StatusUpdate
    ( StandbyStatusUpdate(..)
    , encodeUpdate
    , standbyStatusUpdate
    , sendStatusUpdate
    ) where

import           Baulig.Prelude

import           Data.Binary.Builder
                 hiding (flush, singleton)

import qualified Database.PostgreSQL.LibPQ                as PQ

import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- StatusUpdate
------------------------------------------------------------------------------

data StandbyStatusUpdate =
    StandbyStatusUpdate { ssuLastWritten :: !WalAddress
                        , ssuLastFlushed :: !WalAddress
                        , ssuLastApplied :: !WalAddress
                        , ssuTime        :: !PostgresTimestamp
                        , ssuNeedReply   :: !Word8
                        }
    deriving stock (Eq, Generic, Show)

instance Display StandbyStatusUpdate where
    display this = display $ this <:> ssuLastWritten this
        <+> ssuLastFlushed this <+> ssuLastApplied this <+> ssuTime this

encodeUpdate :: StandbyStatusUpdate -> ByteStringBuilder
encodeUpdate this = putCharUtf8 'r'
    <> putWord64be (walAddressRaw $ ssuLastWritten this)
    <> putWord64be (walAddressRaw $ ssuLastFlushed this)
    <> putWord64be (walAddressRaw $ ssuLastApplied this)
    <> encodeTimestamp (ssuTime this) <> singleton 0

standbyStatusUpdate
    :: MonadIO m => WalAddress -> WalAddress -> m StandbyStatusUpdate
standbyStatusUpdate write flush =
    StandbyStatusUpdate write flush invalidWalAddress <$> currentTimestamp
    <*> pure 0

sendStatusUpdate :: (MonadUnliftIO m, MonadLogger m)
                 => StandbyStatusUpdate
                 -> ReplicationTaskT m ()
sendStatusUpdate update = do
    logMessageN $ "Sending status update: " <> display update

    result <- withConnection $ \raw -> do
        res <- PQ.putCopyData raw $ toStrict $ toLazyByteString
            $ encodeUpdate update
        msg <- PQ.errorMessage raw
        _ <- PQ.flush raw
        pure (res, msg)

    logMessageN $ "Status update done: " <> tshow result

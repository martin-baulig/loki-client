module Watchdog.Replication.Protocol.ListReplicationSlots
    ( listReplicationSlots
    , updateReplicationSlots
    ) where

import           Baulig.Prelude                               hiding (delete)

import           Database.Persist

import           Watchdog.Replication.Connection
import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Connection.ExpectResult
import           Watchdog.Replication.Database
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- ListReplicationSlots
------------------------------------------------------------------------------

data ListReplicationSlots = ListReplicationSlots

instance Display ListReplicationSlots where
    display _ = "[ListReplicationSlots]"

instance IsProtocolCommand ListReplicationSlots where
    protocolCommandString _ =
        "SELECT slot_name, temporary, active, restart_lsn "
        <> "FROM pg_replication_slots" <> " WHERE slot_type = 'physical'"
        <> " AND NOT temporary"

instance IsTuplesCommand ListReplicationSlots where
    type CommandResult ListReplicationSlots = Slot

    createCommandResultValue _
                             [ Just (decodeUtf8 -> name)
                             , _
                             , _
                             , Just (lsnFromByteString -> Right restart)
                             ] = Right $ mkSlot name $ walAddressFromLSN restart

    createCommandResultValue _ _ =
        Left $ protocolException "Unexpected pg_replication_slots result."

listReplicationSlots
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationTaskT m [Slot]
listReplicationSlots = do
    result <- execProtocolCommand ListReplicationSlots
    tuples <- getTuplesResult result
    createCommandResult ListReplicationSlots tuples

updateReplicationSlots
    :: WalConstraints env m => [Slot] -> ReplicationTaskT m [Slot]
updateReplicationSlots slots = do
    logMessageN $ "Update replication slots:"
        <> intercalateDisplay' True "\n    " slots

    runReplicationDatabase $ do
        dbSlots :: [Entity Slot] <- selectList [] []
        catMaybes <$> mapM performUpdate (updateSlots dbSlots slots)
  where
    performUpdate action
        | InsertSlot slot <- action = insert slot >> pure (Just slot)
        | ReplaceSlot key slot <- action = replace key slot >> pure (Just slot)
        | DeleteSlot key <- action = delete key >> pure Nothing

data UpdateAction =
    InsertSlot !Slot | ReplaceSlot !(Key Slot) !Slot | DeleteSlot !(Key Slot)

updateSlots :: [Entity Slot] -> [Slot] -> [UpdateAction]
updateSlots databaseSlots newSlots = deleteOld foldNewSlots
  where
    dbHashFold c idx = insertMap (slotName $ entityVal idx) idx c

    foldNewSlots =
        foldl' foldNew (foldl' dbHashFold new databaseSlots, mempty) newSlots

    new :: HashMap Text (Entity Slot)
    new = mempty

    deleteOld (mapToList -> leftOver, updatedSlots) =
        fmap (DeleteSlot . entityKey . snd) leftOver <> updatedSlots

    foldNew (fromDb, current) item = case lookup (slotName item) fromDb of
        Nothing -> (fromDb, InsertSlot item : current)
        Just existing ->
            let key = entityKey existing
                name = slotName $ entityVal existing
                updated = item { slotStart = slotStart $ entityVal existing }
            in
                (deleteMap name fromDb, ReplaceSlot key updated : current)

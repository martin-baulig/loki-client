module Watchdog.Replication.Protocol.Types
    ( CreateBaseBackup(..)
    , BaseBackupHeader
    , ReadBaseBackupHeader(..)
    , BaseBackupTable
    , ReadBaseBackupTable(..)
    , StartReplication
    , ReplicationSlotName
    , HasReplicationSlotName(..)
    , ReplicationSlot(..)
    , CreateReplicationSlot(..)
    , DropReplicationSlot(..)
    , ReadReplicationSlot(..)
    , startReplicationCommand
    ) where

import           Baulig.Prelude

import           Data.Aeson

import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- CreateBaseBackup
------------------------------------------------------------------------------

data CreateBaseBackup = CreateBaseBackup { cbbLabel    :: !(Maybe ByteString)
                                         , cbbProgress :: !Bool
                                         , cbbManifest :: !Bool
                                         , cbbWal      :: !Bool
                                         , cbbWait     :: !Bool
                                         }
    deriving stock (Eq, Generic, Show)

instance Display CreateBaseBackup where
    display _ = "[CreateBaseBackup]"

instance Default CreateBaseBackup where
    def = CreateBaseBackup Nothing True True True True

instance IsProtocolCommand CreateBaseBackup where
    protocolCommandString this = "BASE_BACKUP (" <> options' <> ")"
      where
        options' = intercalate ", "
            $ catMaybes [ mkBool "PROGRESS" $ cbbProgress this
                        , mkBool "WAL" $ cbbWal this
                        , mkBool "WAIT" $ cbbWait this
                        , if cbbManifest this
                          then Just "MANIFEST yes"
                          else Nothing
                        , if cbbManifest this
                          then Just "MANIFEST_CHECKSUMS SHA256"
                          else Nothing
                        ]

        mkBool _ False = Nothing
        mkBool name True = Just $ name <> " true"

------------------------------------------------------------------------------
--- BaseBackupHeader
------------------------------------------------------------------------------

data BaseBackupHeader = BaseBackupHeader { bbStart    :: !LongSequenceNumber
                                         , bbTimeline :: !TimelineId
                                         }
    deriving stock (Eq, Show)

instance Display BaseBackupHeader where
    display this = display $ this <:> bbStart this <+> bbTimeline this

instance HasWalAddress BaseBackupHeader where
    walAddress = walAddressFromLSN . bbStart

instance HasTimelineId BaseBackupHeader where
    timelineId = bbTimeline

data ReadBaseBackupHeader = ReadBaseBackupHeader

instance IsTuplesCommand ReadBaseBackupHeader where
    type CommandResult ReadBaseBackupHeader = BaseBackupHeader

    createCommandResultValue _ = \case
        [ Just (lsnFromByteString -> Right lsn)
            , Just (timelineIdFromByteString -> Right timeline)
            ] -> Right $ BaseBackupHeader lsn timeline
        other -> Left $ protocolException
            $ "Invalid BaseBackup header: " <> tshow other

------------------------------------------------------------------------------
--- BaseBackupTable
------------------------------------------------------------------------------

data BaseBackupTable =
    BaseBackupTable !(Maybe ByteString) !(Maybe ByteString) !(Maybe ByteString)
    deriving stock (Eq, Show)

instance Display BaseBackupTable where
    display (BaseBackupTable oid loc size) =
        mconcat [ "[BaseBackupTable"
                , disp "oid" oid
                , disp "location" loc
                , disp "size" size
                , "]"
                ]
      where
        disp _ Nothing = ""
        disp name (Just val) = " " <> name <> "=" <> decodeUtf8 val

data ReadBaseBackupTable = ReadBaseBackupTable

instance IsTuplesCommand ReadBaseBackupTable where
    type CommandResult ReadBaseBackupTable = BaseBackupTable

    createCommandResultValue _ = \case
        [ oid, loc, size ] -> Right $ BaseBackupTable oid loc size
        other -> Left $ protocolException
            $ "Invalid BaseBackup table header: " <> tshow other

------------------------------------------------------------------------------
--- StartReplication
------------------------------------------------------------------------------

data StartReplication =
    StartReplication !(Maybe ReplicationSlotName) !WalAddress
    deriving stock (Eq, Show)

instance Display StartReplication where
    display (StartReplication slot start) = unwords
        $ catMaybes [ Just "[StartReplication"
                    , fmap display slot
                    , Just $ display start
                    , Just "]"
                    ]

instance IsProtocolCommand StartReplication where
    protocolCommandString (StartReplication slot start) = intercalate " "
        $ catMaybes [ Just "START_REPLICATION"
                    , (\n -> Just $ "SLOT "
                       <> encodeUtf8 (replicationSlotRawName n)) =<< slot
                    , Just "PHYSICAL"
                    , Just $ lsnToByteString $ walAddressToLSN start
                    ]

instance HasWalAddress StartReplication where
    walAddress (StartReplication _ start) = start

startReplicationCommand
    :: Maybe ReplicationSlotName -> WalAddress -> StartReplication
startReplicationCommand maybeSlot =
    StartReplication maybeSlot . resetWalAddressToSegmentStart

------------------------------------------------------------------------------
--- ReplicationSlotName
------------------------------------------------------------------------------

newtype ReplicationSlotName = ReplicationSlotName ByteString
    deriving stock (Eq, Generic, Show)

instance Display ReplicationSlotName where
    display (ReplicationSlotName name) =
        "[ReplicationSlotName " <> decodeUtf8 name <> "]"

instance IsString ReplicationSlotName where
    fromString = ReplicationSlotName . fromString

instance FromJSON ReplicationSlotName where
    parseJSON = withText "ReplicationSlot"
        $ \value -> pure $ ReplicationSlotName $ encodeUtf8 value

------------------------------------------------------------------------------
--- HasReplicationSlotName
------------------------------------------------------------------------------

class HasReplicationSlotName x where
    replicationSlotName :: x -> ReplicationSlotName

    replicationSlotRawName :: x -> Text
    replicationSlotRawName this =
        let ReplicationSlotName name = replicationSlotName this
        in
            decodeUtf8 name

instance HasReplicationSlotName ReplicationSlotName where
    replicationSlotName = id

instance HasReplicationSlotName Text where
    replicationSlotName = ReplicationSlotName . encodeUtf8

------------------------------------------------------------------------------
--- ReplicationSlot
------------------------------------------------------------------------------

data ReplicationSlot = ReplicationSlot { rsName       :: !ReplicationSlotName
                                       , rsRestartLsn :: !LongSequenceNumber
                                       , rsTimeline   :: !(Maybe TimelineId)
                                       }

instance Display ReplicationSlot where
    display this = display
        $ this <:> rsName this <+> rsRestartLsn this <+> rsTimeline this

instance HasReplicationSlotName ReplicationSlot where
    replicationSlotName = rsName

------------------------------------------------------------------------------
--- CreateReplicationSlot
------------------------------------------------------------------------------

data CreateReplicationSlot =
    CreateReplicationSlot { crsName       :: !ReplicationSlotName
                          , crsTemporary  :: !Bool
                          , crsReserveWal :: !Bool
                          }

instance Display CreateReplicationSlot where
    display (CreateReplicationSlot name temp reserve) =
        mconcat [ "[CreateReplicationSlot "
                , display name
                , bool "" " temporary" temp
                , bool "" " reserve-wal" reserve
                , "]"
                ]

instance HasReplicationSlotName CreateReplicationSlot where
    replicationSlotName = crsName

instance IsProtocolCommand CreateReplicationSlot where
    protocolCommandString (CreateReplicationSlot name temp reserve) =
        mconcat [ "CREATE_REPLICATION_SLOT "
                , encodeUtf8 $ replicationSlotRawName name
                , bool "" " TEMPORARY" temp
                , " PHYSICAL"
                , bool "" " (RESERVE_WAL true)" reserve
                ]

instance IsTuplesCommand CreateReplicationSlot where
    type CommandResult CreateReplicationSlot = ReplicationSlot

    createCommandResultValue _
                             [ Just name
                             , Just (lsnFromByteString -> Right consistent)
                             , Nothing
                             , Nothing
                             ] = Right
        $ ReplicationSlot (ReplicationSlotName name) consistent Nothing
    createCommandResultValue _ result = Left $ protocolException
        $ "Unexpected CREATE_REPLICATION_SLOT result: " <> tshow result

------------------------------------------------------------------------------
--- DropReplicationSlot
------------------------------------------------------------------------------

data DropReplicationSlot =
    DropReplicationSlot { drsName :: !ReplicationSlotName, drsWait :: !Bool }

instance Display DropReplicationSlot where
    display (DropReplicationSlot name wait') =
        mconcat [ "[DropReplicationSlot "
                , display name
                , bool " wait" "" wait'
                , "]"
                ]

instance HasReplicationSlotName DropReplicationSlot where
    replicationSlotName = drsName

instance IsProtocolCommand DropReplicationSlot where
    protocolCommandString (DropReplicationSlot name wait') =
        mconcat [ "DROP_REPLICATION_SLOT "
                , encodeUtf8 $ replicationSlotRawName name
                , bool " WAIT" "" wait'
                ]

------------------------------------------------------------------------------
--- ReadReplicationSlot
------------------------------------------------------------------------------

newtype ReadReplicationSlot = ReadReplicationSlot ReplicationSlotName

instance Display ReadReplicationSlot where
    display (ReadReplicationSlot name) =
        mconcat ["[ReadReplicationSlot ", display name, "]"]

instance HasReplicationSlotName ReadReplicationSlot where
    replicationSlotName (ReadReplicationSlot name) = name

instance IsProtocolCommand ReadReplicationSlot where
    protocolCommandString (ReadReplicationSlot name) =
        mconcat [ "READ_REPLICATION_SLOT "
                , encodeUtf8 $ replicationSlotRawName name
                ]

instance IsTuplesCommand ReadReplicationSlot where
    type CommandResult ReadReplicationSlot = Maybe ReplicationSlot

    createCommandResultValue _ [ Nothing, Nothing, Nothing ] = pure Nothing

    createCommandResultValue command
                             [ Just "physical"
                             , Just (lsnFromByteString -> Right restart)
                             , Just (timelineIdFromByteString -> Right timeline)
                             ] = Right $ Just
        $ ReplicationSlot { rsName       = replicationSlotName command
                          , rsRestartLsn = restart
                          , rsTimeline   = Just timeline
                          }
    createCommandResultValue _ result = Left $ protocolException
        $ "Unexpected READ_REPLICATION_SLOT result: " <> tshow result


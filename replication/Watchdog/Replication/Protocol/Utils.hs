module Watchdog.Replication.Protocol.Utils
    ( printHex32
    , printHex16
    , printHex
    , walSegmentSize
    , walSegmentSize32
    , lowBits64
    , highBits64
    ) where

import           Baulig.Prelude

import qualified Data.Text      as T
import           Data.Word

import           Numeric

------------------------------------------------------------------------------
--- Misc helpers
------------------------------------------------------------------------------

printHex32 :: Word32 -> Text
printHex32 = T.justifyRight 8 '0' . pack . flip showHex ""

printHex16 :: Word16 -> Text
printHex16 = T.justifyRight 4 '0' . pack . flip showHex ""

printHex :: Word32 -> Text
printHex = pack . flip showHex ""

walSegmentSize32 :: Word32
walSegmentSize32 = 16 * 1024 * 1024

walSegmentSize :: Word64
walSegmentSize = 16 * 1024 * 1024

lowBits64 :: Word64 -> Word32
lowBits64 val = fromIntegral $ val `mod` 4294967296

highBits64 :: Word64 -> Word32
highBits64 val = fromIntegral $ val `div` 4294967296

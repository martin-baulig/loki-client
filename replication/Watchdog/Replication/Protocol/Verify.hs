module Watchdog.Replication.Protocol.Verify (verifyWalFile, runVerify) where

import           Baulig.Prelude                           hiding (take)

import           Data.Binary
import           Data.Binary.Get
import           Data.Conduit.Serialization.Binary

import           Watchdog.Replication.Backend
import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.TimelineId
import           Watchdog.Replication.Protocol.Utils
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- verifyWalFile
------------------------------------------------------------------------------

data Header = Header { xlpMagic     :: !Word16
                     , xlpInfo      :: !Word16
                     , xlpTimeline  :: !TimelineId
                     , xlpAddress   :: !WalAddress
                     , xlpRemaining :: !Word32
                     }
    deriving stock (Eq, Show)

instance Display Header where
    display this = unwords [ "[Header"
                           , printHex16 $ xlpMagic this
                           , printHex16 $ xlpInfo this
                           , display $ xlpTimeline this
                           , display $ xlpAddress this
                           , printHex32 $ xlpRemaining this
                           ] <> "]"

readHeader :: Get Header
readHeader = Header <$> getWord16le <*> getWord16le
    <*> (timelineId <$> getWord32le) <*> readWalAddressLe <*> getWord32le

headerMagic :: Word16
headerMagic = 0xd110

verifyWalFile
    :: WalConstraints env m => WalFileAddress -> ConduitT ByteString o m Bool
verifyWalFile file = do
    logMessageN $ "Verifying WAL file " <> display file
    header <- sinkGet readHeader
    logVerboseN $ "Got WAL header " <> display header
    unless (xlpMagic header == headerMagic) $ invalid $ "invalid magic; got 0x"
        <> printHex16 (xlpMagic header) <> ", but expected 0x"
        <> printHex16 headerMagic
    unless (xlpTimeline header == timelineId file) $ invalid
        $ "wrong timeline; got " <> display (xlpTimeline header)
        <> ", but expected " <> display (timelineId file)
    rest <- sinkGet $ getRemainingLazyByteString
    unless (length rest + 20 == fromIntegral walSegmentSize) $ invalid
        $ "incorrect size; got " <> tshow (length rest + 20)
        <> ", but expected " <> tshow walSegmentSize
    pure True
  where
    invalid msg = throwM $ protocolException
        $ "Invalid WAL file " <> display file <> ": " <> msg <> "."

runVerify :: (WalConstraints env m, Backend x) => x -> WalFileAddress -> m ()
runVerify backend file = do
    ok <- runConduitRes $ readWalFile backend file .| verifyWalFile file

    unless ok $ throwM $ protocolException "Failed to verify WAL file."

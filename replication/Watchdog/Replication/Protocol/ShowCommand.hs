module Watchdog.Replication.Protocol.ShowCommand
    ( Parameter(..)
    , ShowCommand
    , showParameter
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.Connection
import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- ShowCommand
------------------------------------------------------------------------------

data Parameter = WalLevel
               | CheckpointTimeout
               | MinWalSize
               | MaxWalSize
               | MaxWalSenders
               | WalSegmentSize
    deriving stock (Enum, Eq, Ord, Show)

instance Display Parameter where
    display = tshow

newtype ShowCommand = ShowCommand Parameter
    deriving stock (Eq, Show)

instance Display ShowCommand where
    display (ShowCommand parameter) = "[ShowCommand " <> tshow parameter <> "]"

instance IsProtocolCommand ShowCommand where
    protocolCommandString (ShowCommand parameter) = "SHOW " <> case parameter of
        WalLevel -> "wal_level"
        CheckpointTimeout -> "checkpoint_timeout"
        MinWalSize -> "min_wal_size"
        MaxWalSize -> "max_wal_size"
        MaxWalSenders -> "max_wal_senders"
        WalSegmentSize -> "wal_segment_size"

instance IsTuplesCommand ShowCommand where
    type CommandResult ShowCommand = Text

    createCommandResultValue _ values = Right $ tshow values

showParameter :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
              => Parameter
              -> ReplicationTaskT m ()
showParameter parameter = do
    result <- execSingleRowCommand $ ShowCommand parameter
    logMessageN
        $ "Show parameter: " <> display parameter <> " " <> display result

module Watchdog.Replication.Protocol.CopyOut
    ( CopyOutChunk(..)
    , XLogData(..)
    , XLogChunk(..)
    , PrimaryKeepAlive(..)
    , parseCopyOutChunk
    , mapToXLogChunk
    ) where

import           Baulig.Prelude

import           Data.Binary.Get
import qualified Data.ByteString.Char8                    as BC8

import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- XLogData
------------------------------------------------------------------------------

data XLogData = XLogData !WalAddress !WalAddress !Word64 !ByteString
    deriving stock (Eq)

instance Display XLogData where
    display (XLogData start end time (length -> len)) =
        mconcat [ "[XLogData "
                , display start
                , " "
                , display end
                , " "
                , display time
                , " "
                , display len
                , "]"
                ]

instance HasWalAddress XLogData where
    walAddress (XLogData start _ _ _) = start

readXLogData :: Get XLogData
readXLogData = XLogData <$> readWalAddress <*> readWalAddress <*> getWord64be
    <*> fmap toStrict getRemainingLazyByteString

------------------------------------------------------------------------------
--- XLogChunk
------------------------------------------------------------------------------

data XLogChunk =
    XLogChunk { xlogChunkAddress :: !WalAddress, xlogChunkBody :: !ByteString }
    deriving stock (Eq)

instance Display XLogChunk where
    display (XLogChunk start (length -> len)) =
        mconcat ["[XLogChunk ", display start, " ", display len, "]"]

instance HasWalAddress XLogChunk where
    walAddress (XLogChunk start _) = start

mapToXLogChunk :: XLogData -> XLogChunk
mapToXLogChunk (XLogData start _ _ body) = XLogChunk start body

------------------------------------------------------------------------------
--- PrimaryKeepAlive
------------------------------------------------------------------------------

data PrimaryKeepAlive = PrimaryKeepAlive !WalAddress !PostgresTimestamp !Bool
    deriving stock (Eq)

instance Display PrimaryKeepAlive where
    display (PrimaryKeepAlive end time needReply) =
        display $ "PrimaryKeepAlive" <:=> end <+> time <+> needReply

instance HasWalAddress PrimaryKeepAlive where
    walAddress (PrimaryKeepAlive address _ _) = address

readPrimaryKeepAlive :: Get PrimaryKeepAlive
readPrimaryKeepAlive =
    PrimaryKeepAlive <$> readWalAddress <*> readTimestamp <*> getBool
  where
    getBool = getInt8 <&> (0 <)

------------------------------------------------------------------------------
--- CopyOutChunk
------------------------------------------------------------------------------

data CopyOutChunk =
      EmptyChunk
    | UnknownChunk !Char !ByteString
    | DecodeFailed !Text !ByteString
    | NewArchive !Text !(Maybe Text)
    | ProgressReport !Word64
    | DataChunk !ByteString
    | ManifestStartChunk
    | WalChunk !XLogData
    | KeepAliveChunk !PrimaryKeepAlive
    | ShutdownChunk
    deriving stock (Eq)

instance Display CopyOutChunk where
    display EmptyChunk = "[EmptyChunk]"
    display (UnknownChunk code (length -> len)) =
        mconcat ["[UnknownChunk ", display code, " ", display len, "]"]
    display (DecodeFailed msg (length -> len)) =
        mconcat ["[DecodeFailed ", display msg, " ", display len, "]"]
    display (NewArchive name Nothing) =
        mconcat ["[NewArchive ", display name, "]"]
    display (NewArchive name (Just dir)) =
        mconcat ["[NewArchive ", display name, " ", display dir, "]"]
    display (ProgressReport len) =
        mconcat ["[ProgressReport ", display len, "]"]
    display (DataChunk (length -> len)) =
        mconcat ["[DataChunk ", display len, "]"]
    display ManifestStartChunk = "[ManifestStartChunk]"
    display (WalChunk wal) = mconcat ["[WalChunk ", display wal, "]"]
    display (KeepAliveChunk chunk) =
        mconcat ["[KeepAliveChunk ", display chunk, "]"]
    display ShutdownChunk = "[ShutdownChunk]"

{-
instance HasLogLevel CopyOutChunk where
    getLogLevel EmptyChunk = LevelWarn
    getLogLevel (UnknownChunk _ _) = LevelWarn
    getLogLevel (DecodeFailed _ _) = LevelWarn
    getLogLevel (NewArchive _ _) = LevelInfo
    getLogLevel (ProgressReport _) = LevelInfo
    getLogLevel (DataChunk _) = LevelDebug
    getLogLevel ManifestStartChunk = LevelInfo
    getLogLevel (WalChunk _) = LevelInfo
    getLogLevel (KeepAliveChunk _) = LevelInfo
    getLogLevel ShutdownChunk = LevelWarn
-}

getText :: Get Text
getText = getLazyByteStringNul <&> decodeUtf8 . toStrict

getMaybeText :: Get (Maybe Text)
getMaybeText = getText <&> maybeText
  where
    maybeText text
        | null text = Nothing
    maybeText text = Just text

parseCopyOutChunk :: ByteString -> CopyOutChunk
parseCopyOutChunk input = case BC8.uncons input of
    Nothing -> EmptyChunk
    Just (p, rest) -> decode p rest
  where
    decode 'n' (fromStrict -> rest) =
        flip runGet rest $ NewArchive <$> getText <*> getMaybeText

    decode 'm' rest
        | null rest = ManifestStartChunk
    decode 'm' rest = DecodeFailed "Invalid manifest start chunk." rest

    decode 'd' rest = DataChunk rest

    decode 'p' (fromStrict -> rest)
        | length rest == 8 = ProgressReport $ runGet getWord64be rest
    decode 'p' rest = DecodeFailed "Invalid progress report." rest

    decode 'w' (fromStrict -> rest) = WalChunk $ runGet readXLogData rest

    decode 'k' (fromStrict -> rest) =
        KeepAliveChunk $ runGet readPrimaryKeepAlive rest

    decode code rest = UnknownChunk code rest

module Watchdog.Replication.Protocol.WalAddress
    ( WalAddress
    , WalFileAddress
    , HasWalAddress(..)
    , HasWalFileAddress(..)
    , CanAdvanceWalAddress(..)
    , readWalAddress
    , readWalAddressLe
    , walAddressFromLSN
    , walAddressToLSN
    , invalidWalAddress
    , walFileNameParser
    , filterWalFilesWithTimeline
    , module Watchdog.Replication.Protocol.TimelineId
    , module Watchdog.Replication.Protocol.LongSequenceNumber
    , module Watchdog.Replication.Protocol.Utils
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.Attoparsec.Text                             hiding (count)
import           Data.Binary.Get
import           Data.Scientific

import           Watchdog.Replication.Protocol.LongSequenceNumber
import           Watchdog.Replication.Protocol.TimelineId
import           Watchdog.Replication.Protocol.Utils
import           Watchdog.Utils.ConduitUtils

------------------------------------------------------------------------------
--- WalAddress
------------------------------------------------------------------------------

newtype WalAddress = WalAddress Word64
    deriving stock (Eq, Ord)

instance Display WalAddress where
    display this = "[WalAddress " <> displayWalAddress this <> "]"

instance Show WalAddress where
    show = unpack . display

instance ToJSON WalAddress where
    toJSON (WalAddress address) = toJSON address

instance FromJSON WalAddress where
    parseJSON =
        withScientific "WalAddress" $ \value -> case toBoundedInteger value of
            Just address -> pure $ WalAddress address
            Nothing -> error $ "Invalid WAL address: " ++ show value

displayWalAddress :: WalAddress -> Text
displayWalAddress this = printHex hi <> "/" <> printHex32 lo
  where
    hi = highBits64 $ walAddressRaw this

    lo = lowBits64 $ walAddressRaw this

readWalAddress :: Get WalAddress
readWalAddress = WalAddress <$> getWord64be

readWalAddressLe :: Get WalAddress
readWalAddressLe = WalAddress <$> getWord64le

walAddressFromLSN :: LongSequenceNumber -> WalAddress
walAddressFromLSN = WalAddress . lsnToWord64

walAddressToLSN :: WalAddress -> LongSequenceNumber
walAddressToLSN = lsnFromWord64 . walAddressRaw

invalidWalAddress :: WalAddress
invalidWalAddress = WalAddress 0

------------------------------------------------------------------------------
--- WalFileAddress
------------------------------------------------------------------------------

data WalFileAddress =
    WalFileAddress { wfaTimeline :: !TimelineId, wfaAddress :: !WalAddress }
    deriving stock (Eq, Ord, Show)

instance Display WalFileAddress where
    display (WalFileAddress timeline address) =
        "[WalFileAddress " <> displayWalAddress address <> " (timeline "
        <> printHex (rawTimelineId timeline) <> ")]"

instance HasTimelineId WalFileAddress where
    timelineId = wfaTimeline

isHex :: Char -> Bool
isHex c
    | c >= '0' && c <= '9' = True
    | c >= 'a' && c <= 'f' = True
    | c >= 'A' && c <= 'F' = True
    | otherwise = False

walFileNameParser :: FilePath -> Maybe WalFileAddress
walFileNameParser path
    | length path /= 24 || not (all isHex path) = Nothing
    | (parser -> Right time, parser -> Right file) <- split = Just
        $ WalFileAddress (timeCtor time) (WalAddress $ file * walSegmentSize)
    | otherwise = Nothing
  where
    split = splitAt 8 $ pack path

    parser = parseOnly (hexadecimal :: Parser Word64)

    timeCtor = TimelineId . fromIntegral

filterWalFilesWithTimeline
    :: Monad m => Maybe TimelineId -> ConduitT WalFileAddress WalAddress m ()
filterWalFilesWithTimeline (fromMaybe def -> timeline) = filterMapC sameTimeline
  where
    sameTimeline this
        | wfaTimeline this == timeline = Just $ wfaAddress this
        | otherwise = Nothing

------------------------------------------------------------------------------
--- HasWalAddress
------------------------------------------------------------------------------

class HasWalAddress x where
    walAddress :: x -> WalAddress

    walAddressRaw :: x -> Word64
    walAddressRaw (walAddress -> WalAddress ptr) = ptr

    walAddressSegmentOffset :: x -> Word32
    walAddressSegmentOffset (walAddressRaw -> address) =
        fromIntegral $ address `mod` walSegmentSize

    walAddressSegmentRemaining :: x -> Word32
    walAddressSegmentRemaining address =
        walSegmentSize32 - walAddressSegmentOffset address

    walAddressIsAtSegmentStart :: x -> Bool
    walAddressIsAtSegmentStart (walAddressRaw -> address) =
        address `mod` walSegmentSize == 0

instance HasWalAddress WalAddress where
    walAddress = id

instance HasWalAddress WalFileAddress where
    walAddress = wfaAddress

instance HasWalAddress Word64 where
    walAddress = WalAddress

------------------------------------------------------------------------------
--- CanAdvanceWalAddress
------------------------------------------------------------------------------

class CanAdvanceWalAddress x where
    resetWalAddressToSegmentStart :: x -> x

    advanceWalAddress :: Integral a => x -> a -> x

    advanceWalAddressToNextFile :: x -> x
    advanceWalAddressToNextFile this =
        advanceWalAddress (resetWalAddressToSegmentStart this) walSegmentSize

instance CanAdvanceWalAddress Word64 where
    resetWalAddressToSegmentStart address = (address `div` walSegmentSize)
        * walSegmentSize

    advanceWalAddress address amount = address + fromIntegral amount

instance CanAdvanceWalAddress WalAddress where
    resetWalAddressToSegmentStart (WalAddress address) =
        WalAddress $ resetWalAddressToSegmentStart address

    advanceWalAddress (WalAddress address) amount =
        WalAddress $ address + fromIntegral amount

instance CanAdvanceWalAddress WalFileAddress where
    resetWalAddressToSegmentStart (WalFileAddress timeline address) =
        WalFileAddress timeline $ resetWalAddressToSegmentStart address

    advanceWalAddress (WalFileAddress timeline address) amount =
        WalFileAddress timeline $ advanceWalAddress address amount

------------------------------------------------------------------------------
--- HasWalFileAddress
------------------------------------------------------------------------------

class HasWalFileAddress x where
    walFileAddress :: x -> WalFileAddress

    walFileAddressToFileName :: x -> FilePath
    walFileAddressToFileName (walFileAddress -> this) = unpack $ printHex32 tli
        <> printHex32 (highBits64 addr) <> printHex32 (lowBits64 addr)
      where
        tli = rawTimelineId $ wfaTimeline this

        addr = walAddressRaw this `div` walSegmentSize

instance HasWalFileAddress WalFileAddress where
    walFileAddress = id

instance HasWalFileAddress (TimelineId, WalAddress) where
    walFileAddress (timeline, address) = WalFileAddress timeline address

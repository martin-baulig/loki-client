module Watchdog.Replication.Connection
    ( execProtocolCommand
    , execNoResultCommand
    , execSingleRowCommand
      -- re-exports
    , ReplicationConnection
    , WalConstraints
    , IsProtocolCommand(..)
    , connectToReplication
    , connectToReplicationDatabase
    , replicationConnectionStatus
    , module Watchdog.Replication.Connection.StreamAsync
    ) where

import           Baulig.Prelude

import           Database.PostgreSQL.LibPQ

import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Connection.ExpectResult
import           Watchdog.Replication.Connection.StreamAsync
import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- Helper functions
------------------------------------------------------------------------------

execProtocolCommand
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => IsProtocolCommand x
    => x
    -> ReplicationTaskT m AsyncResult
execProtocolCommand command = do
    logVerboseN $ "Executing command: " <> display command <> " => "
        <> decodeUtf8 (protocolCommandString command)
    withConnection (`exec` cstring) >>= \case
        Just result -> fromResult result >>= \ares -> do
            logVerboseN $ "Command " <> display command <> " returned: "
                <> display ares
            pure ares
        Nothing -> do
            logWarnN $ "Command failed: " <> display command
            throwM $ protocolException $ display command
  where
    cstring = protocolCommandString command

execNoResultCommand
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => IsProtocolCommand x
    => x
    -> ReplicationTaskT m ()
execNoResultCommand command = expectNoResult =<< execProtocolCommand command

execSingleRowCommand
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , IsProtocolCommand x
       , IsTuplesCommand x
       , HasCallStack
       )
    => x
    -> ReplicationTaskT m (CommandResult x)
execSingleRowCommand command = do
    execProtocolCommand command
        >>= flip withTuplesResult (createSingleRowResult command)


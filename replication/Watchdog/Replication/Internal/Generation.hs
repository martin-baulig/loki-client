module Watchdog.Replication.Internal.Generation
    ( BackupGeneration(..)
    , HasBackupGeneration(..)
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Database
import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- BackupGeneration
------------------------------------------------------------------------------

newtype BackupGeneration =
    BackupGeneration { unBackupGeneration :: Generation }
    deriving stock (Eq, Generic, Show)
    deriving newtype (FromJSON, ToJSON)

instance HasWalAddress BackupGeneration where
    walAddress (unBackupGeneration -> this) = walAddress $ generationStart this

instance HasWalFileAddress BackupGeneration where
    walFileAddress (unBackupGeneration -> this) =
        walFileAddress ( TimelineId $ generationTimeline this
                       , walAddress $ generationStart this
                       )

instance HasTimelineId BackupGeneration where
    timelineId (unBackupGeneration -> this) =
        TimelineId $ generationTimeline this

instance Display BackupGeneration where
    display this = display $ this <:> generationCreated generation
        <+> generationUpdated generation <+> generationName generation
        <+> generationStatus generation <+> walFileAddress this
      where
        generation = unBackupGeneration this

------------------------------------------------------------------------------
--- HasBackupGeneration
------------------------------------------------------------------------------

class HasBackupGeneration x where
    backupGeneration :: x -> BackupGeneration

    backupGenerationTimestamp :: x -> PostgresTimestamp
    backupGenerationTimestamp (backupGeneration -> BackupGeneration this) =
        generationCreated this

    backupGenerationRootPath :: x -> FilePath
    backupGenerationRootPath (backupGeneration -> BackupGeneration this) =
        unpack $ printTimestamp $ generationCreated this

    backupGenerationPath :: x -> BackupFileName -> FilePath
    backupGenerationPath this file =
        backupGenerationRootPath this </> rawBackupFileName file

    backupGenerationPosition :: x -> WalAddress
    backupGenerationPosition (backupGeneration -> BackupGeneration this) =
        walAddress $ generationPosition this

instance HasBackupGeneration BackupGeneration where
    backupGeneration = id


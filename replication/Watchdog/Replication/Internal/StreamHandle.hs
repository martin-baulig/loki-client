module Watchdog.Replication.Internal.StreamHandle
    ( StreamHandle
    , newHandle
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Internal.AsyncStream
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- StreamHandle
------------------------------------------------------------------------------

data StreamHandle = StreamHandle { shFile     :: !WalFileAddress
                                 , shName     :: !BackupFileName
                                 , shPosition :: !WalAddress
                                 , shAsync    :: !AsyncStream
                                 }

instance Display StreamHandle where
    display this = display $ this <:> shFile this <+> shPosition this

instance HasWalAddress StreamHandle where
    walAddress = shPosition

instance HasAsyncStream StreamHandle where
    asyncStream = shAsync

instance HasWalFileAddress StreamHandle where
    walFileAddress = shFile

instance HasBackupFileName StreamHandle where
    backupFileName = shName

instance CanAdvanceWalAddress StreamHandle where
    resetWalAddressToSegmentStart this =
        this { shPosition = resetWalAddressToSegmentStart $ shPosition this }

    advanceWalAddress this amount =
        this { shPosition = advanceWalAddress (shPosition this) amount }

newHandle :: MonadIO m => WalFileAddress -> m StreamHandle
newHandle address = StreamHandle address name (walAddress address)
    <$> newAsyncStream name
  where
    name = WalFileName $ pack $ walFileAddressToFileName address

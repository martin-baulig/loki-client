module Watchdog.Replication.Internal.BackupContext
    ( BackupState(..)
    , BackupContext
    , ContextConstraints
    , newBackupContext
    , backupContextInitialize
    , backupContextStartFile
    , backupContextStartManifest
    , backupContextFinish
    , backupContextFinalize
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.Backend
import           Watchdog.Replication.BackupInfo
import           Watchdog.Replication.Connection
import           Watchdog.Replication.Database.Generation
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Internal.AsyncStream
import           Watchdog.Replication.Internal.Generation
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.StreamReplication
import           Watchdog.Replication.Task
import           Watchdog.Utils.InternalError

------------------------------------------------------------------------------
--- BackupState
------------------------------------------------------------------------------

data BackupState = BackupStart
                 | BackupHeader !BaseBackupHeader
                 | BackupTables !BaseBackupHeader
                 | BackupFile !AsyncStreamWriter
                 | BackupManifest !AsyncStreamWriter
                 | BackupFooter !BaseBackupHeader
                 | BackupFinished !BackupInfo

instance Display BackupState where
    display BackupStart = "[BackupStart]"
    display (BackupHeader header) = display $ "BackupHeader" <:=> header
    display (BackupTables header) = display $ "BackupTables" <:=> header
    display (BackupFile writer) = display $ "BackupFile" <:=> writer
    display (BackupManifest writer) = display $ "BackupManifest" <:=> writer
    display (BackupFooter footer) = display $ "BackupFooter" <:=> footer
    display (BackupFinished _) = "[BackupFinished]"

------------------------------------------------------------------------------
--- BackupContext
------------------------------------------------------------------------------

data BackupContext x =
    BackupContext { bcBackend        :: !x
                  , bcGeneration     :: !BackupGeneration
                  , bcStart          :: !(TVar (Maybe WalAddress))
                  , bcStorageContext :: !(TVar (Maybe (StorageContext x)))
                  , bcAsync          :: !(TVar (Maybe AsyncStream))
                  , bcOperation      :: !(TVar (Maybe StreamReplicationContext))
                  , bcFileStore      :: !BackupFileStore
                  }

instance Backend x => Display (BackupContext x) where
    display this = display $ this <:> bcBackend this <:> bcGeneration this

instance HasBackupGeneration (BackupContext x) where
    backupGeneration = bcGeneration

type ContextConstraints env x m =
    (MonadResource m, WalConstraints env m, Backend x)

newBackupContext :: MonadIO m => x -> BackupGeneration -> m (BackupContext x)
newBackupContext backend generation = BackupContext backend generation
    <$> newTVarIO Nothing <*> newTVarIO Nothing <*> newTVarIO Nothing
    <*> newTVarIO Nothing <*> newBackupFileStore

backupContextInitialize
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, Backend x, HasCallStack)
    => BackupContext x
    -> BaseBackupHeader
    -> [BaseBackupTable]
    -> ReplicationT m ()
backupContextInitialize this header _ = do
    logMessageN $ "Initialize backup context: " <> display this

    storage <- createStorageContext (bcBackend this) generation

    atomically $ do
        unlessM (isNothing <$> readTVar (bcStorageContext this)) $ throwM
            $ protocolException "Backup context already initialized."
        writeTVar (bcStorageContext this) $ Just storage
        writeTVar (bcStart this) $ Just start

    slot <- case generationSlotName $ unBackupGeneration generation of
        Just slotName -> pure $ replicationSlotName slotName
        Nothing -> throwM
            $ backupException "Generation does not have a replication slot."

    logMessageN $ "Starting WAL streaming in the background: " <> display this

    operation <- startReplicationForGeneration storage generation slot start

    atomically $ do
        unlessM (isNothing <$> readTVar (bcOperation this)) $ throwM
            $ protocolException "Backup context already initialized."
        writeTVar (bcOperation this) $ Just operation

    waitForReplication operation

    logMessageN $ "Successfully started WAL streaming in the background: "
        <> display this
  where
    generation = bcGeneration this

    start = walAddress header

backupContextFinish
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, Backend x, HasCallStack)
    => BackupContext x
    -> BaseBackupHeader
    -> ReplicationT m BackupInfo
backupContextFinish this (walAddress -> end) = do
    logMessageN
        $ "Finishing base backup: " <> display this <> " " <> display end

    (operation, storage, start) <- atomically getStatus

    walFiles <- finishReplication operation end

    files <- atomically $ backupFileStoreGetFiles $ bcFileStore this

    logMessageN "Finishing base backup #1."
    forM_ files $ \file -> logMessageN $ "File: " <> display file
    forM_ walFiles $ \file -> logMessageN $ "WAL File: " <> display file

    let info = createBackupInfo generation start end files walFiles

    writeBackupInfo storage info

    makeGenerationActive generation start end

    pure info
  where
    generation = bcGeneration this

    getStatus = (,,) <$> readOrThrow (bcOperation this)
        <*> readOrThrow (bcStorageContext this) <*> readOrThrow (bcStart this)

readOrThrow :: TVar (Maybe x) -> STM x
readOrThrow var = readTVar var >>= \case
    Just value -> pure value
    Nothing -> throwM $ internalErrorException "Backup context not initialized."

finalizeFile
    :: WalConstraints env m => BackupContext x -> Maybe AsyncStream -> m ()
finalizeFile _ Nothing = pure ()
finalizeFile this (Just stream) = do
    logMessageN $ "Finalizing file: " <> display stream
    atomically $ writeAsyncStreamFinal $ asyncStreamWriter stream
    logDebugN "Wrote final chunk."
    waitForAsyncStream stream >>= recordFileInfo this (asyncStreamName stream)

recordFileInfo :: WalConstraints env m
               => BackupContext x
               -> BackupFileName
               -> AsyncStreamResult
               -> m ()
recordFileInfo this name result
    | AsyncStreamFailed exc <- result = throwM exc
    | AsyncStreamFinished hash' <- result = do
        info <- atomically $ addToBackupFileStore (bcFileStore this)
            $ backupFileInfo name hash'
        logImportantN $ "Finished file: " <> display info

getStorageContext :: BackupContext x -> STM (StorageContext x)
getStorageContext this = readTVar (bcStorageContext this) >>= \case
    Just context -> pure context
    Nothing -> throwM $ protocolException "Backup context not initialized yet."

backupContextStartFile :: ContextConstraints env x m
                       => BackupContext x
                       -> BackupFileName
                       -> m AsyncStreamWriter
backupContextStartFile this name = do
    logMessageN $ "Backup context start file: " <> display name

    stream <- newAsyncStream name

    context <- atomically $ getStorageContext this

    lastAsync <- atomically $ swapTVar (bcAsync this) $ Just stream

    finalizeFile this lastAsync

    logMessageN $ "Backup context start file #1: " <> display name

    startAsyncStream stream $ writeBackupFile context name
        $ asyncStreamReader stream

    logMessageN $ "Backup context start file #2: " <> display name

    pure $ asyncStreamWriter stream

backupContextStartManifest
    :: ContextConstraints env x m => BackupContext x -> m AsyncStreamWriter
backupContextStartManifest this = do
    logMessageN "Backup context start manifest."
    backupContextStartFile this BackupManifestFileName

backupContextFinalize :: ContextConstraints env x m => BackupContext x -> m ()
backupContextFinalize this = do
    logMessageN "Backup context finalize."

    lastAsync <- atomically $ swapTVar (bcAsync this) Nothing

    finalizeFile this lastAsync

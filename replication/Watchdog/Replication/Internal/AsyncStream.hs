module Watchdog.Replication.Internal.AsyncStream
    ( AsyncStreamChunk(..)
    , AsyncStreamResult(..)
    , AsyncStream
    , AsyncStreamReader
    , AsyncStreamWriter
    , HasAsyncStream(..)
    , newAsyncStream
    , startAsyncStream
    , finishAsyncStream
    , finishAsyncStream_
    , waitForAsyncStream
    , tryWaitForAsyncStream
    , writeAsyncStream
    , writeAsyncStreamFinal
    , readAsyncStream
    , asyncStreamReader
    ) where

import           Baulig.Prelude                  hiding (hash)

import qualified Crypto.Hash.SHA256              as C

import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Connection
import           Watchdog.Replication.Exception

------------------------------------------------------------------------------
--- AsyncStreamResult
------------------------------------------------------------------------------

data AsyncStreamResult =
    AsyncStreamFinished !HashAndLength | AsyncStreamFailed !SomeException

instance Display AsyncStreamResult where
    display (AsyncStreamFinished hash) =
        display $ "AsyncStreamFinished" <:=> hash
    display (AsyncStreamFailed exc) = display $ "AsyncStreamFailed" <:=> exc

------------------------------------------------------------------------------
--- AsyncStreamChunk
------------------------------------------------------------------------------

data AsyncStreamChunk = RegularChunk !ByteString
                      | FinalChunk !HashAndLength
                      | ErrorChunk !SomeException

instance Display AsyncStreamChunk where
    display (RegularChunk (length -> len)) = display $ "RegularChunk" <:=> len
    display (FinalChunk hash) = display $ "FinalChunk" <:=> hash
    display (ErrorChunk exc) = display $ "ErrorChunk" <:=> exc

------------------------------------------------------------------------------
--- AsyncStream
------------------------------------------------------------------------------

data AsyncStream =
    AsyncStream { asName        :: !BackupFileName
                , asWrite       :: !(TMVar AsyncStreamChunk)
                , asWriteDone   :: !(TVar Bool)
                , asResult      :: !(TMVar AsyncStreamResult)
                , asAsync       :: !(TMVar (Async AsyncStreamResult))
                , asHashContext :: !(TVar C.Ctx)
                }

instance Display AsyncStream where
    display this = display $ this <:> asName this

newAsyncStream :: MonadIO m => BackupFileName -> m AsyncStream
newAsyncStream name = AsyncStream name <$> newEmptyTMVarIO <*> newTVarIO False
    <*> newEmptyTMVarIO <*> newEmptyTMVarIO <*> newTVarIO C.init

startAsyncStream :: WalConstraints env m => AsyncStream -> m () -> m ()
startAsyncStream stream func = do
    handler <- async $ tryAny func >>= finish
    atomically $ putTMVar (asAsync stream) handler
  where
    finish (Right _) = do
        logDebugN "Async operation finished."
        ctx <- readTVarIO $ asHashContext stream
        finishAsyncStream stream $ AsyncStreamFinished $ hashAndLength ctx

    finish (Left exc) = do
        $logError $ "Async operation threw an exception: " <> display exc
        finishAsyncStream_ stream $ AsyncStreamFailed exc
        throwM exc

finishAsyncStream
    :: MonadIO m => AsyncStream -> AsyncStreamResult -> m AsyncStreamResult
finishAsyncStream (asResult -> var) result = atomically $ do
    tryTakeTMVar var >>= \case
        Just existing -> pure existing
        Nothing -> putTMVar var result >> pure result

finishAsyncStream_ :: MonadIO m => AsyncStream -> AsyncStreamResult -> m ()
finishAsyncStream_ var = void . finishAsyncStream var

waitForAsyncStream :: MonadIO m => AsyncStream -> m AsyncStreamResult
waitForAsyncStream (asAsync -> var) = wait =<< atomically (takeTMVar var)

tryWaitForAsyncStream :: AsyncStream -> STM (Maybe AsyncStreamResult)
tryWaitForAsyncStream this = tryTakeTMVar (asResult this)

------------------------------------------------------------------------------
--- AsyncStreamReader
------------------------------------------------------------------------------

newtype AsyncStreamReader = AsyncStreamReader AsyncStream

readAsyncStream :: AsyncStreamReader -> STM AsyncStreamChunk
readAsyncStream (AsyncStreamReader stream) = takeTMVar $ asWrite stream

asyncStreamReader
    :: (HasAsyncStream x, MonadIO m) => x -> ConduitT () ByteString m ()
asyncStreamReader (AsyncStreamReader . asyncStream -> areader) = loop
  where
    loop = atomically (readAsyncStream areader) >>= \case
        FinalChunk _ -> pure ()
        RegularChunk chunk -> yield chunk >> loop
        ErrorChunk exc -> throwIO exc

------------------------------------------------------------------------------
--- AsyncStreamWriter
------------------------------------------------------------------------------

newtype AsyncStreamWriter = AsyncStreamWriter AsyncStream

instance Display AsyncStreamWriter where
    display (AsyncStreamWriter this) = display this

writeAsyncStreamInternal :: AsyncStream -> AsyncStreamChunk -> STM ()
writeAsyncStreamInternal this chunk = do
    done <- readTVar $ asWriteDone this
    if done
        then throwM stillWriting
        else tryTakeTMVar (asResult this) >>= \case
            Just (AsyncStreamFinished _) -> throwM stillWriting
            Just (AsyncStreamFailed exc) -> throwM exc
            Nothing -> do
                putTMVar (asWrite this) chunk
                when (isFinal chunk) $ writeTVar (asWriteDone this) True
  where
    isFinal (RegularChunk _) = False
    isFinal _ = True

    stillWriting =
        replicationException "Got StreamFinished result while still writing data."

writeAsyncStream :: MonadIO m => AsyncStreamWriter -> ByteString -> m ()
writeAsyncStream (AsyncStreamWriter this) chunk = do
    atomically $ do
        writeAsyncStreamInternal this $ RegularChunk chunk
        modifyTVar' (asHashContext this) $ flip C.update chunk

writeAsyncStreamFinal :: AsyncStreamWriter -> STM ()
writeAsyncStreamFinal (AsyncStreamWriter this) = do
    hash <- hashAndLength <$> readTVar (asHashContext this)
    writeAsyncStreamInternal this $ FinalChunk hash

------------------------------------------------------------------------------
--- HasAsyncStream
------------------------------------------------------------------------------

class HasAsyncStream x where
    asyncStream :: x -> AsyncStream

    asyncStreamName :: x -> BackupFileName
    asyncStreamName = asName . asyncStream

    asyncStreamWriter :: x -> AsyncStreamWriter
    asyncStreamWriter = AsyncStreamWriter . asyncStream

instance HasAsyncStream AsyncStream where
    asyncStream = id

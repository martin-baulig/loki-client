module Watchdog.Replication.Internal.StreamContext
    ( StreamHandle
    , StreamContext
    , newStreamContext
    , getCurrentAddress
    , writeChunk
    ) where

import           Baulig.Prelude
                 hiding (handle, hash)

import           Watchdog.Replication.Backend
import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Connection
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Internal.AsyncStream
import           Watchdog.Replication.Internal.StreamHandle
import           Watchdog.Replication.Protocol.CopyOut
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- StreamContext
------------------------------------------------------------------------------

data StreamContext =
    StreamContext { _streamContextEndVar    :: !(TVar (Maybe WalAddress))
                  , _streamContextFileStore :: !BackupFileStore
                  , _streamContextStart     :: !(TVar WalAddress)
                  , _streamContextWrite     :: !(TVar WalAddress)
                  , _streamContextFlush     :: !(TVar WalAddress)
                  , _streamContextHandle    :: !(TVar (Maybe StreamHandle))
                  }

newStreamContext :: MonadIO m
                 => WalAddress
                 -> TVar (Maybe WalAddress)
                 -> BackupFileStore
                 -> m StreamContext
newStreamContext start endAddress fileStore =
    StreamContext endAddress fileStore <$> newTVarIO start <*> newTVarIO start
    <*> newTVarIO start <*> newTVarIO Nothing

getCurrentAddress :: MonadIO m => StreamContext -> m (WalAddress, WalAddress)
getCurrentAddress context = (,) <$> readTVarIO (_streamContextWrite context)
    <*> readTVarIO (_streamContextFlush context)

------------------------------------------------------------------------------
--- New Write chunk
------------------------------------------------------------------------------

data WriteState = NewHandle !StreamHandle
                | NormalWrite !WalAddress
                | FinishHandle !StreamHandle !WalAddress
                | FinishWithLeftover !StreamHandle !WalAddress !XLogChunk

instance Display WriteState where
    display (NewHandle position) = display $ "NewHandle" <:=> position
    display (NormalWrite position) = display $ "NormalWrite" <:=> position
    display (FinishHandle _ next) = display $ "FinishHandle" <:=> next
    display (FinishWithLeftover _ next rest) =
        display $ "FinishWithLeftOver" <:=> next <+> rest

ensureCorrectPosition
    :: (MonadThrow m, HasWalAddress e, HasWalAddress a, HasCallStack)
    => e
    -> a
    -> m ()
ensureCorrectPosition (walAddress -> expected) (walAddress -> position)
    | position == expected = pure ()
    | otherwise =
        throwM $ protocolException $ "Invalid chunk position; expected "
        <> display position <> ", but got " <> display expected <> "."

ensureAtSegmentStart
    :: (MonadThrow m, HasWalAddress x, HasCallStack) => x -> m ()
ensureAtSegmentStart (walAddress -> position)
    | walAddressIsAtSegmentStart position = pure ()
    | otherwise = throwM $ protocolException
        $ "First segment is not at the beginning of a WAL file: "
        <> display position

beginWrite :: (MonadResource m, WalConstraints env m)
           => StreamContext
           -> XLogChunk
           -> m WriteState
beginWrite context chunk = readTVarIO var >>= \case
    Nothing -> do
        ensureAtSegmentStart chunk
        handle <- newHandle fileName
        atomically $ writeTVar var $ Just handle
        pure $ NewHandle handle

    Just handle -> do
        do
            ensureCorrectPosition handle chunk

            let available = fromIntegral $ walAddressSegmentRemaining handle
            let (body, rest) = splitAt available $ xlogChunkBody chunk

            writeAsyncStream (asyncStreamWriter handle) body

            continue handle body rest
  where
    var = _streamContextHandle context

    fileName = walFileAddress (defaultTimeline, walAddress chunk)

    continue handle body rest
        | not $ null rest = do
            logDebugN $ "Final chunk with leftover: " <> tshow (length rest)
            atomically $ do
                writeAsyncStreamFinal $ asyncStreamWriter handle
                writeTVar var Nothing
            pure $ FinishWithLeftover handle nextFile $ XLogChunk nextFile rest
        | not $ walAddressIsAtSegmentStart next = do
            logDebugN $ "Regular chunk: " <> display handle <> " => "
                <> display next <> " / " <> display (walAddress next)
            atomically $ writeTVar var $ Just next
            pure $ NormalWrite $ walAddress next
        | otherwise = do
            logDebugN "Final chunk."
            atomically $ do
                writeAsyncStreamFinal $ asyncStreamWriter handle
                writeTVar var Nothing
            logDebugN "Final chunk #1."
            pure $ FinishHandle handle nextFile
      where
        next = advanceWalAddress handle $ length body

        nextFile = advanceWalAddressToNextFile $ walAddress handle

writeChunk :: (MonadResource m, WalConstraints env m, Backend x)
           => StreamContext
           -> StorageContext x
           -> XLogChunk
           -> m StreamStatus
writeChunk context backend chunk = beginWrite context chunk >>= \case
    NormalWrite position -> do
        atomically $ writeTVar (_streamContextWrite context) position
        pure KeepGoing
    NewHandle handle -> do
        maybeEnd <- readTVarIO (_streamContextEndVar context)
        startNewHandle handle maybeEnd
    FinishHandle handle next -> do
        finishStream handle next
        pure KeepGoing
    FinishWithLeftover handle next rest -> do
        finishStream handle next
        writeChunk context backend rest
  where
    startNewHandle handle maybeEnd
        | Just end <- maybeEnd, end <= walAddress handle = do
            logVerboseN $ "New WAL file - at end: " <> display handle <> " "
                <> display end
            pure Finished

        | otherwise = do
            logMessageN $ "New WAL file: " <> display handle

            startAsyncStream (asyncStream handle)
                $ streamWalFile backend (walAddress handle)
                $ asyncStreamReader handle

            writeChunk context backend chunk

    finishStream handle next = do
        logMessageN $ "Finish WAL file: " <> display handle
        result <- waitForAsyncStream $ asyncStream handle
        logMessageN $ "Got result: " <> display result <> " " <> display next
        case result of
            AsyncStreamFailed exc -> throwM exc
            AsyncStreamFinished hash -> do
                atomically $ do
                    writeTVar (_streamContextWrite context) next
                    writeTVar (_streamContextFlush context) next
                info <- atomically
                    $ addToBackupFileStore (_streamContextFileStore context)
                    $ backupFileInfo (backupFileName handle) hash
                logNormalN $ "Finished WAL file "
                    <> display (walFileAddress handle) <> " " <> display info
                    <> ", next is " <> display next <> "."

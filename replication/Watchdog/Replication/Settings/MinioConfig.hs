module Watchdog.Replication.Settings.MinioConfig
    ( MinioConfig
    , HasMinioConfig(..)
    , resolveMinioConfig
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Yaml                   as Y

import           Network.Minio
import           Network.URI                 (URI, parseURI)

import           Watchdog.Settings.AppConfig

------------------------------------------------------------------------------
--- MinioURI
------------------------------------------------------------------------------

newtype MinioURI = MinioURI URI
    deriving stock (Eq, Show, Generic)

instance FromJSON MinioURI where
    parseJSON = coerce <$> withText "URI" $ \v -> do
        let maybeURI = parseURI $ unpack v
        maybe (error "Bad URI") pure maybeURI

------------------------------------------------------------------------------
--- MinioConfig
------------------------------------------------------------------------------

data MinioConfig = MinioConfig { mcUri       :: !MinioURI
                               , mcBucket    :: !Bucket
                               , mcAccessKey :: !AccessKey
                               , mcSecretKey :: !SecretKey
                               }
    deriving (Eq, Generic, Show)

instance Display MinioConfig where
    display this = display $ this <:!> mcUri this <+> mcBucket this

instance FromJSON MinioConfig where
    parseJSON = Y.withObject "MinioConfig" $ \v -> MinioConfig <$> v .: "uri"
        <*> v .: "bucket" <*> (AccessKey <$> v .: "accessKey")
        <*> (SecretKey . fromString <$> v .: "secretKey")

resolveMinioConfig :: MonadAppConfig m => m MinioConfig
resolveMinioConfig = getAppConfigFile "minio-config.yaml" >>= Y.decodeFileThrow

------------------------------------------------------------------------------
--- HasMinioConfig
------------------------------------------------------------------------------

class HasMinioConfig env where
    minioConfigL :: Lens' env MinioConfig

    minioConfigUri :: SimpleGetter env URI
    minioConfigUri = minioConfigL . to (coerce mcUri)

    minioConfigBucket :: SimpleGetter env Bucket
    minioConfigBucket = minioConfigL . to mcBucket

    minioConfigAccessKey :: SimpleGetter env AccessKey
    minioConfigAccessKey = minioConfigL . to mcAccessKey

    minioConfigSecretKey :: SimpleGetter env SecretKey
    minioConfigSecretKey = minioConfigL . to mcSecretKey

instance HasMinioConfig MinioConfig where
    minioConfigL = id

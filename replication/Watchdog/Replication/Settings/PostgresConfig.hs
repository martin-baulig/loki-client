module Watchdog.Replication.Settings.PostgresConfig
    ( PostgresConfig
    , HasPostgresConfig(..)
    , BaseBackupTempPath
    , resolvePostgresConfig
    , postgresBaseBackupCommandLine
    , postgresStreamWalCommandLine
    , baseBackupTempPath
    , createBaseBackupTempPath
    ) where

import           Baulig.Prelude

import           Data.Aeson                           hiding (Key)
import qualified Data.Yaml                            as Y

import           Database.Persist.Sqlite

import           Network.Socket

import           UnliftIO.Directory
import           UnliftIO.Process

import           Watchdog.Common.Sockets
import           Watchdog.Replication.Database.Backup
import           Watchdog.Settings.PostgresSettings
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- PostgresConfig
------------------------------------------------------------------------------

data PostgresConfig =
    PostgresConfig { pgcHostAddress :: !DaemonHostAddress
                   , pgcHostName    :: !(Maybe HostName)
                   , pgcTls         :: !(Maybe TlsSettings)
                   , pgcUserName    :: !String
                   , pgcToolPath    :: !FilePath
                   , pgcTempPath    :: !FilePath
                   }
    deriving stock (Eq, Generic, Show)

instance FromJSON PostgresConfig where
    parseJSON (Y.Object v) = PostgresConfig <$> v .: "hostAddress"
        <*> v .:? "hostName" <*> v .:? "tls" <*> v .: "userName"
        <*> v .: "toolPath" <*> v .: "tempPath"
    parseJSON _ = error "Invalid PostgresConfig."

resolvePostgresConfig :: MonadIO m => FilePath -> m PostgresConfig
resolvePostgresConfig = Y.decodeFileThrow

instance HasPostgresSettings PostgresConfig where
    pgConnectionString c = unwords
        $ [ "user=" ++ pgcUserName c
          , "host=" ++ host
          , "port=" ++ show (getPortNumber address)
          ] ++ [pgConnectionString tls | Just tls <- [pgcTls c]]
      where
        address = pgcHostAddress c

        host = fromMaybe (printHostAddress address) $ pgcHostName c

------------------------------------------------------------------------------
--- BaseBackupTempPath
------------------------------------------------------------------------------

newtype BaseBackupTempPath = BaseBackupTempPath FilePath
    deriving stock (Eq, Generic, Show)

instance Display BaseBackupTempPath where
    display (BaseBackupTempPath path) =
        "[BaseBackupTempPath " <> fromString path <> "]"

baseBackupTempPath :: PostgresConfig -> Key Backup -> BaseBackupTempPath
baseBackupTempPath config (unSqlBackendKey . unBackupKey -> key) =
    BaseBackupTempPath $ pgcTempPath config ++ "/base-backup/" ++ show key

createBaseBackupTempPath :: MonadIO m => BaseBackupTempPath -> m ()
createBaseBackupTempPath (BaseBackupTempPath path) =
    createDirectoryIfMissing True path

------------------------------------------------------------------------------
--- HasPostgresConfig
------------------------------------------------------------------------------

class HasPostgresConfig x where
    postgresConfigL :: Lens' x PostgresConfig

instance HasPostgresConfig PostgresConfig where
    postgresConfigL = id

postgresBaseBackupCommandLine
    :: PostgresConfig -> BaseBackupTempPath -> CreateProcess
postgresBaseBackupCommandLine c (BaseBackupTempPath temp) =
    let p = proc path args
    in
        p { env       = Just []
          , std_in    = NoStream
          , std_out   = CreatePipe
          , std_err   = CreatePipe
          , close_fds = True
          }
  where
    path = pgcToolPath c ++ "/bin/pg_basebackup"

    tlsArgs
        | Just tls <- pgcTls c = ["-d", pgConnectionString tls]
        | otherwise = mempty

    args = [ "-D"
           , temp
           , "-h"
           , printHostAddress $ pgcHostAddress c
           , "-p"
           , show $ getPortNumber $ pgcHostAddress c
           , "-U"
           , pgcUserName c
           , "-X"
           , "stream"
           , "-F"
           , "t"
           , "-v"
           ] <> tlsArgs

postgresStreamWalCommandLine
    :: PostgresConfig -> BaseBackupTempPath -> CreateProcess
postgresStreamWalCommandLine c (BaseBackupTempPath temp) =
    let p = proc path args
    in
        p { env       = Just []
          , std_in    = NoStream
          , std_out   = CreatePipe
          , std_err   = CreatePipe
          , close_fds = True
          }
  where
    path = pgcToolPath c ++ "/bin/pg_receivewal"

    tlsArgs
        | Just tls <- pgcTls c = ["-d", pgConnectionString tls]
        | otherwise = mempty

    args = [ "-D"
           , temp
           , "-h"
           , printHostAddress $ pgcHostAddress c
           , "-p"
           , show $ getPortNumber $ pgcHostAddress c
           , "-U"
           , pgcUserName c
           , "-S"
           , "test"
           , "--synchronous"
           , "-n"
           , "-v"
           ] <> tlsArgs

module Watchdog.Replication.Settings.WalConfig
    ( WalConfig(..)
    , HasWalConfig(..)
    , resolveWalConfig
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Yaml                                as Y

import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Settings.AppConfig

------------------------------------------------------------------------------
--- WalConfig
------------------------------------------------------------------------------

data WalConfig = WalConfig { walConfigReplicationSlot  :: !ReplicationSlotName
                           , walConfigStorageDirectory :: !FilePath
                           , walConfigBackupDirectory  :: !FilePath
                           , walConfigTempDirectory    :: !FilePath
                           , walConfigTimeline         :: !(Maybe TimelineId)
                           , walConfigDatabasePath     :: !FilePath
                           }
    deriving (Eq, Generic, Show)

instance FromJSON WalConfig where
    parseJSON =
        Y.withObject "WalConfig" $ \v -> WalConfig <$> v .: "replicationSlot"
        <*> v .: "storageDirectory" <*> v .: "backupDirectory"
        <*> v .: "tempDirectory" <*> v .:? "timeline" <*> v .: "database"

instance Display WalConfig where
    display this = display $ this <:!> walConfigStorageDirectory this
        <+!> walConfigTempDirectory this <+> walConfigReplicationSlot this
        <+> walConfigTimeline this

resolveWalConfig :: MonadAppConfig m => m WalConfig
resolveWalConfig = getAppConfigFile "wal-config.yaml" >>= Y.decodeFileThrow

------------------------------------------------------------------------------
--- HasWalConfig
------------------------------------------------------------------------------

class HasWalConfig env where
    walConfigL :: Lens' env WalConfig

instance HasWalConfig WalConfig where
    walConfigL = id

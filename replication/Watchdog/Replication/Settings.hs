module Watchdog.Replication.Settings (module Import) where

import           Watchdog.Replication.Settings.MinioConfig    as Import
import           Watchdog.Replication.Settings.PostgresConfig as Import
import           Watchdog.Replication.Settings.WalConfig      as Import

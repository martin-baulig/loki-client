module Watchdog.Replication.Types
    ( ReplicationContext
    , HasReplicationContext(..)
    , ReplicationT
    , runReplicationT
    , runRecursiveReplicationT
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.Database
import           Watchdog.Replication.Settings.MinioConfig
import           Watchdog.Replication.Settings.PostgresConfig
import           Watchdog.Replication.Settings.WalConfig

------------------------------------------------------------------------------
--- ReplicationContext
------------------------------------------------------------------------------

data ReplicationContext =
    ReplicationContext { rcDatabase       :: !ReplicationDatabase
                       , rcPostgresConfig :: !PostgresConfig
                       , rcMinioConfig    :: !MinioConfig
                       , rcWalConfig      :: !WalConfig
                       }

class (HasPostgresConfig x, HasWalConfig x) => HasReplicationContext x where
    replicationContextL :: Lens' x ReplicationContext

instance HasReplicationContext ReplicationContext where
    replicationContextL = id

instance HasPostgresConfig ReplicationContext where
    postgresConfigL =
        lens rcPostgresConfig (\x y -> x { rcPostgresConfig = y })

instance HasMinioConfig ReplicationContext where
    minioConfigL = lens rcMinioConfig (\x y -> x { rcMinioConfig = y })

instance HasWalConfig ReplicationContext where
    walConfigL = lens rcWalConfig (\x y -> x { rcWalConfig = y })

------------------------------------------------------------------------------
--- ReplicationT
------------------------------------------------------------------------------

newtype ReplicationT (m :: Type -> Type) x =
    ReplicationT (ReaderT ReplicationContext (ResourceT m) x)
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadThrow, MonadResource, MonadReader ReplicationContext
                    , MonadLogger)

instance MonadUnliftIO m => MonadReplicationDatabase (ReplicationT m) where
    runReplicationDatabase action = do
        ctx <- ask
        runDatabaseOp (rcDatabase ctx) action

runReplicationT
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , MonadReader env m
       , HasPostgresConfig env
       , HasMinioConfig env
       , HasWalConfig env
       )
    => DatabasePath
    -> LogSource
    -> ReplicationT m x
    -> m x
runReplicationT path source (ReplicationT func) = do
    withReplicationDatabase path $ \database -> do
        logVerboseNS source $ "Run replication, using database " <> tshow path
        ctx <- ReplicationContext database <$> view postgresConfigL
            <*> view minioConfigL <*> view walConfigL
        result <- runResourceT $ runReaderT func ctx
        logVerboseNS source "Run replication done."
        pure result

runRecursiveReplicationT
    :: MonadLogger m => LogSource -> ReplicationT m x -> ReplicationT m x
runRecursiveReplicationT source func = do
    logVerboseNS source "Run recursive replication."
    result <- func
    logVerboseNS source "Run recursive replication done."
    pure result

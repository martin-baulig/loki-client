module Watchdog.Replication.ScanBackups (runBackupScan) where

import           Baulig.Prelude

import           Watchdog.Replication.Backend
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.IdentifySystem
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Replication.Task
import           Watchdog.Utils.ConduitUtils

------------------------------------------------------------------------------
--- ScanBackups
------------------------------------------------------------------------------

runBackupScan :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
              => ReplicationTaskT m ()
runBackupScan = do
    system <- identifySystem
    logNormalN $ "Identify system: " <> display system
    unless (isNothing $ identifySystemDatabase system) $ throwIO
        $ replicationException "Not a replication connection."

    config <- view walConfigL

    let backend = fileBackend config

    logNormalN $ "Using backend: " <> display backend

    runConduitRes $ scanBackups backend .| displaySink "Backup scan"

module Watchdog.Replication.BackupInfo
    ( BackupInfo(..)
    , createBackupInfo
    , module Watchdog.Replication.BackupFile
    ) where

import           Baulig.Prelude

import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- BackupInfo
------------------------------------------------------------------------------

data BackupInfo = BackupInfo { backupInfoGeneration :: !PostgresTimestamp
                             , backupInfoTimeline   :: !TimelineId
                             , backupInfoStart      :: !WalAddress
                             , backupInfoEnd        :: !WalAddress
                             , backupInfoFiles      :: ![BackupFileInfo]
                             , backupInfoWalFiles   :: ![BackupFileInfo]
                             }
    deriving stock Generic
    deriving (FromJSON, ToJSON)

instance Display BackupInfo where
    display this = display $ this <:> backupInfoGeneration this
        <@> backupInfoFiles this <@> backupInfoWalFiles this

createBackupInfo :: BackupGeneration
                 -> WalAddress
                 -> WalAddress
                 -> [BackupFileInfo]
                 -> [BackupFileInfo]
                 -> BackupInfo
createBackupInfo generation start end files walFiles =
    BackupInfo { backupInfoGeneration = backupGenerationTimestamp generation
               , backupInfoTimeline   = timelineId generation
               , backupInfoStart      = start
               , backupInfoEnd        = end
               , backupInfoFiles      = files
               , backupInfoWalFiles   = walFiles
               }

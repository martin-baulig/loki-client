module Watchdog.Replication.Exception
    ( ReplicationException
    , getResultException
    , getConnectionException
    , isShutdownRequestedException
    , isConnectionClosedException
    , backupException
    , protocolException
    , replicationException
    , connectionBusyException
    , shutdownRequestedException
    , connectionClosedException
    , expectResultStatus
    ) where

import           Baulig.Prelude

import           Database.PostgreSQL.LibPQ

import           GHC.Exception
import           GHC.Stack

------------------------------------------------------------------------------
--- ReplicationException
------------------------------------------------------------------------------

data ReplicationException where
    ConnectionException
        :: HasCallStack => !ByteString -> CallStack -> ReplicationException
    ReplicationException
        :: HasCallStack => !ByteString -> CallStack -> ReplicationException
    BackupException
        :: HasCallStack => !Text -> CallStack -> ReplicationException
    ProtocolException
        :: HasCallStack => !Text -> CallStack -> ReplicationException
    ConnectionBusyException
        :: HasCallStack => CallStack -> ReplicationException
    ShutdownRequestedException
        :: HasCallStack => CallStack -> ReplicationException
    ConnectionClosedException
        :: HasCallStack => CallStack -> ReplicationException
    UnknownException :: HasCallStack => CallStack -> ReplicationException

instance Exception ReplicationException

instance Show ReplicationException where
    show = unpack . display

instance Display ReplicationException where
    display (ConnectionException msg stack) = "[ConnectionException "
        <> decodeUtf8 msg <> "]\n" <> pack (prettyCallStack stack) <> "\n"
    display (ReplicationException msg stack) = "[ReplicationException "
        <> decodeUtf8 msg <> "]\n" <> pack (prettyCallStack stack) <> "\n"
    display (ProtocolException msg stack) = "[ProtocolException " <> display msg
        <> "]\n" <> pack (prettyCallStack stack) <> "\n"
    display (BackupException msg stack) = "[BackupException " <> display msg
        <> "]\n" <> pack (prettyCallStack stack) <> "\n"
    display (ConnectionBusyException stack) = "[ConnectionBusyException]\n"
        <> pack (prettyCallStack stack) <> "\n"
    display (ShutdownRequestedException stack) =
        "[ShutdownRequestedException]\n" <> pack (prettyCallStack stack) <> "\n"
    display (ConnectionClosedException stack) = "[ConnectionClosedException]\n"
        <> pack (prettyCallStack stack) <> "\n"
    display (UnknownException stack) = "[UnknownExpection]"
        <> pack (prettyCallStack stack) <> "\n"

isShutdownRequestedException :: ReplicationException -> Bool
isShutdownRequestedException (ShutdownRequestedException _) = True
isShutdownRequestedException _ = False

isConnectionClosedException :: ReplicationException -> Bool
isConnectionClosedException (ConnectionClosedException _) = True
isConnectionClosedException _ = False

backupException :: HasCallStack => Text -> ReplicationException
backupException msg = BackupException msg callStack

protocolException :: HasCallStack => Text -> ReplicationException
protocolException msg = ProtocolException msg callStack

replicationException :: HasCallStack => ByteString -> ReplicationException
replicationException msg = ReplicationException msg callStack

connectionBusyException :: HasCallStack => ReplicationException
connectionBusyException = ConnectionBusyException callStack

shutdownRequestedException :: HasCallStack => ReplicationException
shutdownRequestedException = ShutdownRequestedException callStack

connectionClosedException :: HasCallStack => ReplicationException
connectionClosedException = ConnectionClosedException callStack

getResultException
    :: (MonadIO m, HasCallStack) => Result -> m ReplicationException
getResultException result = exc <$> liftIO (resultErrorMessage result)
  where
    exc Nothing = UnknownException callStack
    exc (Just err) = ReplicationException err callStack

getConnectionException
    :: (MonadIO m, HasCallStack) => Connection -> m ReplicationException
getConnectionException connection = exc <$> liftIO (errorMessage connection)
  where
    exc Nothing = UnknownException callStack
    exc (Just err) = ConnectionException err callStack

expectResultStatus
    :: (MonadIO m, HasCallStack) => Result -> ExecStatus -> m x -> m x
expectResultStatus result expected func = liftIO (resultStatus result) >>= check
  where
    check s
        | s == expected = func
    check _ = throwIO =<< getResultException result


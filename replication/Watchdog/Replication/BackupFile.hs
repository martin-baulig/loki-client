module Watchdog.Replication.BackupFile
    ( BackupFileName(..)
    , BackupFileInfo(..)
    , HasBackupFileName(..)
    , BackupFileStore
    , HashAndLength
    , addToBackupFileStore
    , backupFileInfo
    , backupFileStoreGetFiles
    , hashAndLength
    , newBackupFileStore
    ) where

import           Baulig.Prelude         hiding (hash)

import qualified Crypto.Hash.SHA256     as C

import           Data.Aeson
import qualified Data.ByteString.Base16 as B16

------------------------------------------------------------------------------
--- BackupFileName
------------------------------------------------------------------------------

data BackupFileName = BackupBaseFileName !Text
                    | BackupTableFileName !Text
                    | BackupManifestFileName
                    | BackupInfoFileName
                    | WalFileName !Text
    deriving stock (Eq, Generic, Show)

instance Display BackupFileName where
    display (BackupBaseFileName name) = display $ "BackupBaseFileName" <:=> name
    display (BackupTableFileName name) =
        display $ "BackupTableFileName" <:=> name
    display BackupManifestFileName = "[BackupManifestFileName]"
    display (WalFileName name) = display $ "WalFileName" <:=> name
    display BackupInfoFileName = "[BackupInfoFileName]"

instance ToJSON BackupFileName where
    toJSON this =
        object ["tag" .= backupFileType this, "name" .= rawBackupFileName this]

instance FromJSON BackupFileName where
    parseJSON = withObject "BackupFileName"
        $ \o -> (ctor <$> o .: "tag" <*> o .: "name") >>= \case
            Just name -> pure name
            Nothing -> error "Invalid BackupFileName."
      where
        ctor :: Text -> Text -> Maybe BackupFileName
        ctor tag name = case tag of
            "BackupBaseFileName" -> Just $ BackupBaseFileName name
            "BackupTableFileName" -> Just $ BackupTableFileName name
            "BackupManifestFileName" -> Just BackupManifestFileName
            "WalFileName" -> Just $ WalFileName name
            "BackupInfoFileName" -> Just $ BackupInfoFileName
            _ -> Nothing

class HasBackupFileName x where
    backupFileName :: x -> BackupFileName

    rawBackupFileName :: x -> FilePath
    rawBackupFileName (backupFileName -> this)
        | BackupBaseFileName name <- this = unpack name
        | BackupTableFileName name <- this = unpack name
        | BackupManifestFileName <- this = "manifest"
        | WalFileName name <- this = unpack name
        | BackupInfoFileName <- this = "backup-info.yaml"

    backupFileType :: x -> Text
    backupFileType (backupFileName -> this)
        | BackupBaseFileName _ <- this = "BackupBaseFileName"
        | BackupTableFileName _ <- this = "BackupTableFileName"
        | BackupManifestFileName <- this = "BackupManifestFileName"
        | WalFileName _ <- this = "WalFileName"
        | BackupInfoFileName <- this = "BackupInfoFileName"

instance HasBackupFileName BackupFileName where
    backupFileName = id

------------------------------------------------------------------------------
--- HashAndLength
------------------------------------------------------------------------------

data HashAndLength = HashAndLength !ByteString !Word64

instance Display HashAndLength where
    display this
        | HashAndLength hash len <- this = display $ this <:> len
            <:> decodeUtf8 (B16.encode hash)

hashAndLength :: C.Ctx -> HashAndLength
hashAndLength ctx = uncurry HashAndLength $ C.finalizeAndLength ctx

------------------------------------------------------------------------------
--- BackupFileInfo
------------------------------------------------------------------------------

data BackupFileInfo = BackupFileInfo !BackupFileName !Word64 !Text
    deriving stock (Eq, Generic, Show)

instance Display BackupFileInfo where
    display this
        | BackupFileInfo name len hash
          <- this = display $ this <:> name <+> len <+> hash

instance ToJSON BackupFileInfo where
    toJSON (BackupFileInfo name len hash) =
        object ["fileName" .= name, "size" .= len, "sha256" .= hash]

instance FromJSON BackupFileInfo where
    parseJSON = withObject "BackupFileInfo" $ \o ->
        BackupFileInfo <$> o .: "fileName" <*> o .: "size" <*> o .: "sha256"

backupFileInfo :: BackupFileName -> HashAndLength -> BackupFileInfo
backupFileInfo name (HashAndLength hash len) =
    BackupFileInfo name len $ decodeUtf8 $ B16.encode hash

------------------------------------------------------------------------------
--- BackupFileStore
------------------------------------------------------------------------------

newtype BackupFileStore = BackupFileStore (TVar [BackupFileInfo])

newBackupFileStore :: MonadIO m => m BackupFileStore
newBackupFileStore = BackupFileStore <$> newTVarIO mempty

addToBackupFileStore :: BackupFileStore -> BackupFileInfo -> STM BackupFileInfo
addToBackupFileStore (BackupFileStore var) info = modifyTVar' var (info :)
    >> pure info

backupFileStoreGetFiles :: BackupFileStore -> STM [BackupFileInfo]
backupFileStoreGetFiles (BackupFileStore var) = readTVar var

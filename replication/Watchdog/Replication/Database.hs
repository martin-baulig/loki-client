module Watchdog.Replication.Database
    ( ReplicationDatabase
    , MonadReplicationDatabase(..)
    , withReplicationDatabase
      -- Re-exports
    , module Watchdog.Vault.Database.Types
    , module Watchdog.Replication.Database.Slot
    , module Watchdog.Replication.Database.Generation
    , entityKey
    , entityVal
    , Entity
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Database.Persist.Class
import           Database.Persist.Sqlite

import           Watchdog.Replication.Database.Generation
import           Watchdog.Replication.Database.Slot
import           Watchdog.Vault.Database.Types

------------------------------------------------------------------------------
--- ReplicationDatabase
------------------------------------------------------------------------------

class MonadUnliftIO m => MonadReplicationDatabase m where
    runReplicationDatabase :: ReaderT SqlBackend IO x -> m x

data ReplicationTag

type ReplicationDatabase = Tagged ReplicationTag Database

withReplicationDatabase :: (MonadUnliftIO m, MonadLogger m)
                        => DatabasePath
                        -> (ReplicationDatabase -> m x)
                        -> m x
withReplicationDatabase path = withDatabase (Tagged @ReplicationTag path) $ do
    runMigration migrateGeneration
    runMigration migrateSlot

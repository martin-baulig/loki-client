{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Replication.Database.Backup where

import           Baulig.Prelude

import           Database.Persist.Sqlite
import           Database.Persist.TH

------------------------------------------------------------------------------
--- BaseBackup
------------------------------------------------------------------------------

data BackupStatus = Running | Uploading | Success | Failed
    deriving stock (Enum, Eq, Generic, Read, Show)
    deriving (FromJSON, ToJSON)

derivePersistField "BackupStatus"

share [mkPersist sqlSettings, mkMigrate "migrateBackup"]
      [persistLowerCase|
Backup
    status BackupStatus
    startTime UTCTime
    endTime UTCTime Maybe
    deriving Show
    deriving Generic
    deriving FromJSON
    deriving ToJSON
|]

mkRunningBackup :: MonadIO m => m Backup
mkRunningBackup = ctor <$> liftIO getCurrentTime
  where
    ctor now = Backup Running now Nothing

instance Display (Key Backup) where
    display (keyToValues -> [ PersistInt64 values ]) = "#" <> tshow values
    display key = "[BackupKey " <> tshow key <> "]"

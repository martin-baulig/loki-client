{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Replication.Database.Generation where

import           Baulig.Prelude

import           Database.Persist.Sqlite
import           Database.Persist.TH

import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- Generation
------------------------------------------------------------------------------

data GenerationStatus = Unknown | Closed | Pending | Active
    deriving stock (Enum, Eq, Generic, Read, Show)
    deriving (FromJSON, ToJSON)

instance Display GenerationStatus where
    display = tshow

derivePersistField "GenerationStatus"

share [mkPersist sqlSettings, mkMigrate "migrateGeneration"]
      [persistLowerCase|
Generation
    created PostgresTimestamp
    updated PostgresTimestamp
    name Text Maybe
    slotName Text Maybe
    status GenerationStatus
    start Word64
    position Word64
    backupStart Word64 Maybe
    backupEnd Word64 Maybe
    timeline Word32
    UniqueCreated created
    deriving Eq
    deriving Show
    deriving Generic
    deriving FromJSON
    deriving ToJSON
|]

mkGeneration :: MonadIO m
             => Maybe Text
             -> WalFileAddress
             -> ReplicationSlotName
             -> m Generation
mkGeneration name address slot =
    mkGeneration' name address slot <$> currentTimestamp

mkGeneration' :: Maybe Text
              -> WalFileAddress
              -> ReplicationSlotName
              -> PostgresTimestamp
              -> Generation
mkGeneration' name address slot now =
    Generation { generationCreated     = now
               , generationUpdated     = now
               , generationName        = name
               , generationSlotName    = Just $ replicationSlotRawName slot
               , generationStatus      = Pending
               , generationStart       = walAddressRaw address
               , generationPosition    = walAddressRaw address
               , generationBackupStart = Nothing
               , generationBackupEnd   = Nothing
               , generationTimeline    = rawTimelineId address
               }

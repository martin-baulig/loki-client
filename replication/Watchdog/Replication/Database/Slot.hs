{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Replication.Database.Slot where

import           Baulig.Prelude

import           Database.Persist.Sqlite
import           Database.Persist.TH

import           Watchdog.Replication.Protocol.WalAddress

------------------------------------------------------------------------------
--- Generation
------------------------------------------------------------------------------

share [mkPersist sqlSettings, mkMigrate "migrateSlot"]
      [persistLowerCase|
Slot
    name Text
    start Word64
    position Word64
    deriving Eq
    deriving Show
    deriving Generic
    deriving FromJSON
    deriving ToJSON
|]

mkSlot :: Text -> WalAddress -> Slot
mkSlot name address = Slot { slotName     = name
                           , slotStart    = walAddressRaw address
                           , slotPosition = walAddressRaw address
                           }

instance Display Slot where
    display this = display $ this <:> slotName this
        <+> walAddress (slotStart this) <+> walAddress (slotPosition this)

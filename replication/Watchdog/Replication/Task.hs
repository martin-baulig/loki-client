module Watchdog.Replication.Task
    ( ReplicationTask
    , HasReplicationTask(..)
    , ReplicationTaskT
    , ReplicationOperation
    , runReplicationTaskT
    , liftReplicationTaskT
    , runWithConnection
    , withConnection
    , startReplicationOperation
    , startRecursiveReplicationOperation
    , cancelReplicationOperation
    , finishReplicationOperation
    , waitForReplicationOperation
    , module Watchdog.Replication.Types
    ) where

import           Baulig.Prelude

import           Database.PostgreSQL.LibPQ

import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Database
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Settings.MinioConfig
import           Watchdog.Replication.Settings.PostgresConfig
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Replication.Types
import           Watchdog.Utils.AsyncOperation

------------------------------------------------------------------------------
--- ReplicationTask
------------------------------------------------------------------------------

data ReplicationTask = ReplicationTask { rtContext    :: !ReplicationContext
                                       , rtConnection :: !ReplicationConnection
                                       }

class HasReplicationContext x => HasReplicationTask x where
    replicationTaskL :: Lens' x ReplicationTask

instance HasReplicationTask ReplicationTask where
    replicationTaskL = id

instance HasReplicationContext ReplicationTask where
    replicationContextL = lens rtContext (\x y -> x { rtContext = y })

instance HasMinioConfig ReplicationTask where
    minioConfigL = replicationContextL . minioConfigL

instance HasWalConfig ReplicationTask where
    walConfigL = replicationContextL . walConfigL

instance HasPostgresConfig ReplicationTask where
    postgresConfigL = replicationContextL . postgresConfigL

------------------------------------------------------------------------------
--- ReplicationTask Monad
------------------------------------------------------------------------------

newtype ReplicationTaskT (m :: Type -> Type) x =
    ReplicationTaskT (ReaderT ReplicationTask (ReplicationT m) x)
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadThrow, MonadResource, MonadReader ReplicationTask
                    , MonadLogger)

instance MonadUnliftIO m => MonadReplicationDatabase (ReplicationTaskT m) where
    runReplicationDatabase = liftReplicationTaskT . runReplicationDatabase

runReplicationTaskT :: (MonadUnliftIO m, MonadThrow m, HasCallStack)
                    => (HasCallStack => ReplicationTaskT m x)
                    -> ReplicationT m x
runReplicationTaskT func = do
    settings <- view postgresConfigL
    connection <- connectToReplication settings
    runWithConnection connection func

runWithConnection :: (MonadUnliftIO m, MonadThrow m, HasCallStack)
                  => ReplicationConnection
                  -> (HasCallStack => ReplicationTaskT m x)
                  -> ReplicationT m x
runWithConnection connection (ReplicationTaskT func) =
    bracket createTask freeTask $ runReaderT func
  where
    createTask = flip ReplicationTask connection <$> ask

    freeTask task = closeReplicationConnection $ rtConnection task

liftReplicationTaskT
    :: MonadUnliftIO m => ReplicationT m x -> ReplicationTaskT m x
liftReplicationTaskT func = ReplicationTaskT $ lift func

withConnection
    :: MonadUnliftIO m => (Connection -> IO x) -> ReplicationTaskT m x
withConnection func = do
    connection <- asks rtConnection
    liftReplicationTaskT $ withReplicationConnection connection func

------------------------------------------------------------------------------
--- ReplicationOperation
------------------------------------------------------------------------------

newtype ReplicationOperation =
    ReplicationOperation (AsyncOperation ReplicationConnection
                                         ReplicationResponse)

startOperationInternal :: (MonadUnliftIO m, MonadThrow m, MonadLogger m)
                       => ReplicationConnection
                       -> ReplicationTaskT m ()
                       -> ReplicationT m ReplicationResponse
startOperationInternal connection func =
    tryAny (runWithConnection connection func) >>= check
  where
    check (Right _) = pure ReplicationSuccess
    check (Left exc) = do
        printException exc
        pure $ ReplicationError exc

startReplicationOperation
    :: ( WalConstraints env m
       , HasPostgresConfig env
       , HasMinioConfig env
       , HasWalConfig env
       )
    => LogSource
    -> ReplicationTaskT m ()
    -> m ReplicationOperation
startReplicationOperation source func = do
    dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL
    ReplicationOperation <$> startAsyncOperation source
                                                 initialize
                                                 finalize
                                                 (main dbPath)
                                                 printException
                                                 cancelReplicationConnection
  where
    initialize = connectToReplication =<< view postgresConfigL

    finalize connection = do
        logMessageN "Closing replication connection."
        closeReplicationConnection connection

    main dbPath connection = runReplicationT dbPath source $ do
        logMessageN "Running replication operation."
        startOperationInternal connection func

startRecursiveReplicationOperation
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m)
    => LogSource
    -> ReplicationTaskT m ()
    -> ReplicationT m ReplicationOperation
startRecursiveReplicationOperation source func = do
    ReplicationOperation <$> startAsyncOperation source
                                                 initialize
                                                 finalize
                                                 main
                                                 printException
                                                 cancelReplicationConnection
  where
    initialize = connectToReplication =<< view postgresConfigL

    finalize connection = do
        logMessageN "Closing replication connection."
        closeReplicationConnection connection

    main connection = runRecursiveReplicationT source $ do
        logMessageN "Running replication operation."
        startOperationInternal connection func

waitForReplicationOperation
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationOperation
    -> m ()
waitForReplicationOperation (ReplicationOperation op) = waitForAsyncOperation op

finishReplicationOperation
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationOperation
    -> m ()
finishReplicationOperation (ReplicationOperation op) =
    finishAsyncOperation op >>= \case
        ReplicationSuccess -> pure ()
        ReplicationError exc -> do
            printException exc
            throwM exc
        ReplicationAborted ->
            throwM $ replicationException "Replication aborted."

cancelReplicationOperation
    :: WalConstraints env m => ReplicationOperation -> m ()
cancelReplicationOperation (ReplicationOperation op) = do
    logMessageN "Canceling replication operation."
    cancelAsyncOperation op
    response <- finishAsyncOperation op
    logMessageN $ "Done canceling replication operation: " <> display response

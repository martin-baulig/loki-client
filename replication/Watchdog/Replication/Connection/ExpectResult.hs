module Watchdog.Replication.Connection.ExpectResult
    ( ExpectResult(..)
    , getTuplesResult
    , withTuplesResult
    ) where

import           Baulig.Prelude

import           Database.PostgreSQL.LibPQ

import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- ExpectResult
------------------------------------------------------------------------------

class ExpectResult x where
    expectNoResult :: (MonadIO m, MonadThrow m, HasCallStack) => x -> m ()

    expectTuples :: (MonadUnliftIO m, MonadThrow m, HasCallStack)
                 => x
                 -> ReplicationTaskT m TuplesResult

    unexpectedResult :: (MonadIO m, MonadThrow m, HasCallStack) => x -> m Void

instance ExpectResult AsyncResult where
    expectNoResult (AsyncResult _ CommandOk) = pure ()
    expectNoResult ares = absurd <$> unexpectedResult ares

    expectTuples = getTuplesResult

    unexpectedResult (AsyncResult result FatalError) =
        throwIO =<< getResultException result
    unexpectedResult (AsyncResult _ status') = throwIO $ protocolException
        $ "Got unexpected status: " <> tshow status'

------------------------------------------------------------------------------
--- Tuple result functions
------------------------------------------------------------------------------

getTuplesResult :: (MonadUnliftIO m, MonadThrow m, HasCallStack)
                => AsyncResult
                -> ReplicationTaskT m TuplesResult
getTuplesResult (AsyncResult result TuplesOk) = liftIO ctor
  where
    ctor = TuplesResult result <$> ntuples result <*> nfields result
getTuplesResult (AsyncResult result FatalError) =
    throwM =<< getResultException result

getTuplesResult (AsyncResult _ status') =
    throwM $ protocolException $ "Got unexpected status: " <> tshow status'

withTuplesResult :: (MonadUnliftIO m, MonadThrow m, HasCallStack)
                 => AsyncResult
                 -> (TuplesResult -> ReplicationTaskT m x)
                 -> ReplicationTaskT m x
withTuplesResult ares func = do
    result <- getTuplesResult ares
    func result

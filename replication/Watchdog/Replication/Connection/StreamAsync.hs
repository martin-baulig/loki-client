module Watchdog.Replication.Connection.StreamAsync
    ( StreamChunk(..)
    , StreamStatus(..)
    , streamAsyncCommand
    , streamAsyncCommand_
    ) where

import           Baulig.Prelude

import           Database.PostgreSQL.LibPQ

import           UnliftIO.Concurrent

import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Connection.ExpectResult
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Protocol.CopyOut
import           Watchdog.Replication.Protocol.StatusUpdate
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- StreamChunk
------------------------------------------------------------------------------

data StreamChunk = StreamFinished
                 | StreamTuples !TuplesResult
                 | StreamData !CopyOutChunk
                 | StreamShutdown

instance Display StreamChunk where
    display StreamFinished = "[StreamFinished]"
    display (StreamTuples tuples) =
        mconcat ["[StreamTuples ", tshow tuples, "]"]
    display (StreamData body) = mconcat ["[StreamData ", display body, "]"]
    display StreamShutdown = "[StreamShutdown]"

------------------------------------------------------------------------------
--- Helper functions
------------------------------------------------------------------------------

data StreamStatus =
    KeepGoing | Finished | AfterCopyOut | SendStatusUpdate !StandbyStatusUpdate

------------------------------------------------------------------------------
--- Helper functions
------------------------------------------------------------------------------

streamAsyncCommand
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , HasCallStack
       , IsProtocolCommand x
       )
    => x
    -> y
    -> (y -> StreamChunk -> ReplicationTaskT m (y, StreamStatus))
    -> ReplicationTaskT m y
streamAsyncCommand command initial func = do
    withConnection $ \raw -> do
        unlessM (sendQuery raw commandString)
            $ throwM =<< getConnectionException raw
    result <- commandLoop initial func
    logVerboseN "Stream async command done."
    pure result
  where
    commandString = protocolCommandString command

streamAsyncCommand_
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , HasCallStack
       , IsProtocolCommand x
       )
    => x
    -> (StreamChunk -> ReplicationTaskT m StreamStatus)
    -> ReplicationTaskT m ()
streamAsyncCommand_ command func = do
    withConnection $ \raw -> do
        unlessM (sendQuery raw commandString)
            $ throwIO =<< getConnectionException raw
    commandLoop () wrapped
    logVerboseN "Stream async command done."
  where
    commandString = protocolCommandString command

    wrapped _ chunk = ((), ) <$> func chunk

waitForNextResultMaybe
    :: MonadUnliftIO m => ReplicationTaskT m (Maybe AsyncResult)
waitForNextResultMaybe = withConnection (getResult >=> ctor)
  where
    ctor Nothing = pure Nothing
    ctor (Just result) = Just . AsyncResult result <$> resultStatus result

commandLoop :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
            => x
            -> (x -> StreamChunk -> ReplicationTaskT m (x, StreamStatus))
            -> ReplicationTaskT m x
commandLoop current func = handleJust select handler waitNext >>= \case
    Left chunk -> processChunk chunk
    Right Nothing -> pure current
    Right (Just ares) -> processResult ares
  where
    waitNext = Right <$> waitForNextResultMaybe

    select exc = if isShutdownRequestedException exc then Just exc else Nothing

    handler _ = logVerboseN "Got shutdown request."
        >> pure (Left StreamShutdown)

    processResult result
        | asyncResultStatus result == TuplesOk = do
            tuples <- getTuplesResult result
            processChunk $ StreamTuples tuples

    processResult (AsyncResult result status')
        | status' == CopyOut || status' == CopyBoth = do
            withConnection (`getCopyData` False) >>= \case
                CopyOutRow row -> do
                    processChunk $ StreamData $ parseCopyOutChunk row
                CopyOutDone -> continue (current, AfterCopyOut)
                CopyOutError -> throwIO =<< getResultException result
                other -> throwM $ protocolException
                    $ "Got unexpected result: " <> tshow other

    processResult (AsyncResult _ CommandOk) = processChunk StreamFinished

    processResult (AsyncResult result FatalError) =
        throwM =<< getResultException result

    processResult other =
        throwM $ protocolException $ "Got unexpected result: " <> tshow other

    processChunk chunk = func current chunk >>= continue

    continue (next, KeepGoing) = commandLoop next func
    continue (next, Finished) = logVerboseN "Finished" >> pure next
    continue (next, AfterCopyOut) =
        logVerboseN "CopyOut done." >> commandLoop next func
    continue (next, SendStatusUpdate update) = do
        logVerboseN $ "Send status update: " <> display update
        sendStatusUpdate update
        threadDelay 500000
        logVerboseN $ "Send status update done: " <> display update
        commandLoop next func

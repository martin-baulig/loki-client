module Watchdog.Replication.Connection.AsyncResult
    ( AsyncResult(..)
    , TuplesResult(..)
    , IsProtocolCommand(..)
    , IsTuplesCommand(..)
    , CreateFromResult(..)
    , CreateFromRow(..)
    , asyncResultStatus
    , fromResult
    , createCommandResult
    , createCommandResultRow
    , createSingleRowResult
    ) where

import           Baulig.Prelude

import qualified Data.Text.IO                   as TIO

import           Database.PostgreSQL.LibPQ

import           Watchdog.Replication.Exception

------------------------------------------------------------------------------
--- IsProtocolCommand
------------------------------------------------------------------------------

class Display x => IsProtocolCommand x where
    protocolCommandString :: x -> ByteString

------------------------------------------------------------------------------
--- AsyncResult
------------------------------------------------------------------------------

data AsyncResult = AsyncResult Result ExecStatus
    deriving stock (Eq, Show)

instance Display AsyncResult where
    display (AsyncResult result status') =
        mconcat ["[AsyncResult ", tshow result, " ", tshow status', "]"]

fromResult :: MonadIO m => Result -> m AsyncResult
fromResult result = AsyncResult result <$> liftIO (resultStatus result)

asyncResultStatus :: AsyncResult -> ExecStatus
asyncResultStatus (AsyncResult _ status') = status'

------------------------------------------------------------------------------
--- CreateFromResult
------------------------------------------------------------------------------

class CreateFromResult x where
    createFromResult
        :: (MonadIO m, MonadThrow m, HasCallStack) => AsyncResult -> m x

------------------------------------------------------------------------------
--- CreateFromRow
------------------------------------------------------------------------------

class CreateFromRow x where
    createFromRow :: [Maybe ByteString] -> Either ReplicationException x

    createFromRowThrow
        :: (MonadThrow m, HasCallStack) => [Maybe ByteString] -> m x
    createFromRowThrow input = case createFromRow input of
        Left exc -> throwM exc
        Right res -> pure res

------------------------------------------------------------------------------
--- TuplesResult
------------------------------------------------------------------------------

data TuplesResult = TuplesResult !Result !Row !Column
    deriving stock (Eq, Show)

------------------------------------------------------------------------------
--- IsTuplesCommand
------------------------------------------------------------------------------

class IsTuplesCommand x where
    type CommandResult x

    createCommandResultValue :: x
                             -> [Maybe ByteString]
                             -> Either ReplicationException (CommandResult x)

    createCommandResultValueThrow :: (MonadThrow m, HasCallStack)
                                  => x
                                  -> [Maybe ByteString]
                                  -> m (CommandResult x)
    createCommandResultValueThrow command values =
        case createCommandResultValue command values of
            Left exc -> throwM exc
            Right res -> pure res

createCommandResultRow
    :: (MonadIO m, MonadThrow m, IsTuplesCommand x, HasCallStack)
    => x
    -> TuplesResult
    -> Row
    -> m (CommandResult x)
createCommandResultRow command (TuplesResult result _ fields) row = do
    values <- liftIO $ forM [0 .. (fields - 1)] $ getvalue' result row
    liftIO $ TIO.putStrLn $ "VALUES: " <> tshow values
    createCommandResultValueThrow command values

createCommandResult
    :: (MonadIO m, MonadThrow m, IsTuplesCommand x, HasCallStack)
    => x
    -> TuplesResult
    -> m [CommandResult x]
createCommandResult command tresult =
    let TuplesResult _ rows _ = tresult
    in
        forM [0 .. (rows - 1)] $ createCommandResultRow command tresult

createSingleRowResult
    :: (MonadIO m, MonadThrow m, IsTuplesCommand x, HasCallStack)
    => x
    -> TuplesResult
    -> m (CommandResult x)
createSingleRowResult command ares =
    ensureSingleton =<< createCommandResult command ares
  where
    ensureSingleton [ s ] = pure s
    ensureSingleton s = throwM $ protocolException
        $ "Expected single row, but got " <> tshow (length s) <> "."


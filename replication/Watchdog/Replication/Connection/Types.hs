module Watchdog.Replication.Connection.Types
    ( ReplicationConnection
    , ReplicationResponse(..)
    , WalConstraints
    , connectToReplication
    , connectToReplicationDatabase
    , closeReplicationConnection
    , cancelReplicationConnection
    , replicationConnectionStatus
    , withReplicationConnection
    ) where

import           Baulig.Prelude                               hiding (cancel)

import           Database.PostgreSQL.LibPQ

import           Watchdog.Replication.Exception
import           Watchdog.Replication.Settings.PostgresConfig
import           Watchdog.Replication.Types
import           Watchdog.Settings.PostgresSettings

------------------------------------------------------------------------------
--- WalConstraints
------------------------------------------------------------------------------

type WalConstraints env m =
    ( MonadUnliftIO m
    , MonadThrow m
    , MonadLogger m
    , MonadReader env m
    , HasCallStack
    )

------------------------------------------------------------------------------
--- Connection
------------------------------------------------------------------------------

data ConnectionHandle = IdleHandle !Connection
                      | BusyHandle !Connection
                      | ClosingHandle !Connection
                      | ClosedHandle

newtype ReplicationConnection = ReplicationConnection (TVar ConnectionHandle)

instance Display ReplicationConnection where
    display _ = "[ReplicationConnection]"

------------------------------------------------------------------------------
--- Connection API
------------------------------------------------------------------------------

connectToReplication
    :: (MonadIO m, MonadThrow m) => PostgresConfig -> m ReplicationConnection
connectToReplication settings = do
    putStrLn $ "CONNECT: " <> tshow connectionString
    raw <- liftIO $ connectdb $ fromString connectionString
    unlessM (liftIO $ success raw) $ throwM =<< getConnectionException raw
    ReplicationConnection <$> newTVarIO (IdleHandle raw)
  where
    connectionString = pgConnectionString settings ++ " " ++ "replication=true"

    success conn = (ConnectionOk ==) <$> status conn

connectToReplicationDatabase
    :: (MonadIO m, MonadThrow m) => PostgresConfig -> m ReplicationConnection
connectToReplicationDatabase settings = do
    putStrLn $ "CONNECT: " <> tshow connectionString
    raw <- liftIO $ connectdb $ fromString connectionString
    unlessM (liftIO $ success raw) $ throwM =<< getConnectionException raw
    ReplicationConnection <$> newTVarIO (IdleHandle raw)
  where
    connectionString = pgConnectionString settings ++ " " ++ "dbname=postgres"

    success conn = (ConnectionOk ==) <$> status conn

cancelReplicationConnection :: MonadUnliftIO m => ReplicationConnection -> m ()
cancelReplicationConnection (ReplicationConnection var) =
    acquireHandle >>= mapM sendCancel >> pure ()
  where
    sendCancel raw = liftIO $ theGetCancel raw >>= mapM cancel

    theGetCancel :: Connection -> IO (Maybe Cancel)
    theGetCancel = getCancel

    acquireHandle = atomically $ readTVar var >>= \case
        IdleHandle raw -> writeTVar var (ClosingHandle raw) >> pure (Just raw)
        BusyHandle raw -> writeTVar var (ClosingHandle raw) >> pure (Just raw)
        _ -> pure Nothing

closeReplicationConnection :: MonadUnliftIO m => ReplicationConnection -> m ()
closeReplicationConnection (ReplicationConnection var) =
    liftIO $ acquireHandle >>= mapM_ finish
  where
    getHandle = \case
        IdleHandle raw -> Just raw
        BusyHandle raw -> Just raw
        ClosingHandle raw -> Just raw
        ClosedHandle -> Nothing

    acquireHandle = atomically $ do
        value <- getHandle <$> readTVar var
        writeTVar var ClosedHandle
        pure value

replicationConnectionStatus
    :: MonadUnliftIO m => ReplicationConnection -> ReplicationT m ConnStatus
replicationConnectionStatus connection =
    withReplicationConnection connection status

withReplicationConnection :: MonadUnliftIO m
                          => ReplicationConnection
                          -> (Connection -> IO x)
                          -> ReplicationT m x
withReplicationConnection (ReplicationConnection var) func = do
    bracket acquireHandle releaseHandle $ liftIO . func
  where
    acquireHandle = atomically $ readTVar var >>= \case
        IdleHandle raw -> writeTVar var (BusyHandle raw) >> pure raw
        BusyHandle _ -> throwM connectionBusyException
        ClosingHandle _ -> throwM shutdownRequestedException
        ClosedHandle -> throwM connectionClosedException

    releaseHandle _ = atomically $ readTVar var >>= \case
        BusyHandle raw -> writeTVar var (IdleHandle raw)
        _ -> pure ()

------------------------------------------------------------------------------
--- ReplicationResponse
------------------------------------------------------------------------------

data ReplicationResponse =
    ReplicationSuccess | ReplicationAborted | ReplicationError !SomeException

instance Display ReplicationResponse where
    display ReplicationSuccess = "[ReplicationSuccess]"
    display ReplicationAborted = "[ReplicationAborted]"
    display (ReplicationError msg) = "[ReplicationError " <> tshow msg <> "]"

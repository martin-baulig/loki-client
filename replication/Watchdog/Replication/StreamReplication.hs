module Watchdog.Replication.StreamReplication
    ( runStreamReplication
      -- Internal
    , StartReplication
    , StreamReplicationContext
    , startReplication
    , startReplicationCommand
    , startReplicationForGeneration
    , waitForReplication
    , finishReplication
    ) where

import           Baulig.Prelude

import qualified Data.Conduit.Combinators                     as CC

import           Watchdog.Replication.Backend
import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.Connection
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Internal.StreamContext
import           Watchdog.Replication.Protocol.CopyOut
import           Watchdog.Replication.Protocol.IdentifySystem
import           Watchdog.Replication.Protocol.StatusUpdate
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.Utils
import           Watchdog.Replication.Protocol.Verify
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Replication.Task
import           Watchdog.Utils.ConduitUtils

------------------------------------------------------------------------------
--- StreamReplication - Public API
------------------------------------------------------------------------------

runStreamReplication
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationTaskT m ()
runStreamReplication = do
    system <- identifySystem
    logNormalN $ "Identify system: " <> display system
    unless (isNothing $ identifySystemDatabase system) $ throwIO
        $ replicationException "Not a replication connection."

    (generation, slotName) <- findGeneration

    logMessageN $ "Start WAL streaming: " <> display generation <> " "
        <> display slotName

    config <- view walConfigL

    let backend = fileBackend config

    logNormalN $ "Using backend: " <> display backend

    runConduitRes $ scanWalFiles backend .| displaySink "WAL file"

    runConduitRes $ scanWalFiles backend .| CC.mapM_ (runVerify backend)

    lastWal <- runConduitRes
        $ scanWalFiles backend .| filterWalFilesWithTimeline Nothing .| maximumC

    let position = backupGenerationPosition generation

    logNormalN $ "Last WAL file: " <> display lastWal
    logNormalN $ "Generation position: " <> display position

    startReplication backend generation
        $ startReplicationCommand (Just slotName) position

------------------------------------------------------------------------------
--- Generation API
------------------------------------------------------------------------------

data StreamReplicationContext =
    StreamReplicationContext { _srcGeneration :: !BackupGeneration
                             , _srcEndAddress :: !(TVar (Maybe WalAddress))
                             , _srcFiles      :: !BackupFileStore
                             , _srcOperation  :: !ReplicationOperation
                             }

startReplicationForGeneration
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, Backend x)
    => StorageContext x
    -> BackupGeneration
    -> ReplicationSlotName
    -> WalAddress
    -> ReplicationT m StreamReplicationContext
startReplicationForGeneration storage generation slot start = do
    logVerboseN $ "Start replication: " <> display generation

    invokeM2 inner newBackupFileStore $ newTVarIO Nothing
  where
    inner fileVar endAddress =
        StreamReplicationContext generation endAddress fileVar
        <$> startRecursiveReplicationOperation source main
      where
        source = "Stream replication"

        command = startReplicationCommand (Just slot) start

        main = do
            logVerboseN "Start replication #1"

            context <- newStreamContext start endAddress fileVar

            streamAsyncCommand_ command
                $ streamHandler storage context endAddress

            logVerboseN "Replication done."

waitForReplication :: (MonadUnliftIO m, MonadThrow m, MonadLogger m)
                   => StreamReplicationContext
                   -> ReplicationT m ()
waitForReplication = waitForReplicationOperation . _srcOperation

finishReplication
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => StreamReplicationContext
    -> WalAddress
    -> ReplicationT m [BackupFileInfo]
finishReplication this end = do
    logVerboseN $ "Finish replication: " <> display end
    atomically $ do
        unlessM (isNothing <$> readTVar (_srcEndAddress this)) $ throwM
            $ protocolException "Replication already finished."
        writeTVar (_srcEndAddress this) $ Just end

    logVerboseN "Wrote end address; waiting for WAL streaming to finish."

    finishReplicationOperation $ _srcOperation this

    logVerboseN "WAL streaming finished."

    atomically $ backupFileStoreGetFiles $ _srcFiles this

------------------------------------------------------------------------------
--- New API
------------------------------------------------------------------------------

startReplication :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, Backend x)
                 => x
                 -> BackupGeneration
                 -> StartReplication
                 -> ReplicationTaskT m ()
startReplication backend generation command = do
    logVerboseN $ "Start replication: " <> display command <> " => "
        <> decodeUtf8 (protocolCommandString command)

    context <- invokeM2 (newStreamContext start)
                        (newTVarIO Nothing)
                        newBackupFileStore

    logMessageN $ "Start WAL streaming: " <> display generation

    storage <- liftReplicationTaskT $ createStorageContext backend generation

    endAddr <- newTVarIO Nothing

    handleJust select handler $ streamAsyncCommand_ command
        $ streamHandler storage context endAddr
  where
    start = walAddress command

    select exc = if isShutdownRequestedException exc then Just exc else Nothing

    handler _ = logMessageN "Replication finished after shutdown request."

streamHandler :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, Backend x)
              => StorageContext x
              -> StreamContext
              -> TVar (Maybe WalAddress)
              -> StreamChunk
              -> ReplicationTaskT m StreamStatus
streamHandler storage context endAddress chunk
    | StreamShutdown <- chunk = throwM shutdownRequestedException

    | StreamFinished <- chunk = do
        logMessageN "Received StreamFinished message."
        pure Finished

    | StreamTuples _
      <- chunk = throwM $ protocolException "Received unexpected tuples chunk."

    | StreamData ShutdownChunk <- chunk = throwM shutdownRequestedException

    | StreamData EmptyChunk
      <- chunk = logVerboseN "Received empty chunk." >> pure KeepGoing

    | StreamData (KeepAliveChunk keepAlive) <- chunk = do
        let PrimaryKeepAlive _ _ needReply = keepAlive
        logMessageN $ "Received keepalive message: " <> display keepAlive
        (write, flush) <- getCurrentAddress context
        logMessageN $ "Received keepalive message: " <> display keepAlive <> " "
            <> display write <> " " <> display flush <> " " <> tshow needReply

        checkFinished flush >>= \case
            Just finished -> pure finished
            Nothing ->
                if needReply
                then SendStatusUpdate <$> standbyStatusUpdate write flush
                else pure KeepGoing

    | StreamData (WalChunk wal) <- chunk = do
        logDebugN $ "Got WAL chunk: " <> display wal <> " "
            <> printHex (fromIntegral walSegmentSize) <> " "
            <> printHex (walAddressSegmentRemaining wal)
        writeChunk context storage $ mapToXLogChunk wal

    | StreamData body <- chunk = throwM $ protocolException
        $ "Received unexpected CopyOut chunk: " <> display body
  where
    checkFinished flush = readTVarIO endAddress >>= \case
        Just end
            | flush >= end -> do
                logNormalN $ "Received end message " <> display end
                    <> ", flushed " <> display flush <> ", finishing."

                pure $ Just Finished
            | otherwise -> do
                logMessageN $ "Received end message " <> display end
                    <> ", but only flushed " <> display flush <> " so far."
                pure Nothing

        Nothing -> pure Nothing

module Watchdog.Replication.Backend.Types
    ( Backend(..)
    , BackendConstraints
    , StorageConstraints
    ) where

import           Baulig.Prelude

import qualified Data.Yaml                                as Y

import           Watchdog.Replication.BackupInfo
import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Types

------------------------------------------------------------------------------
--- Backend
------------------------------------------------------------------------------

type BackendConstraints x = (Display x, Eq x, Show x, Typeable x)

type StorageConstraints x = (Display x, Typeable x, HasBackupGeneration x)

class (BackendConstraints x, StorageConstraints (StorageContext x))
    => Backend x where
    data StorageContext x

    scanWalFiles :: MonadResource m => x -> ConduitT () WalFileAddress m ()

    readWalFile :: (MonadResource m, WalConstraints env m)
                => x
                -> WalFileAddress
                -> ConduitT () ByteString m ()

    scanBackups :: (MonadResource m, WalConstraints env m)
                => x
                -> ConduitT () BackupInfo m ()

    createStorageContext :: (MonadUnliftIO m, MonadLogger m, HasCallStack)
                         => x
                         -> BackupGeneration
                         -> ReplicationT m (StorageContext x)

    streamWalFile :: WalConstraints env m
                  => StorageContext x
                  -> WalAddress
                  -> (forall n. MonadIO n => ConduitT () ByteString n ())
                  -> m ()
    streamWalFile context start source
        | not $ walAddressIsAtSegmentStart start = throwM $ protocolException
            $ "First segment is not at the beginning of a WAL file: "
            <> display start
        | otherwise = writeBackupFile context name source
      where
        name = WalFileName $ pack $ walFileAddressToFileName file

        file = walFileAddress (timelineId $ backupGeneration context, start)

    writeBackupFile :: WalConstraints env m
                    => StorageContext x
                    -> BackupFileName
                    -> (forall n. MonadIO n => ConduitT () ByteString n ())
                    -> m ()

    writeBackupInfo
        :: WalConstraints env m => StorageContext x -> BackupInfo -> m ()
    writeBackupInfo context info =
        writeBackupFile context BackupInfoFileName source
      where
        source :: forall n. MonadIO n => ConduitT () ByteString n ()
        source = yield $ Y.encode info

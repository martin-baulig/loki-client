module Watchdog.Replication.Backend.FileBackend
    ( FileBackend
    , fileBackend
    ) where

import           Baulig.Prelude                           hiding (handle)

import qualified Data.Yaml                                as Y

import           System.FilePath

import           UnliftIO.Directory
import           UnliftIO.IO.File

import           Watchdog.Replication.Backend.Types
import           Watchdog.Replication.BackupInfo
import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Utils.ConduitUtils

------------------------------------------------------------------------------
--- FileBackend
------------------------------------------------------------------------------

newtype FileBackend = FileBackend WalConfig
    deriving stock (Eq, Show)

instance Display FileBackend where
    display (FileBackend config) = "[FileBackend " <> display config <> "]"

fileBackend :: WalConfig -> FileBackend
fileBackend = FileBackend

instance Backend FileBackend where
    data StorageContext FileBackend =
        FileBackendStorage { fbcGeneration :: !BackupGeneration
                           , fbcStorage    :: !FilePath
                           , fbcTemp       :: !FilePath
                           , fbcWal        :: !FilePath
                           }
            deriving stock (Eq, Show)

    scanWalFiles (FileBackend config) = whenM (doesDirectoryExist dir)
        $ sourceDirectory dir .| filterMapC toWalFilePath
      where
        dir = walConfigStorageDirectory config

        toWalFilePath (takeFileName . normalise -> path) =
            walFileNameParser path

    scanBackups (FileBackend config) = whenM (doesDirectoryExist dir)
        $ sourceDirectory dir .| filterMapMC toBackupInfo
      where
        dir = walConfigTempDirectory config

        toBackupInfo (takeFileName . normalise -> path) =
            scanBackupDirectory dir path

    readWalFile (FileBackend config) file = sourceFile name
      where
        name =
            walConfigStorageDirectory config </> walFileAddressToFileName file

    createStorageContext (FileBackend config) generation = do
        logMessageN $ "Create file storage context: " <> pack name

        createDirectoryIfMissing True temp

        pure $ FileBackendStorage { fbcGeneration = generation
                                  , fbcStorage    = name
                                  , fbcTemp       = temp
                                  , fbcWal        = temp </> "wal"
                                  }
      where
        fileName = backupGenerationRootPath generation

        name = walConfigBackupDirectory config </> fileName

        temp = walConfigTempDirectory config </> fileName

    writeBackupFile = writeBackupFileImpl

instance Display (StorageContext FileBackend) where
    display this = display $ this <:!> fbcStorage this <+!> fbcTemp this

instance HasBackupGeneration (StorageContext FileBackend) where
    backupGeneration = fbcGeneration

------------------------------------------------------------------------------
--- Write Backup File
------------------------------------------------------------------------------

writeBackupFileImpl :: WalConstraints env m
                    => StorageContext FileBackend
                    -> BackupFileName
                    -> (forall n. MonadIO n => ConduitT () ByteString n ())
                    -> m ()
writeBackupFileImpl context name source = do
    logMessageN $ "Write backup file: " <> display name
    withBinaryFileDurableAtomic fileName WriteMode loop
    logMessageN $ "Done writing backup file: " <> display name
  where
    fileName = fbcTemp context </> rawBackupFileName name

    loop handle = runConduit $ source .| sinkHandle handle

------------------------------------------------------------------------------
--- Scan Backups
------------------------------------------------------------------------------

scanBackupDirectory
    :: WalConstraints env m => FilePath -> FilePath -> m (Maybe BackupInfo)
scanBackupDirectory path name = do
    hasBackupInfo <- doesFileExist infoPath

    logInfoN $ "Scan backup directory: " <> tshow path <> " " <> tshow name
        <> " " <> tshow hasBackupInfo

    if hasBackupInfo then readAndScan else pure Nothing
  where
    infoPath = path </> name </> "backup-info.yaml"

    readAndScan = do
        contents <- readFile infoPath

        logInfoN $ "Got backup info: " <> decodeUtf8 contents

        liftIO (Y.decodeFileEither infoPath) >>= \case
            Left exc -> printException exc >> throwM exc
            Right info -> do
                logInfoN $ "Parsed backup info: " <> display info
                pure info

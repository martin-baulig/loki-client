module Watchdog.Replication.Backend.MinioBackend
    ( MinioBackend
    , minioBackend
    ) where

import           Baulig.Prelude

import qualified Network.HTTP.Conduit                         as NC
import           Network.Minio

import           Watchdog.Replication.Backend.MinioConnection
import           Watchdog.Replication.Backend.Types
import           Watchdog.Replication.BackupInfo
import           Watchdog.Replication.Connection.Types
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Settings.MinioConfig
import           Watchdog.Replication.Settings.WalConfig

------------------------------------------------------------------------------
--- MinioBackend
------------------------------------------------------------------------------

data MinioBackend = MinioBackend !WalConfig !MinioConfig
    deriving stock (Eq, Show)

instance Display MinioBackend where
    display this
        | MinioBackend wal minio <- this = display $ this <:> wal <+> minio

minioBackend :: WalConfig -> MinioConfig -> MinioBackend
minioBackend = MinioBackend

instance Backend MinioBackend where
    data StorageContext MinioBackend =
        MinioBackendStorage { mbsGeneration :: !BackupGeneration
                            , mbsBucket     :: !Bucket
                            , mbsConnection :: !MinioConn
                            }

    createStorageContext (MinioBackend _ minio) generation = do
        logMessageN "Create MinIO storage context."

        let info = getMinioConnectionInfo minio

        manager <- liftIO $ NC.newManager NC.tlsManagerSettings

        liftIO $ MinioBackendStorage generation bucket
            <$> mkMinioConn info manager
      where
        bucket = minio ^. minioConfigBucket

    writeBackupFile = writeBackupFileImpl

instance Display (StorageContext MinioBackend) where
    display this = display $ this <:!> mbsGeneration this

instance HasBackupGeneration (StorageContext MinioBackend) where
    backupGeneration = mbsGeneration

------------------------------------------------------------------------------
--- Write Backup File
------------------------------------------------------------------------------

writeBackupFileImpl :: WalConstraints env m
                    => StorageContext MinioBackend
                    -> BackupFileName
                    -> (forall n. MonadIO n => ConduitT () ByteString n ())
                    -> m ()
writeBackupFileImpl context name source = do
    logMessageN $ "Write backup file: " <> display name
    withMinioConnection (mbsConnection context)
        $ putObject bucket object source Nothing options
    logMessageN $ "Done writing backup file: " <> display name
  where
    bucket = mbsBucket context

    generation = mbsGeneration context

    object = pack $ backupGenerationPath generation name

    options = mapOptions defaultPutObjectOptions

    mapOptions opts = case name of
        BackupManifestFileName -> opts { pooContentType = Just "text/json" }
        BackupInfoFileName -> opts { pooContentType = Just "text/yaml" }
        _ -> opts

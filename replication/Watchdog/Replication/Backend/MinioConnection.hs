module Watchdog.Replication.Backend.MinioConnection
    ( getMinioConnectionInfo
    , withMinio
    , withMinioConnection
    ) where

import           Baulig.Prelude

import           Network.Minio
import           Network.URI

import           Watchdog.Replication.Settings.MinioConfig

------------------------------------------------------------------------------
--- MinioStatus
------------------------------------------------------------------------------

getMinioConnectionInfo :: MinioConfig -> ConnectInfo
getMinioConnectionInfo config = setCreds creds connInfo
  where
    connInfo = fromString $ uriToString id uri ""

    uri = config ^. minioConfigUri

    accessKey = config ^. minioConfigAccessKey

    secretKey = config ^. minioConfigSecretKey

    creds = CredentialValue accessKey secretKey Nothing

withMinio
    :: (MonadUnliftIO m, MonadThrow m, MonadReader env m, HasMinioConfig env)
    => Minio x
    -> m x
withMinio func = view minioConfigL >>= run >>= check
  where
    run config = liftIO $ runMinio (getMinioConnectionInfo config) func

    check (Left err) = throwM err
    check (Right result) = pure result

withMinioConnection
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => MinioConn
    -> Minio ()
    -> m ()
withMinioConnection conn func =
    withRunInIO $ \io -> runMinioWith conn func >>= check io
  where
    check io (Left err) = io $ printException err >> throwM err
    check _ (Right result) = pure result

module Watchdog.Replication.BaseBackup
    ( CreateBaseBackup(..)
    , createBaseBackup
    , runBaseBackupOperation
    ) where

import           Baulig.Prelude

import qualified Data.Yaml                                        as Y

import           Watchdog.Replication.Backend
import           Watchdog.Replication.BackupFile
import           Watchdog.Replication.BackupInfo
import           Watchdog.Replication.Connection
import           Watchdog.Replication.Connection.AsyncResult
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Internal.AsyncStream
import           Watchdog.Replication.Internal.BackupContext
import           Watchdog.Replication.Protocol.CopyOut
import           Watchdog.Replication.Protocol.IdentifySystem
import           Watchdog.Replication.Protocol.LongSequenceNumber
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Settings.MinioConfig
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- CreateBaseBackup - Public API
------------------------------------------------------------------------------

runBaseBackupOperation
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationTaskT m ()
runBaseBackupOperation = do
    system <- identifySystem
    logNormalN $ "Identify system: " <> display system
    unless (isNothing $ identifySystemDatabase system) $ throwIO
        $ replicationException "Not a replication connection."

    let name = "backup-" <> printStart system

    logNormalN $ "Backup name: " <> tshow name

    generation <- createNewGeneration Nothing

    logNormalN $ "Backup generation: " <> display generation

    backend <- minioBackend <$> view walConfigL <*> view minioConfigL

    logNormalN $ "Using backend: " <> display backend

    context <- newBackupContext backend generation

    info <- createBaseBackup context def

    logNormalN $ "Base backup done: " <> display info

    retireOldGenerations $ backupInfoGeneration info

    logNormalN "All done."
  where
    printStart system = printLSN $ identifySystemLongPosition system

------------------------------------------------------------------------------
--- BaseBackup
------------------------------------------------------------------------------

createBaseBackup
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack, Backend x)
    => BackupContext x
    -> CreateBaseBackup
    -> ReplicationTaskT m BackupInfo
createBaseBackup context command = do
    logVerboseN $ "Base backup: " <> display command <> " => "
        <> decodeUtf8 (protocolCommandString command)

    result <- streamAsyncCommand command BackupStart $ streamHandler context

    case result of
        BackupFinished info -> do
            logVerboseN $ "Base backup done: " <> display info
            logVerboseN $ "Backup info:\n\n" <> decodeUtf8 (Y.encode info)
            pure info
        other -> do
            $logError
                $ "Backup finished with unexpected state: " <> display other
            throwM $ backupException
                $ "Backup finished with unexpected state: " <> display other

------------------------------------------------------------------------------
--- New API
------------------------------------------------------------------------------


streamHandler :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
              => Backend x
              => BackupContext x
              -> BackupState
              -> StreamChunk
              -> ReplicationTaskT m (BackupState, StreamStatus)
streamHandler context state (StreamTuples row)
    | BackupStart <- state = do
        logVerboseN $ "Got first tuples: " <> tshow row
        header <- createSingleRowResult ReadBaseBackupHeader row
        logVerboseN $ "Base backup header: " <> display header
        pure (BackupHeader header, KeepGoing)

    | BackupHeader header <- state = do
        logVerboseN $ "Got second tuples: " <> tshow row
        tables <- createCommandResult ReadBaseBackupTable row
        forM_ tables
            $ \table -> logVerboseN $ "Base backup tables: " <> display table
        logVerboseN $ "Base backup table: " <> display tables

        liftReplicationTaskT $ backupContextInitialize context header tables

        pure (BackupTables header, KeepGoing)

    | BackupManifest _ <- state = do
        logVerboseN $ "Got final tuples: " <> tshow row
        footer <- createSingleRowResult ReadBaseBackupHeader row
        logVerboseN $ "Base backup footer: " <> display footer

        backupContextFinalize context

        pure (BackupFooter footer, KeepGoing)

streamHandler context state (StreamData value)
    | BackupTables _ <- state, NewArchive file dir <- value = do
        logVerboseN $ "Got new file: " <> tshow file <> " " <> display dir

        writer <- backupContextStartFile context $ BackupTableFileName file

        pure (BackupFile writer, KeepGoing)

    | BackupFile _ <- state, NewArchive file dir <- value = do
        logVerboseN $ "Got new file: " <> tshow file <> " " <> display dir

        writer <- backupContextStartFile context $ BackupBaseFileName file

        pure (BackupFile writer, KeepGoing)

    | BackupFile _ <- state, ManifestStartChunk <- value = do
        logVerboseN "Manifest start chunk."

        writer <- backupContextStartManifest context

        pure (BackupManifest writer, KeepGoing)

    | ProgressReport progress <- value = do
        logVerboseN $ "Progress report: " <> display progress
        pure (state, KeepGoing)

streamHandler _ state (StreamData chunk)
    | BackupFile writer <- state, DataChunk body <- chunk = do
        writeAsyncStream writer body

        pure (state, KeepGoing)

    | BackupManifest writer <- state, DataChunk body <- chunk = do
        writeAsyncStream writer body

        pure (state, KeepGoing)

streamHandler context (BackupFooter footer) StreamFinished = do
    logVerboseN $ "Backup finished: " <> display footer

    info <- liftReplicationTaskT $ backupContextFinish context footer

    logVerboseN $ "Backup info: " <> display info

    pure (BackupFinished info, Finished)

streamHandler _ state other = do
    $logWarn $ "Unexpected chunk: " <> display state <> " " <> display other
    throwM $ protocolException
        $ "Unexpected chunk: " <> display state <> " " <> display other

module Watchdog.Replication.Generation
    ( BackupGeneration
    , HasBackupGeneration(..)
    , createNewGeneration
    , listGenerations
    , updateGeneration
    , findGeneration
    , makeGenerationActive
    , retireOldGenerations
    ) where

import           Baulig.Prelude

import           Database.Persist.Sqlite                       hiding (Active)

import           Watchdog.Replication.Database
import           Watchdog.Replication.Exception
import           Watchdog.Replication.Internal.Generation
import           Watchdog.Replication.Protocol.ReplicationSlot
import           Watchdog.Replication.Protocol.Timestamp
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Protocol.WalAddress
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- Generation
------------------------------------------------------------------------------


createNewGeneration
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => Maybe Text
    -> ReplicationTaskT m BackupGeneration
createNewGeneration maybeName = do
    logMessageN $ "Creating new backup generation: " <> tshow maybeName

    now <- currentTimestamp

    let slotName = replicationSlotName $ "generation_" <> printTimestamp now

    void $ createReplicationSlot slotName

    slot <- readReplicationSlot slotName >>= \case
        Nothing ->
            throwM $ replicationException "Failed to create replication slot."
        Just slot -> pure $ replicationSlotToWalAddress slot

    logVerboseN $ "Got replication slot: " <> display slot

    let generation = mkGeneration' Nothing slot slotName now

    BackupGeneration . entityVal
        <$> runReplicationDatabase (insertEntity generation)

checkGeneration :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
                => BackupGeneration
                -> ReplicationTaskT m BackupGeneration
checkGeneration this
    | generationStatus generation /= Active = do
        logVerboseN $ "Generation not active: " <> display this
        pure this

    | Just slotName <- generationSlotName generation = do
        logVerboseN $ "Generation: " <> display this

        readReplicationSlot slotName >>= \case
            Nothing -> liftReplicationTaskT $ do
                logVerboseN $ "Generation does not have a slot anymore: "
                    <> display this
                updateGeneration this
                                 [ GenerationStatus =. Closed
                                 , GenerationSlotName =. Nothing
                                 ]
            Just slot -> liftReplicationTaskT $ do
                let address = replicationSlotToWalAddress slot
                logVerboseN $ "Generation slot: " <> display address
                updateGeneration this
                                 [GenerationStart =. walAddressRaw address]

    | otherwise = throwM $ backupException
        $ "Generation marked active, but does not have a replication slot: "
        <> display this
  where
    generation = unBackupGeneration this

listGenerations
    :: (MonadUnliftIO m, MonadThrow m) => ReplicationT m [BackupGeneration]
listGenerations = do
    fmap (BackupGeneration . entityVal)
        <$> runReplicationDatabase (selectList [] [])

updateGenerations
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
    => ReplicationTaskT m ()
updateGenerations = do
    generations <- fmap (BackupGeneration . entityVal)
        <$> runReplicationDatabase (selectList [GenerationStatus ==. Active] [])

    for_ generations checkGeneration

findGeneration :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, HasCallStack)
               => ReplicationTaskT m (BackupGeneration, ReplicationSlotName)
findGeneration = do
    updateGenerations

    generations <- fmap (BackupGeneration . entityVal)
        <$> runReplicationDatabase (selectList [GenerationStatus ==. Active]
                                               [ Desc GenerationStart
                                               , LimitTo 1
                                               ])

    case generations of
        [] -> do
            $logError "No usable backup generation.  You need to create a base backup first."
            throwM $ backupException "No usable backup generation.  You need to create a base backup first."
        [ generation ]
            | Just slot
              <- generationSlotName (unBackupGeneration generation) -> do
                logVerboseN $ "Found generation: " <> display generation
                pure (generation, replicationSlotName slot)
        _ -> throwM $ backupException "Should never happen."

updateGeneration :: (MonadUnliftIO m, MonadThrow m)
                 => BackupGeneration
                 -> [Update Generation]
                 -> ReplicationT m BackupGeneration
updateGeneration (unBackupGeneration -> generation) updates = do
    now <- currentTimestamp
    BackupGeneration . entityVal
        <$> runReplicationDatabase (upsertBy created generation
                                    $ updateNow now : updates)
  where
    updateNow now = GenerationUpdated =. now

    created = UniqueCreated $ generationCreated generation

makeGenerationActive :: (MonadUnliftIO m, MonadThrow m)
                     => BackupGeneration
                     -> WalAddress
                     -> WalAddress
                     -> ReplicationT m ()
makeGenerationActive generation start end = void
    $ updateGeneration generation
                       [ GenerationStatus =. Active
                       , GenerationBackupStart =. Just (walAddressRaw start)
                       , GenerationBackupEnd =. Just (walAddressRaw end)
                       , GenerationStart =. walAddressRaw end
                       , GenerationPosition =. walAddressRaw end
                       ]

retireOldGenerations :: (MonadUnliftIO m, MonadThrow m, MonadLogger m)
                     => PostgresTimestamp
                     -> ReplicationTaskT m ()
retireOldGenerations key = do
    logMessageN "Retiring old generations."
    now <- currentTimestamp
    slots <- runReplicationDatabase $ do
        old <- selectList filterOld []
        forM_ old $ \(entityKey -> item) -> update item $ updateOld now
        pure (replicationSlotName
              <$> mapMaybe (generationSlotName . entityVal) old)
    logMessageN
        $ "Old replication slots:" <> intercalateDisplay' True "\n    " slots
    forM_ slots dropReplicationSlot
    logMessageN "Done retiring old generations."
  where
    filterOld = [GenerationCreated !=. key, GenerationStatus ==. Active]

    updateOld now = [ GenerationUpdated =. now
                    , GenerationSlotName =. Nothing
                    , GenerationStatus =. Closed
                    ]

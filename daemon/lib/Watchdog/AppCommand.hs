module Watchdog.AppCommand
    ( DaemonCommand(..)
    , DaemonClientCommand(..)
    , ServerManagerCommand(..)
    , DaemonHello(..)
    , Command(..)
    , commandParser
    , daemonCommandParser
    ) where

import           Baulig.Prelude

import           Options.Applicative

import           Watchdog.Settings.ServerConfig
import           Watchdog.Vault.ZoneDatabase.Command

------------------------------------------------------------------------------
--- ServerManagerCommand
------------------------------------------------------------------------------

data ServerManagerCommand = ListServersCommand
                          | ServerStatusCommand
                          | StartServerCommand !ServerName
                          | StopServerCommand !ServerName
    deriving stock (Eq, Show)

instance Display ServerManagerCommand

serverNameParser :: ReadM ServerName
serverNameParser = eitherReader $ mkServerName . pack

serverManagerCommandParser :: Parser ServerManagerCommand
serverManagerCommandParser =
    hsubparser (command "list"
                        (info (pure ListServersCommand)
                              (progDesc "List all servers"))
                <> command "status"
                           (info (pure ServerStatusCommand)
                                 (progDesc "Show the status of a specific server"))
                <> command "start"
                           (info (StartServerCommand
                                  <$> argument serverNameParser
                                               (metavar "SERVER_NAME"))
                                 (progDesc "Start a specific server"))
                <> command "stop"
                           (info (StopServerCommand
                                  <$> argument serverNameParser
                                               (metavar "SERVER_NAME"))
                                 (progDesc "Stop a specific server")))

------------------------------------------------------------------------------
--- DaemonCommand
------------------------------------------------------------------------------

data DaemonHello = HelloOkay | HelloTeapot | HelloFailed | HelloError
    deriving stock (Eq, Show)

instance Display DaemonHello where
    display HelloOkay = "Okay"
    display HelloTeapot = "Teapot"
    display HelloFailed = "Failed"
    display HelloError = "Error"

data DaemonClientCommand =
      DaemonHello !DaemonHello
    | DaemonRetry
    | DaemonQuit
    | MinioStatusCommand
    | MinioUploadCommand !Text !Bool !(Maybe Text)
    | OldBaseBackupCommand
    | OldStreamWalCommand
    | BaseBackupCommand
    | StreamWalCommand
    | CancelStreamWalCommand
    | ScanBackupsCommand
    | ListBackupsCommand
    | ListReplicationSlotsCommand
    | DropReplicationSlotCommand !Text
    | ServerManagerCommand !ServerManagerCommand
    deriving stock (Eq, Show)

instance Display DaemonClientCommand where
    display (DaemonHello hello) = "[DaemonHello " <> display hello <> "]"
    display DaemonRetry = "[DaemonRetry]"
    display DaemonQuit = "[DaemonQuit]"
    display MinioStatusCommand = "[MinioStatus]"
    display (MinioUploadCommand name check _) =
        "[MinioUpload " <> display name <> " " <> tshow check <> "]"
    display OldBaseBackupCommand = "[OldBaseBackup]"
    display OldStreamWalCommand = "[OldStreamWal]"
    display BaseBackupCommand = "[BaseBackup]"
    display StreamWalCommand = "[StreamWalCommand]"
    display CancelStreamWalCommand = "[CancelStreamWalCommand]"
    display ScanBackupsCommand = "[ScanBackupsCommand]"
    display ListBackupsCommand = "[ListBackupsCommand]"
    display ListReplicationSlotsCommand = "[ListReplicationSlotsCommand]"
    display (DropReplicationSlotCommand name) =
        "[DropReplicationSlotsCommand " <> display name <> "]"
    display (ServerManagerCommand command') =
        "[ServerManagerCommand ]" <> display command' <> "]"

data DaemonCommand = RunDaemon | DaemonClient !DaemonClientCommand
    deriving stock (Eq, Show)

------------------------------------------------------------------------------
--- Command
------------------------------------------------------------------------------

data Command = GetLabelsCommand
             | GetLabelValuesCommand !Text
             | PushCommand !Text
             | PushLoremCommand !Int
             | PushFileCommand !FilePath
             | QueryCommand !Text
             | QueryRangeCommand !Text
             | TailCommand !Text
             | DaemonCommand !DaemonCommand
             | ZoneCommand !ZoneDatabaseCommand
    deriving stock (Eq, Show)

getLabelValues :: Parser Command
getLabelValues = GetLabelValuesCommand <$> argument str (metavar "LABEL")

push :: Parser Command
push = PushCommand <$> argument str (metavar "MESSAGE")

query :: Parser Command
query = QueryCommand <$> argument str (metavar "MESSAGE")

queryRange :: Parser Command
queryRange = QueryRangeCommand <$> argument str (metavar "MESSAGE")

tailCommand :: Parser Command
tailCommand = TailCommand <$> argument str (metavar "MESSAGE")

pushLorem :: Parser Command
pushLorem = PushLoremCommand <$> argument auto (metavar "PARAGRAPHS")

pushFile :: Parser Command
pushFile = PushFileCommand <$> argument str (metavar "FILE")

minioUpload :: Parser DaemonClientCommand
minioUpload = MinioUploadCommand <$> argument str (metavar "NAME")
    <*> switch (long "check" <> help "Check whether object exists.")
    <*> option (Just <$> str) (long "file" <> value Nothing <> metavar "FILE")

daemonClientParser :: Parser DaemonClientCommand
daemonClientParser = hsubparser $ commandGroup "Daemon client commands:"
    <> command "hello"
               (info (pure $ DaemonHello HelloOkay) (progDesc "Daemon hello."))
    <> command "hello-teapot"
               (info (pure $ DaemonHello HelloTeapot)
                     (progDesc "Daemon hello, returning status 418."))
    <> command "hello-fail"
               (info (pure $ DaemonHello HelloFailed)
                     (progDesc "Daemon hello, simulating error."))
    <> command "hello-error"
               (info (pure $ DaemonHello HelloError)
                     (progDesc "Daemon hello, simulating exception."))
    <> command "retry"
               (info (pure DaemonRetry) (progDesc "Retry failed uploads."))
    <> command "minio-status"
               (info (pure MinioStatusCommand)
                     (progDesc "Test MinIO connection."))
    <> command "minio-upload" (info minioUpload (progDesc "MinIO upload."))
    <> command "old-base-backup"
               (info (pure OldBaseBackupCommand)
                     (progDesc "PostgreSQL base backup."))
    <> command "old-stream-wal"
               (info (pure OldStreamWalCommand) (progDesc "Stream WAL files."))
    <> command "base-backup"
               (info (pure BaseBackupCommand)
                     (progDesc "PostgreSQL base backup."))
    <> command "stream-wal"
               (info (pure StreamWalCommand) (progDesc "Stream WAL files."))
    <> command "cancel-stream-wal"
               (info (pure CancelStreamWalCommand)
                     (progDesc "Cancel WAL streaming."))
    <> command "scan-backups"
               (info (pure ScanBackupsCommand) (progDesc "Scan backups."))
    <> command "list-backups"
               (info (pure ListBackupsCommand) (progDesc "List backups."))
    <> command "list-replication-slots"
               (info (pure ListReplicationSlotsCommand)
                     (progDesc "List replication slots."))
    <> command "drop-replication-slot"
               (info (DropReplicationSlotCommand
                      <$> argument str (metavar "NAME"))
                     (progDesc "Drop replication slot."))
    <> command "quit" (info (pure DaemonQuit) (progDesc "Quit daemon."))

daemonCommandParser :: Parser Command
daemonCommandParser = DaemonCommand
    <$> (hsubparser commands <|> DaemonClient <$> daemonClientParser)
  where
    commands = commandGroup "Daemon server commands:"
        <> command "daemon" (info (pure RunDaemon) (progDesc "Daemon mode."))
        <> command "server-manager"
                   (info (DaemonClient . ServerManagerCommand
                          <$> serverManagerCommandParser)
                         (progDesc "Manage servers."))

commandParser :: Parser Command
commandParser = hsubparser
    $ command "labels" (info (pure GetLabelsCommand) (progDesc "Get labels."))
    <> command "label" (info getLabelValues (progDesc "Get label values."))
    <> command "push" (info push (progDesc "Push message."))
    <> command "push-lorem" (info pushLorem (progDesc "Push Lorem Ipsum."))
    <> command "push-file" (info pushFile (progDesc "Push file."))
    <> command "query" (info query (progDesc "Query."))
    <> command "query-range" (info queryRange (progDesc "Query range."))
    <> command "tail" (info tailCommand (progDesc "Tail log entries."))

module Watchdog.Settings.AppSettings
    ( parseCommandLineArguments
    , AppSettings(..)
    , HasAppSettings(..)
    ) where

import           Baulig.Prelude

import qualified Data.Attoparsec.Text                as A

import           Options.Applicative

import           Watchdog.Api.Label
import           Watchdog.AppCommand
import           Watchdog.Vault.ZoneDatabase.Command

------------------------------------------------------------------------------
--- LabelArgument
------------------------------------------------------------------------------

attoReadM :: A.Parser a -> ReadM a
attoReadM p = eitherReader (A.parseOnly p . pack)

------------------------------------------------------------------------------
--- AppSettings
------------------------------------------------------------------------------

data AppSettings =
    AppSettings { appSettingsVerbose :: !Bool
                , appSettingsDebug :: !Bool
                , appSettingsDebugSender :: !Bool
                , appSettingsLokiConfig :: !(Maybe FilePath)
                , appSettingsDaemonClientConfig :: !(Maybe FilePath)
                , appSettingsDaemonConfig :: !(Maybe FilePath)
                , appSettingsLabels :: ![Label]
                , appSettingsCommand :: !Command
                }
    deriving stock (Eq, Show)

class HasAppSettings x where
    appSettingsL :: Lens' x AppSettings

instance HasAppSettings AppSettings where
    appSettingsL = id

------------------------------------------------------------------------------
--- Parse command-line arguments
------------------------------------------------------------------------------

argParser :: Parser AppSettings
argParser = AppSettings
    <$> switch (long "verbose" <> short 'v' <> help "Verbose logging.")
    <*> switch (long "debug" <> short 'd' <> help "Debug logging.")
    <*> switch (long "debug-sender" <> help "Debug Loki upload.")
    <*> option (Just <$> str)
               (long "loki-config" <> value Nothing <> metavar "FILE"
                <> help "Loki server config file.")
    <*> option (Just <$> str)
               (long "daemon-client-config" <> value Nothing <> metavar "FILE"
                <> help "Daemon client config file.")
    <*> option (Just <$> str)
               (long "daemon-config" <> value Nothing <> metavar "FILE"
                <> help "Daemon config file.")
    <*> many (option (attoReadM parseLabel)
                     (long "label" <> metavar "KEY=VALUE"
                      <> help "Extra labels."))
    <*> (commandParser <|> daemonCommandParser
         <|> ZoneCommand <$> zoneDatabaseCommandParser)

parseCommandLineArguments :: MonadIO m => m AppSettings
parseCommandLineArguments = liftIO $ execParser opts
  where
    opts = info (argParser <**> helper) fullDesc

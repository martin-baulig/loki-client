module Watchdog.Settings.BaculaConfig
    ( ConsoleConfig(..)
    , resolveConsoleConfig
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Yaml                     as Y

import           Watchdog.App
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- ConsoleConfig
------------------------------------------------------------------------------

data ConsoleConfig = ConsoleConfig { ccName     :: !Text
                                   , ccPassword :: !Text
                                   , ccTls      :: !TlsSettings
                                   }
    deriving (Eq, Generic, Show)

instance FromJSON ConsoleConfig where
    parseJSON = Y.withObject "ConsoleConfig"
        $ \v -> ConsoleConfig <$> v .: "name" <*> v .: "password" <*> v .: "tls"

instance HasTlsSettings ConsoleConfig where
    tlsSettingsL = lens ccTls (\x y -> x { ccTls = y })

resolveConsoleConfig :: MonadAppConfig m => m ConsoleConfig
resolveConsoleConfig =
    getAppConfigFile "bacula-console.yaml" >>= Y.decodeFileThrow

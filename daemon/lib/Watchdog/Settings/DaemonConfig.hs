module Watchdog.Settings.DaemonConfig
    ( DaemonConfig
    , DaemonClientConfig
    , DaemonDatabasePath
    , SenderDatabasePath
    , DaemonConstraints
    , SenderConstraints
    , HasDaemonAddress(..)
    , HasDaemonConfig(..)
    , HasDaemonClientConfig(..)
    , resolveDaemonConfig
    , resolveDaemonClientConfig
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.Tagged
import qualified Data.Yaml                                    as Y

import           Network.Socket
import qualified Network.URI                                  as U

import           Baulig.Utils.PrettyPrint

import           Watchdog.Common.Sockets
import           Watchdog.LokiClient.StreamContext
import           Watchdog.Replication.Settings.PostgresConfig
import           Watchdog.Replication.Settings.WalConfig
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.DatabasePath
import           Watchdog.Settings.LokiConfig
import           Watchdog.Settings.ServerConfig
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- DaemonAddress
------------------------------------------------------------------------------

data DaemonAddress = DaemonAddress { daHostAddress :: !DaemonHostAddress
                                   , daSocketPath  :: !DaemonSocketAddress
                                   , daHostName    :: !(Maybe HostName)
                                   }
    deriving stock (Eq, Generic, Show)

instance FromJSON DaemonAddress where
    parseJSON (Y.Object v) = DaemonAddress <$> v .: "hostAddress"
        <*> v .: "socketPath" <*> v .:? "hostName"
    parseJSON _ = error "Invalid DaemonAddress."

------------------------------------------------------------------------------
--- HasDaemonAddress
------------------------------------------------------------------------------

class HasDaemonAddress env where
    daemonAddressL :: Lens' env DaemonAddress

    daemonHostName :: SimpleGetter env (Maybe HostName)
    daemonHostName = daemonAddressL . to daHostName

    daemonSocketPath :: SimpleGetter env DaemonSocketAddress
    daemonSocketPath = daemonAddressL . to daSocketPath

    daemonHostAddress :: SimpleGetter env DaemonHostAddress
    daemonHostAddress = daemonAddressL . to daHostAddress

instance HasDaemonAddress DaemonAddress where
    daemonAddressL = id

------------------------------------------------------------------------------
--- HasDaemonClientConfig
------------------------------------------------------------------------------

class (HasDaemonAddress env, HasTlsSettings env)
    => HasDaemonClientConfig env where
    daemonClientConfigL :: Lens' env DaemonClientConfig

    daemonClientUri :: SimpleGetter env U.URI
    daemonClientUri = daemonClientConfigL . to getUri
      where
        getUri config
            | dccUseSocket config = U.URI "http:" (Just socketAuth) "/" "" ""
          where
            socketAuth = U.URIAuth "" "localhost" ""

        getUri config = U.URI "https:" (Just auth) "/" "" ""
          where
            hostName = printHostAddress $ daHostAddress $ dccAddress config

            host = fromMaybe hostName $ daHostName $ dccAddress config

            port = show $ getPortNumber $ daHostAddress $ dccAddress config

            auth = U.URIAuth "" host $ ":" ++ port

    daemonClientConfigUseSocket :: SimpleGetter env Bool
    daemonClientConfigUseSocket = daemonClientConfigL . to dccUseSocket

instance HasDaemonClientConfig DaemonClientConfig where
    daemonClientConfigL = id

------------------------------------------------------------------------------
--- HasDaemonConfig
------------------------------------------------------------------------------

class (HasDaemonAddress env, HasTlsServerSettings env, HasPostgresConfig env)
    => HasDaemonConfig env where
    daemonConfigL :: Lens' env DaemonConfig

    daemonDatabasePath :: SimpleGetter env DaemonDatabasePath
    daemonDatabasePath = daemonConfigL . to dcDaemonDatabasePath

    senderDatabasePath :: SimpleGetter env SenderDatabasePath
    senderDatabasePath = daemonConfigL . to dcSenderDatabasePath

instance HasDaemonConfig DaemonConfig where
    daemonConfigL = id

instance HasPostgresConfig DaemonConfig where
    postgresConfigL = lens dcPostgres (\x y -> x { dcPostgres = y })

------------------------------------------------------------------------------
--- DaemonClientConfig
------------------------------------------------------------------------------

data DaemonClientConfig = DaemonClientConfig { dccAddress   :: !DaemonAddress
                                             , dccUseSocket :: !Bool
                                             , dccTls       :: !TlsSettings
                                             }
    deriving stock (Eq, Generic, Show)

instance FromJSON DaemonClientConfig where
    parseJSON (Y.Object v) = DaemonClientConfig <$> v .: "address"
        <*> v .: "useSocket" <*> v .: "tls"
    parseJSON _ = error "Invalid DaemonClientConfig."

instance HasDaemonAddress DaemonClientConfig where
    daemonAddressL = lens dccAddress (\x y -> x { dccAddress = y })

instance HasTlsSettings DaemonClientConfig where
    tlsSettingsL = lens dccTls (\x y -> x { dccTls = y })

resolveDaemonClientConfig :: MonadIO m => FilePath -> m DaemonClientConfig
resolveDaemonClientConfig = Y.decodeFileThrow

------------------------------------------------------------------------------
--- DaemonDatabasePath and SenderDatabasePath
------------------------------------------------------------------------------

data DaemonDatabasePathTag

type DaemonDatabasePath = Tagged DaemonDatabasePathTag DatabasePath

------------------------------------------------------------------------------
--- DaemonConfig
------------------------------------------------------------------------------

data DaemonConfig =
    DaemonConfig { dcAddress :: !DaemonAddress
                 , dcTls :: !TlsServerSettings
                 , dcDaemonDatabasePath :: !DaemonDatabasePath
                 , dcSenderDatabasePath :: !SenderDatabasePath
                 , dcPostgres :: !PostgresConfig
                 , dcWalConfig :: !WalConfig
                 , dcServerConfig :: !ServerConfig
                 }
    deriving stock (Eq, Generic, Show)

instance FromJSON DaemonConfig where
    parseJSON = withObject "DaemonConfig"
        $ \v -> DaemonConfig <$> v .: "address" <*> v .: "tls"
        <*> v .: "daemonDatabasePath" <*> v .: "senderDatabasePath"
        <*> v .: "postgres" <*> v .: "wal" <*> v .: "serverConfig"

instance HasDaemonAddress DaemonConfig where
    daemonAddressL = lens dcAddress (\x y -> x { dcAddress = y })

instance HasTlsServerSettings DaemonConfig where
    tlsServerSettingsL = lens dcTls (\x y -> x { dcTls = y })

instance HasWalConfig DaemonConfig where
    walConfigL = lens dcWalConfig (\x y -> x { dcWalConfig = y })

instance HasServerConfig DaemonConfig where
    serverConfigL = to dcServerConfig

resolveDaemonConfig :: MonadIO m => FilePath -> m DaemonConfig
resolveDaemonConfig = Y.decodeFileThrow

------------------------------------------------------------------------------
--- SenderConstraints
------------------------------------------------------------------------------

type SenderConstraints x =
    ( HasStreamContext x
    , HasLokiConfig x
    , HasColorConfig x
    , HasLogOptions x
    , HasAppSettings x
    , HasCallStack
    )

------------------------------------------------------------------------------
--- DaemonConstraints
------------------------------------------------------------------------------

type DaemonConstraints x =
    ( HasStreamContext x
    , HasLokiConfig x
    , HasColorConfig x
    , HasLogOptions x
    , HasDaemonConfig x
    , HasAppSettings x
    , HasWalConfig x
    , HasCallStack
    )

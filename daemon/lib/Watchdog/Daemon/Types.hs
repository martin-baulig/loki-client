module Watchdog.Daemon.Types
    ( Daemon(..)
    , DaemonT(..)
    , DaemonApp(..)
    , HasDaemonApp(..)
    , runDaemonT
    , getCommand
    , withDaemonApp
    ) where

import           Baulig.Prelude

import           Database.Persist

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.App                          (App, HasApp(..))
import           Watchdog.Daemon.Database.Types
import           Watchdog.Daemon.ServerManager.Manager
import           Watchdog.LokiClient.Server
import           Watchdog.LokiClient.Server.Internal
import           Watchdog.Replication.Settings
import           Watchdog.Replication.Task
import           Watchdog.Settings.AppConfig
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Settings.LokiConfig
import           Watchdog.Settings.ServerConfig
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- DaemonApp
------------------------------------------------------------------------------

data DaemonApp =
    DaemonApp { daApp :: !App
              , daDaemonConfig :: !DaemonConfig
              , daDatabase :: !DaemonDatabase
              , daMinioConfig :: !MinioConfig
              , daLogOptions :: !LogOptions
              , daAppLock :: !(MVar ())
              , daReplicationConnection :: !(IORef (Maybe ReplicationOperation))
              , daServerManager :: !ServerManager
              }

class ( HasApp x
      , HasDaemonConfig x
      , HasMinioConfig x
      , HasLogOptions x
      , HasWalConfig x
      , HasServerManager x
      ) => HasDaemonApp x where
    daemonAppL :: Lens' x DaemonApp

    daemonDatabase :: SimpleGetter x DaemonDatabase
    daemonDatabase = daemonAppL . to daDatabase

instance HasDaemonApp DaemonApp where
    daemonAppL = id

instance HasAppSettings DaemonApp where
    appSettingsL = appL . appSettingsL

instance HasLogOptions DaemonApp where
    logOptionsL = appL . logOptionsL

instance HasColorConfig DaemonApp where
    colorConfigL = appL . colorConfigL

instance HasStreamContext DaemonApp where
    streamContextL = appL . streamContextL

instance HasLokiConfig DaemonApp where
    lokiConfigL = appL . lokiConfigL

instance HasDaemonAddress DaemonApp where
    daemonAddressL = daemonConfigL . daemonAddressL

instance HasTlsServerSettings DaemonApp where
    tlsServerSettingsL = daemonConfigL . tlsServerSettingsL

instance HasPostgresConfig DaemonApp where
    postgresConfigL = daemonConfigL . postgresConfigL

instance HasWalConfig DaemonApp where
    walConfigL = daemonConfigL . walConfigL

instance HasApp DaemonApp where
    appL = lens daApp (\x y -> x { daApp = y })

instance HasMinioConfig DaemonApp where
    minioConfigL = lens daMinioConfig (\x y -> x { daMinioConfig = y })

instance HasDaemonConfig DaemonApp where
    daemonConfigL = lens daDaemonConfig (\x y -> x { daDaemonConfig = y })

instance HasServerManager DaemonApp where
    serverManagerL = to daServerManager

withDaemonApp :: ( MonadUnliftIO m
                 , MonadAppConfig m
                 , MonadThrow m
                 , MonadLoggerIO m
                 , MonadReader env m
                 , HasApp env
                 )
              => (DaemonApp -> m x)
              -> m x
withDaemonApp run = do
    env <- view appL
    config <- resolveDaemonConfig =<< getAppConfigFile "daemon-config.yaml"

    withDaemonDatabase (config ^. daemonDatabasePath) $ \daemonDb -> do
        app <- DaemonApp env config daemonDb <$> resolveMinioConfig
            <*> view logOptionsL <*> newMVar () <*> newIORef Nothing
            <*> mkServerManager env (config ^. serverConfigL)
        run app

------------------------------------------------------------------------------
--- Daemon
------------------------------------------------------------------------------

data Daemon = Daemon { daemonApp      :: !DaemonApp
                     , daemonSender   :: !Sender
                     , daemonCommand  :: !(Entity Command)
                     , daemonMessages :: !(TVar [StreamValue])
                     }

instance HasDaemonApp Daemon where
    daemonAppL = lens daemonApp (\x y -> x { daemonApp = y })

instance HasApp Daemon where
    appL = daemonAppL . appL

instance HasAppSettings Daemon where
    appSettingsL = daemonAppL . appSettingsL

instance HasColorConfig Daemon where
    colorConfigL = daemonAppL . colorConfigL

instance HasLogOptions Daemon where
    logOptionsL = daemonAppL . logOptionsL

instance HasLokiConfig Daemon where
    lokiConfigL = daemonAppL . lokiConfigL

instance HasDaemonConfig Daemon where
    daemonConfigL = daemonAppL . daemonConfigL

instance HasDaemonAddress Daemon where
    daemonAddressL = daemonAppL . daemonAddressL

instance HasTlsServerSettings Daemon where
    tlsServerSettingsL = daemonAppL . tlsServerSettingsL

instance HasPostgresConfig Daemon where
    postgresConfigL = daemonAppL . postgresConfigL

instance HasMinioConfig Daemon where
    minioConfigL = daemonAppL . minioConfigL

instance HasWalConfig Daemon where
    walConfigL = daemonAppL . walConfigL

instance HasServerManager Daemon where
    serverManagerL = daemonAppL . serverManagerL

------------------------------------------------------------------------------
--- DaemonT
------------------------------------------------------------------------------

newtype DaemonT m x = DaemonT { unDaemonT :: ReaderT Daemon m x }
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadReader Daemon, MonadThrow, MonadTrans)

instance (MonadUnliftIO m, MonadThrow m)
    => MonadDaemonDatabase (DaemonT m) where
    runDaemonDatabase action = DaemonT $ do
        daemon <- ask
        runDatabaseOp (senderDatabase $ daemonSender daemon) action

instance HasSender Daemon where
    senderL = lens daemonSender (\x y -> x { daemonSender = y })

instance HasStreamContext Daemon where
    streamContextL = senderL . streamContextL

instance MonadIO m => MonadLogger (DaemonT m) where
    monadLoggerLog loc source level msg = do
        sender <- daemonSender <$> ask
        runSenderT sender $ monadLoggerLog loc source level msg

runDaemonT :: Daemon -> DaemonT m x -> m x
runDaemonT daemon (DaemonT func) = runReaderT func daemon

getCommand :: Monad m => DaemonT m (Entity Command)
getCommand = daemonCommand <$> ask

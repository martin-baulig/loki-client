module Watchdog.Daemon.ServerManager
    ( startServer
    , stopServer
        -- Re-exports
    , ServerManager
    , ServerStatus(..)
    , ServerState
    , ServerGoal
    , HasServerManager(..)
    , runServerManager
    ) where

import           Baulig.Prelude

import           Watchdog.Daemon.DaemonCommand         hiding (ServerManager)
import           Watchdog.Daemon.ServerManager.Manager
import           Watchdog.Daemon.ServerManager.Types
import           Watchdog.Settings.ServerConfig

------------------------------------------------------------------------------
--- Start Server
------------------------------------------------------------------------------

startServer :: MonadUnliftIO m => ServerName -> DaemonT m DaemonResponse
startServer name = do
    $logMessage [i|Starting server: #{serverName name}|]
    chan <- view serverManagerEventChannel
    atomically $ writeTChan chan $ StartServerEvent name
    pure NoResponse

stopServer :: MonadUnliftIO m => ServerName -> DaemonT m DaemonResponse
stopServer name = do
    $logMessage [i|Stopping server: #{serverName name}|]
    chan <- view serverManagerEventChannel
    atomically $ writeTChan chan $ StopServerEvent name
    pure NoResponse

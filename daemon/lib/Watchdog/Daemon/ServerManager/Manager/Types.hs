module Watchdog.Daemon.ServerManager.Manager.Types
    ( ServerManager(..)
    , HasServerManager(..)
    , ServerManagerT
    , runServerManagerT
    , askServerManagerSender
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Baulig.Utils.PrettyPrint

import           Watchdog.App
import           Watchdog.Daemon.ServerManager.Metrics
import           Watchdog.Daemon.ServerManager.Types
import           Watchdog.LokiClient.Server
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.LokiConfig
import           Watchdog.Settings.ServerConfig
import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- ServerManager
------------------------------------------------------------------------------

data ServerManager =
    ServerManager { smApp           :: !App
                  , smStreamContext :: !StreamContext
                  , smConfig        :: !ServerConfig
                  , smConfigMap     :: !(Map ServerName ServerInfo)
                  , smStatusMap     :: !(Map ServerName ServerStatus)
                  , smEventChan     :: !(TChan ServerManagerEvent)
                  , smRegistry      :: !ServerManagerRegistry
                  }

instance HasApp ServerManager where
    appL = lens smApp (\x y -> x { smApp = y })

instance HasStreamContext ServerManager where
    streamContextL = lens smStreamContext (\x y -> x { smStreamContext = y })

instance HasLokiConfig ServerManager where
    lokiConfigL = appL . lokiConfigL

instance HasAppSettings ServerManager where
    appSettingsL = appL . appSettingsL

instance HasLogOptions ServerManager where
    logOptionsL = appL . logOptionsL

instance HasColorConfig ServerManager where
    colorConfigL = appL . colorConfigL

instance HasRunningGauge ServerManager where
    setRunningGauge = setRunningGauge . smRegistry

------------------------------------------------------------------------------
--- ServerManagerT
------------------------------------------------------------------------------

newtype ServerManagerT m x =
    ServerManagerT (ReaderT ServerManager (SenderT m) x)
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadReader ServerManager, MonadLogger, MonadThrow)

runServerManagerT :: (MonadUnliftIO m, MonadThrow m, MonadLoggerIO m)
                  => ServerManager
                  -> ServerManagerT m x
                  -> m x
runServerManagerT manager (ServerManagerT func) = do
    withSender manager
               (manager ^. streamContextL)
               (manager ^. logOptionsL)
               path
               labels $ runReaderT func manager
  where
    path = retag $ serverConfigDatabase $ smConfig manager

    labels = [MetricLabel "service" "server-manager"]

askServerManagerSender :: Monad m => ServerManagerT m Sender
askServerManagerSender = ServerManagerT $ ReaderT $ const ask

------------------------------------------------------------------------------
--- HasServerManager
------------------------------------------------------------------------------

class HasServerManager x where
    serverManagerL :: SimpleGetter x ServerManager

    serverManagerConfig :: SimpleGetter x ServerConfig
    serverManagerConfig = serverManagerL . to smConfig

    serverManagerConfigMap :: SimpleGetter x (Map ServerName ServerInfo)
    serverManagerConfigMap = serverManagerL . to smConfigMap

    serverManagerStatusMap :: SimpleGetter x (Map ServerName ServerStatus)
    serverManagerStatusMap = serverManagerL . to smStatusMap

    serverManagerEventChannel :: SimpleGetter x (TChan ServerManagerEvent)
    serverManagerEventChannel = serverManagerL . to smEventChan

instance HasServerManager ServerManager where
    serverManagerL = id

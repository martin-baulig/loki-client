module Watchdog.Daemon.ServerManager.Types
    ( ServerException(..)
    , ServerContext(..)
    , ServerState(..)
    , ServerGoal(..)
    , ServerStatus(..)
    , ServerManagerEvent(..)
    , mkServerContext
    , mkServerStatus
    ) where

import           Baulig.Prelude

import           Data.Time.Clock

import           System.Clock
import           System.Process

import           Text.Printf

import           Watchdog.Api
import           Watchdog.LokiClient.StreamContext
import           Watchdog.Settings.ServerConfig
import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- ServerException
------------------------------------------------------------------------------

data ServerException = ServerAlreadyRunning !ServerName
                     | ServerRestartingAutomatically !ServerName
                     | NoSuchServer !ServerName
                     | FailedToStartServerException !ServerName !SomeException
                     | ServerExceedsMaximumFailureCount !ServerName
                     | ServerExceedsMaximumRestartCount !ServerName
                     | UnknownServerException !SomeException
    deriving stock (Generic, Show)

instance Exception ServerException

------------------------------------------------------------------------------
--- ServerManagerEvent
------------------------------------------------------------------------------

data ServerManagerEvent =
    StartServerEvent !ServerName | StopServerEvent !ServerName
    deriving stock (Eq, Generic, Show)

------------------------------------------------------------------------------
--- ServerGoal
------------------------------------------------------------------------------

data ServerGoal = WantRunning | WantStopped | Disabled
    deriving stock (Eq, Generic, Show)

instance Display ServerGoal

instance FormatMetricValue ServerGoal where
    formatMetricValue = \case
        Disabled -> "0"
        WantStopped -> "1"
        WantRunning -> "2"

------------------------------------------------------------------------------
--- ServerState
------------------------------------------------------------------------------

data ServerState = Stopped
                 | Running !UTCTime !ProcessHandle
                 | Restarting !Int !UTCTime
                 | Crashed !SomeException
    deriving stock Generic

instance Display ServerState where
    display = \case
        Stopped -> "Stopped"
        Running time _ -> [i|Running since #{prettyFormatTime time}|]
        Restarting delay time ->
            let restart = addUTCTime (fromIntegral delay) time
            in
                [i|Restarting at #{prettyFormatTime restart}|]
        Crashed exc -> [i|[Crashed #{displayException exc}]|]

instance FormatMetricValue ServerState where
    formatMetricValue = \case
        Crashed{} -> "0"
        Stopped -> "1"
        Restarting{} -> "2"
        Running{} -> "3"

------------------------------------------------------------------------------
--- ServerStatus
------------------------------------------------------------------------------

data ServerStatus =
    ServerStatus { ssName          :: !ServerName
                 , ssConfig        :: !ServerInfo
                 , ssStreamContext :: !StreamContext
                 , ssState         :: !(TVar ServerState)
                 , ssGoal          :: !(TVar ServerGoal)
                 , ssLastChanged   :: !(TVar UTCTime)
                 , ssContext       :: !(TVar (Maybe ServerContext))
                 , ssAsync         :: !(TVar (Maybe (Async ())))
                 }

instance Display ServerStatus where
    display this = display $ this <:> ssName this <+> ssConfig this

mkServerStreamContext :: MonadIO m => ServerName -> m StreamContext
mkServerStreamContext name = addLabels <$> defaultStreamContext
  where
    addLabels ctx = addLabelToContext (Label "service" "server-logs")
        $ addLabelToContext (mkLabel "source" $ serverName name) ctx

mkServerStatus :: MonadIO m => ServerName -> ServerInfo -> m ServerStatus
mkServerStatus name config = ServerStatus name config
    <$> mkServerStreamContext name <*> newTVarIO Stopped
    <*> newTVarIO WantStopped <*> (liftIO getCurrentTime >>= newTVarIO)
    <*> newTVarIO Nothing <*> newTVarIO Nothing

------------------------------------------------------------------------------
--- ServerContext
------------------------------------------------------------------------------

data ServerContext =
    ServerContext { scName         :: !ServerName
                  , scConfig       :: !ServerInfo
                  , scFirstStarted :: !TimeSpec
                  , scLastStarted  :: !TimeSpec
                  , scLastExited   :: !(Maybe TimeSpec)
                  , scFailureCount :: !Int
                  , scTotalCount   :: !Int
                  }
    deriving stock (Generic, Show)

instance Display ServerContext where
    display this = display $ this <:> scName this <+> scConfig this
        <+> formatTimeSpec (scFirstStarted this)
        <+> formatTimeSpec (scLastStarted this) <+> scFailureCount this
        <+> scTotalCount this

mkServerContext :: MonadIO m => ServerStatus -> m ServerContext
mkServerContext status = ctor <$> liftIO (getTime Monotonic)
  where
    ctor now = ServerContext { scName         = ssName status
                             , scConfig       = ssConfig status
                             , scFirstStarted = now
                             , scLastStarted  = now
                             , scLastExited   = Nothing
                             , scFailureCount = 0
                             , scTotalCount   = 0
                             }

------------------------------------------------------------------------------
--- Utils
------------------------------------------------------------------------------

prettyFormatTime :: UTCTime -> Text
prettyFormatTime = pack . formatTime defaultTimeLocale dateFormat
  where
    dateFormat = "%D %H:%M:%S"

formatTimeSpec :: TimeSpec -> Text
formatTimeSpec (TimeSpec sec _) =
    let (mins, sec') = sec `divMod` 60
        (hours, mins') = mins `divMod` 60
    in
        pack $ printf "%02d:%02d:%02d" hours mins' sec'


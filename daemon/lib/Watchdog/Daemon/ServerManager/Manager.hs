module Watchdog.Daemon.ServerManager.Manager
    ( mkServerManager
    , runServerManager
      -- re-exports
    , ServerManager
    , HasServerManager(..)
    ) where

import           Baulig.Prelude

import           Control.Monad.Extra

import qualified Data.Map.Strict                             as M

import           System.Clock
import           System.Exit

import           UnliftIO.Concurrent
import           UnliftIO.Process

import           Watchdog.Api
import           Watchdog.App
import           Watchdog.Daemon.MetricsApp
import           Watchdog.Daemon.ServerManager.Manager.Types
import           Watchdog.Daemon.ServerManager.Metrics
import           Watchdog.Daemon.ServerManager.Types
import           Watchdog.LokiClient.Server
import           Watchdog.Settings.ServerConfig
import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- ServerManager Constructor
------------------------------------------------------------------------------

mkServerManager :: MonadIO m => App -> ServerConfig -> m ServerManager
mkServerManager smApp smConfig = do
    smStatusMap <- mkStatusMap
    smEventChan <- newTChanIO

    smRegistry <- createServerManagerRegistry smStatusMap

    pure ServerManager { .. }
  where
    smStreamContext = addLabelToContext (Label "service" "server-manager")
        $ smApp ^. streamContextL

    smConfigMap = M.fromList $ serverConfigServerList smConfig

    mkStatusMap = do
        pairs <- forM (serverConfigServerList smConfig)
            $ \(name, info) -> (name, ) <$> mkServerStatus name info
        pure $ M.fromList pairs

------------------------------------------------------------------------------
--- Server Manager Main Loop
------------------------------------------------------------------------------

runServerManager :: (MonadUnliftIO m, MonadThrow m, MonadLoggerIO m)
                 => ServerManager
                 -> m ()
runServerManager manager =
    runServerManagerT manager $ withRunningGauge manager $ do
        race_ runServerManagerMetrics runServerManagerMain

runServerManagerMetrics :: MonadUnliftIO m => ServerManagerT m ()
runServerManagerMetrics = do
    manager <- ask
    sender <- askServerManagerSender
    runMetricsApp 9202 $ smRegistry manager ^. metricsRegistryCollectionL
        <> sender ^. metricsRegistryCollectionL

runServerManagerMain :: MonadUnliftIO m => ServerManagerT m ()
runServerManagerMain = do
    manager <- ask

    threadDelay 500000

    $logNormal "Server Manager Main."
    $logImportant "Server Manager Main."
    $logWarn "Server Manager Main."

    forever $ do
        event <- atomically $ readTChan $ smEventChan manager
        $logMessage [i|Server Manager Event: #{event}|]

        case event of
            StartServerEvent name -> tryAny (startServer manager name) >>= \case
                Left exc -> $logError [i|Failed to start server: #{serverName name}\n#{displayException exc}|]
                Right _ -> $logMessage [i|Successfully started server: #{serverName name}|]

            StopServerEvent name -> tryAny (stopServer manager name) >>= \case
                Left exc -> $logError [i|Failed to stop server: #{serverName name}\n#{displayException exc}|]
                Right _ -> $logMessage [i|Successfully stopped server: #{serverName name}|]

------------------------------------------------------------------------------
--- Start Server
------------------------------------------------------------------------------

startServer
    :: MonadUnliftIO m => ServerManager -> ServerName -> ServerManagerT m ()
startServer manager name
    | Just status <- M.lookup name $ manager ^. serverManagerStatusMap = do
        $logMessageS (serverName name) [i|Start server: #{display status}|]

        atomically $ readTVar (ssState status) >>= \case
            Restarting{} -> throwM $ ServerRestartingAutomatically name
            Running{} -> throwM $ ServerAlreadyRunning name
            Crashed exc -> throwM $ FailedToStartServerException name exc
            Stopped -> writeTVar (ssGoal status) WantRunning

        asyncHandle <- async $ do
            result <- tryAny $ monitorProcess status

            case result of
                Left exc -> do
                    $logErrorS (serverName name)
                               [i|Server manager crashed: #{displayException exc}|]

                Right _ -> do
                    $logMessageS (serverName name) [i|Server exited.|]

            utcNow <- liftIO getCurrentTime

            atomically $ do
                writeTVar (ssAsync status) Nothing
                writeTVar (ssContext status) Nothing
                writeTVar (ssGoal status) WantStopped
                writeTVar (ssLastChanged status) utcNow
                case result of
                    Left exc -> do
                        writeTVar (ssState status) $ Crashed exc
                        writeTVar (ssGoal status) Disabled
                    Right _ -> writeTVar (ssState status) Stopped

        atomically $ writeTVar (ssAsync status) $ Just asyncHandle

    | otherwise = throwIO $ NoSuchServer name

startProcess
    :: MonadUnliftIO m
    => ServerInfo
    -> ServerManagerT m
                      (Maybe Handle, Maybe Handle, Maybe Handle, ProcessHandle)
startProcess config = createProcess (proc path args) { std_in  = NoStream
                                                     , std_out = CreatePipe
                                                     , std_err = CreatePipe
                                                     }
  where
    path = unpack $ serverInfoPath config

    args = unpack <$> serverInfoArguments config

processStream
    :: MonadUnliftIO m => StreamContext -> Maybe Handle -> ServerManagerT m ()
processStream _ Nothing = pure ()
processStream context (Just h) = do
    sender <- askServerManagerSender
    runConduitRes $ sourceHandle h .| linesUnboundedAsciiC
        .| mapM_C (processLine sender)
  where
    processLine sender bs = do
        let textValue = decodeUtf8 bs
        value <- timestampedValue textValue
        sendMessageWithStreamContext sender context value

monitorProcess :: MonadUnliftIO m => ServerStatus -> ServerManagerT m ()
monitorProcess status = do
    $logInfoS name "Starting process."

    start =<< mkServerContext status
  where
    name = serverName $ ssName status

    start context = tryAny (startProcess $ ssConfig status) >>= \case
        Left exc -> do
            $logErrorS name
                       [i|Failed to start process:\n#{displayException exc}|]
            throwIO exc

        Right (hout, herr, _, processHandle) -> do
            now <- liftIO getCurrentTime

            atomically $ do
                writeTVar (ssContext status) $ Just context
                writeTVar (ssState status) $ Running now processHandle
                writeTVar (ssLastChanged status) now

            concurrently_ (processStream (ssStreamContext status) hout)
                          (processStream (ssStreamContext status) herr)

            monitor processHandle context

    monitor processHandle context = do
        $logMessageS name [i|Started process: #{display context}|]

        tryAny (waitForProcess processHandle) >>= \case
            Left exc -> do
                $logErrorS name
                           [i|Failed to monitor process:\n#{displayException exc}|]
                throwIO exc

            Right code -> do
                $logMessageS name [i|Process exited with #{code}.|]

                now <- liftIO $ getTime Monotonic

                maybeRestart <- checkRestartServer status context now code

                whenJust maybeRestart restart

    restart context = do
        $logInfoS name [i|Restart requested: #{display context}|]

        atomically $ writeTVar (ssContext status) $ Just context

        start context

checkRestartServer :: MonadUnliftIO m
                   => ServerStatus
                   -> ServerContext
                   -> TimeSpec
                   -> ExitCode
                   -> ServerManagerT m (Maybe ServerContext)
checkRestartServer status initialContext now code
    | Just policy <- serverInfoRestartPolicy (ssConfig status) = do
        $logInfoS name [i|Restart policy: #{policy}|]

        readTVarIO (ssGoal status) >>= \case
            WantStopped -> do
                $logInfoS name
                          "Not restarting because stopping has been requested."
                pure Nothing

            WantRunning -> checkPolicy policy

            Disabled -> do
                $logWarnS name
                          "Not restarting because service has been disabled."
                pure Nothing

    | otherwise = do
        $logMessageS name [i|No restart policy.|]
        pure Nothing
  where
    name = serverName $ ssName status

    isSuccessCode = case code of
        ExitSuccess -> True
        ExitFailure _ -> False

    elapsed = now - scLastStarted initialContext

    totalCount = scTotalCount initialContext

    failureCount = scFailureCount initialContext

    checkPolicy policy = do
        maxRestartCtx <- case restartPolicyMaxRestarts policy of
            Just maxRestarts -> checkMaxRestarts initialContext maxRestarts
            Nothing -> pure $ Just initialContext

        maxFailCtx <- case (maxRestartCtx, restartPolicyMaxFailures policy) of
            (Just ctx, Just maxFailure) -> checkMaxFailures ctx maxFailure
            (Just ctx, Nothing) -> pure $ Just ctx
            _ -> pure Nothing

        case maxFailCtx of
            Just ctx -> checkDelay policy ctx
            Nothing -> pure Nothing

    checkDelay policy context = do
        let delay = fromMaybe 5 $ restartPolicyDelay policy

        delayTVar <- registerDelay $ delay * 1000 * 1000

        $logInfoS name [i|Waiting #{delay} seconds prior to restarting.|]

        utcNow <- liftIO getCurrentTime
        atomically $ writeTVar (ssState status) $ Restarting delay utcNow

        wantRestart <- atomically $ do
            goal <- readTVar $ ssGoal status
            delayReached <- readTVar delayTVar
            checkSTM $ goal == WantStopped || delayReached
            pure $ goal == WantRunning

        if wantRestart
            then do
                $logInfoS name [i|Done waiting, still want restarting.|]
                pure $ Just $ updateContext context
            else do
                $logInfoS name
                          [i|Stop requested while waiting, not restarting.|]
                pure Nothing

    updateContext context
        | isSuccessCode = context { scLastExited = Just now
                                  , scTotalCount = scTotalCount context + 1
                                  }
        | otherwise = context { scLastExited   = Just now
                              , scTotalCount   = scTotalCount context + 1
                              , scFailureCount = scFailureCount context + 1
                              }

    checkMaxRestarts context (maxRestarts, window)
        | elapsed > window = do
            $logInfoS name
                      [i|Resetting restart count because #{elapsed} seconds passed.|]
            pure $ Just $ context { scTotalCount = 0 }

        | totalCount > maxRestarts = do
            $logErrorS name
                       [i|Server exceeds max restart count of #{maxRestarts}.|]
            pure Nothing

        | otherwise = pure $ Just context

    checkMaxFailures context (maxFailures, window)
        | elapsed > window = do
            $logInfoS name
                      [i|Resetting failure count because #{elapsed} seconds passed.|]
            pure $ Just $ context { scFailureCount = 0 }

        | failureCount > maxFailures = do
            $logErrorS name
                       [i|Server exceeds max failure count of #{maxFailures}.|]
            pure Nothing

        | otherwise = pure $ Just context

------------------------------------------------------------------------------
--- Stop Server
------------------------------------------------------------------------------

stopServer
    :: MonadUnliftIO m => ServerManager -> ServerName -> ServerManagerT m ()
stopServer manager name
    | Just status <- M.lookup name $ manager ^. serverManagerStatusMap = do
        $logMessageS (serverName name) [i|Stop server: #{display status}|]

        state <- atomically $ do
            writeTVar (ssGoal status) WantStopped
            readTVar (ssState status)

        $logInfoS (serverName name) [i|Stop server \#1: #{display state}|]

        case state of
            Running _ processHandle -> terminateProcess processHandle
            _ -> pure ()

    | otherwise = throwIO $ NoSuchServer name

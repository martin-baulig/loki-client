{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Watchdog.Daemon.ServerManager.Metrics
    ( ServerManagerRegistry
    , ServerGoalGauge
    , ServerStateGauge
    , createServerManagerRegistry
    ) where

import           Baulig.Prelude

import           Data.Time.Clock.POSIX

import           Watchdog.Daemon.ServerManager.Types
import           Watchdog.Settings.ServerConfig
import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- ServerRegistry
------------------------------------------------------------------------------

data RegistryTag

data ServerManagerRegistry =
    ServerManagerRegistry { smrRunning  :: !(RunningGauge RegistryTag)
                          , smrRegistry :: !(MetricsRegistry RegistryTag)
                          }

instance HasRunningGauge ServerManagerRegistry where
    setRunningGauge = setRunningGauge . smrRunning

instance HasMetricsRegistryCollection ServerManagerRegistry where
    metricsRegistryCollectionL = to smrRegistry . metricsRegistryCollectionL

instance HasMetricsRegistry RegistryTag ServerManagerRegistry where
    metricsRegistryL = to smrRegistry

------------------------------------------------------------------------------
--- ServerGoalGauge
------------------------------------------------------------------------------

newtype ServerGoalGauge = ServerGoalGauge (SimpleGauge RegistryTag ServerGoal)
    deriving newtype (Metric RegistryTag)

serverGoalGauge :: [MetricLabel] -> ServerStatus -> ServerGoalGauge
serverGoalGauge labels status = ServerGoalGauge
    $ SimpleGauge "watchdog_server_desired_state"
                  "Desired server state."
                  (name : labels) $ ssGoal status
  where
    name = MetricLabel "name" $ serverName $ ssName status

------------------------------------------------------------------------------
--- ServerStateGauge
------------------------------------------------------------------------------

newtype ServerStateGauge =
    ServerStateGauge (SimpleGauge RegistryTag ServerState)
    deriving newtype (Metric RegistryTag)

serverStateGauge :: [MetricLabel] -> ServerStatus -> ServerStateGauge
serverStateGauge labels status = ServerStateGauge
    $ SimpleGauge "watchdog_server_current_state"
                  "Current server state."
                  (name : labels) $ ssState status
  where
    name = MetricLabel "name" $ serverName $ ssName status

------------------------------------------------------------------------------
--- ServerLastChangedGauge
------------------------------------------------------------------------------

newtype ServerLastChangedGauge =
    ServerLastChangedGauge (SimpleGauge RegistryTag UTCTime)
    deriving newtype (Metric RegistryTag)

instance FormatMetricValue UTCTime where
    formatMetricValue time =
        let seconds :: Int = round $ utcTimeToPOSIXSeconds time
        in
            tshow $ 1000 * seconds

serverLastChangedGauge
    :: [MetricLabel] -> ServerStatus -> ServerLastChangedGauge
serverLastChangedGauge labels status = ServerLastChangedGauge
    $ SimpleGauge "watchdog_server_last_changed"
                  "Timestamp of last update."
                  (name : labels) $ ssLastChanged status
  where
    name = MetricLabel "name" $ serverName $ ssName status

------------------------------------------------------------------------------
--- Server Manager Metrics
------------------------------------------------------------------------------

createServerManagerRegistry
    :: MonadIO m => Map ServerName ServerStatus -> m ServerManagerRegistry
createServerManagerRegistry statusMap = do
    smrRunning <- runningGauge "watchdog_server_manager" mempty
    smrRegistry
        <- createMetricsRegistry mempty $ AnyMetric smrRunning : allMetrics
    pure ServerManagerRegistry { .. }
  where
    allMetrics = mconcat $ serverGauges <$> toList statusMap

    serverGauges server = [ AnyMetric $ serverGoalGauge mempty server
                          , AnyMetric $ serverStateGauge mempty server
                          , AnyMetric $ serverLastChangedGauge mempty server
                          ]


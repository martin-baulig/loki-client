module Watchdog.Daemon.Database.Types
    ( DaemonDatabase
    , DaemonDatabaseCounter
    , DaemonDatabaseGauge
    , DaemonDatabaseMultiGauge
    , DaemonRegistry
    , MonadDaemonDatabase(..)
    , withDaemonDatabase
    , createDaemonRegistry
    , Command
    , insertCommand
    , updateCommand
    , insertBackup
    , setBackupUploading
    , module Watchdog.Vault.Database.Types
    ) where

import           Baulig.Prelude

import           Data.List.NonEmpty                   (NonEmpty(..))
import           Data.Tagged

import           Database.Persist
import           Database.Persist.Sqlite

import           Watchdog.Daemon.Database.Command
import qualified Watchdog.Daemon.Database.Command     as C
import           Watchdog.Replication.Database.Backup
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Vault.Database.Types
import           Watchdog.Vault.Metrics.Registry
import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- DaemonDatabase
------------------------------------------------------------------------------

class MonadUnliftIO m => MonadDaemonDatabase m where
    runDaemonDatabase :: ReaderT SqlBackend IO x -> m x

data DaemonTag

type DaemonDatabase = Tagged DaemonTag Database

type DaemonDatabaseCounter = DatabaseCounter DaemonTag DaemonTag

type DaemonDatabaseGauge = DatabaseGauge DaemonTag DaemonTag

type DaemonDatabaseMultiGauge = DatabaseMultiGauge DaemonTag DaemonTag

withDaemonDatabase :: (MonadUnliftIO m, MonadLogger m)
                   => DaemonDatabasePath
                   -> (DaemonDatabase -> m x)
                   -> m x
withDaemonDatabase (retag @_ @_ @DaemonTag -> path) = withDatabase path $ do
    runMigration migrateCommand
    runMigration migrateBackup

------------------------------------------------------------------------------
--- DaemonRegistry
------------------------------------------------------------------------------

newtype DaemonRegistry =
    DaemonRegistry { unDaemonRegistry :: MetricsRegistry DaemonTag }

instance HasMetricsRegistry DaemonTag DaemonRegistry where
    metricsRegistryL = to unDaemonRegistry

instance HasMetricsRegistryCollection DaemonRegistry where
    metricsRegistryCollectionL =
        to unDaemonRegistry . metricsRegistryCollectionL

countCommandsMetric :: DaemonDatabase -> DaemonDatabaseCounter
countCommandsMetric db = DatabaseCounter db
                                         "loki_client_command_count"
                                         "Total number of commands run."
                                         (mempty :: [Filter Command])

commandGauge
    :: MonadIO m => DaemonDatabase -> m (DaemonDatabaseMultiGauge Command)
commandGauge db = liftIO $ do
    putStrLn $ "COMMAND GAUGE: " <> tshow combined

    pure $ DatabaseMultiGauge db
                              "loki_client_command_gauge"
                              "Command gauge."
                              mempty
                              filters
  where
    statusList = C.Running :| [C.Success, C.Failed, C.Error]

    commandList = Hello :| [MinioStatus, BaseBackup, Unknown]

    combined = liftM2 (,) statusList commandList

    filters = mkFilter <$> combined

    mkFilter (status, command) =
        ( MetricLabel "status" (toLower $ tshow status)
          :| [MetricLabel "command" $ toLower $ tshow command]
        , (CommandStatus ==. status) :| [CommandType ==. command]
        )

createDaemonRegistry :: MonadIO m => DaemonDatabase -> m DaemonRegistry
createDaemonRegistry daemonDb = do
    commands <- commandGauge daemonDb

    DaemonRegistry
        <$> createMetricsRegistry mempty
                                  [ AnyMetric $ countCommandsMetric daemonDb
                                  , AnyMetric commands
                                  ]

------------------------------------------------------------------------------
--- Database
------------------------------------------------------------------------------

insertCommand :: MonadDaemonDatabase m => Command -> m (Key Command)
insertCommand = runDaemonDatabase . insert

insertBackup :: MonadDaemonDatabase m => Backup -> m (Key Backup)
insertBackup = runDaemonDatabase . insert

setBackupUploading :: MonadDaemonDatabase m => Key Backup -> m ()
setBackupUploading key =
    runDaemonDatabase $ update key [BackupStatus =. Uploading]

updateCommand :: MonadUnliftIO m
              => DaemonDatabase
              -> Key Command
              -> CommandStatus
              -> Maybe Text
              -> m Command
updateCommand db key status message = runDatabaseOp db $ do
    now <- liftIO getCurrentTime
    updateGet key
              [ CommandStatus =. status
              , CommandMessage =. message
              , CommandEndTime =. Just now
              ]


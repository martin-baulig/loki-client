{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Daemon.Database.Command where

import           Baulig.Prelude          hiding (Type)

import           Database.Persist.Sqlite
import           Database.Persist.TH

------------------------------------------------------------------------------
--- Command
------------------------------------------------------------------------------

data Type = Hello
          | MinioStatus
          | MinioUpload
          | BaseBackup
          | StreamWal
          | CancelStreamWal
          | ScanBackups
          | ServerManager
          | Unknown
    deriving stock (Enum, Eq, Generic, Read, Show)
    deriving (FromJSON, ToJSON)

instance Display Type where
    display = tshow

derivePersistField "Type"

data CommandStatus = Running | Success | Failed | Error
    deriving stock (Enum, Eq, Generic, Read, Show)
    deriving (FromJSON, ToJSON)

derivePersistField "CommandStatus"

share [mkPersist sqlSettings, mkMigrate "migrateCommand"]
      [persistLowerCase|
Command
    type Type
    status CommandStatus
    message Text Maybe
    startTime UTCTime
    endTime UTCTime Maybe
    deriving Show
    deriving Generic
    deriving FromJSON
    deriving ToJSON
|]

mkStarted :: MonadIO m => Type -> m Command
mkStarted t = ctor <$> liftIO getCurrentTime
  where
    ctor now = Command t Running Nothing now Nothing

instance Display Command where
    display this = "[" <> display (commandType this) <> "]"

setFinished :: MonadIO m => Command -> CommandStatus -> Maybe Text -> m Command
setFinished command status maybeMessage = modify <$> liftIO getCurrentTime
  where
    modify now = command { commandStatus  = status
                         , commandMessage = maybeMessage
                         , commandEndTime = Just now
                         }

instance Display (Key Command) where
    display (keyToValues -> [ PersistInt64 values ]) = "#" <> tshow values
    display key = "[CommandKey " <> tshow key <> "]"

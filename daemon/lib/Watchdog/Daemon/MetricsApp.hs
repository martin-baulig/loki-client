module Watchdog.Daemon.MetricsApp (runMetricsApp) where

import           Baulig.Prelude

import           Network.HTTP.Types
import           Network.Wai
import           Network.Wai.Handler.Warp

import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- Database Metrics
------------------------------------------------------------------------------

metricsApp :: MonadUnliftIO m
           => MetricsRegistryCollection
           -> Request
           -> (Response -> IO x)
           -> m x
metricsApp _ request sendResponse
    | requestMethod request /= methodGet = liftIO $ sendResponse
        $ responseLBS methodNotAllowed405 [] "Invalid method."
metricsApp _ request sendResponse
    | rawPathInfo request /= "/metrics" =
        liftIO $ sendResponse $ responseLBS notFound404 [] "Invalid path."
metricsApp registry _ sendResponse = formatAllMetrics registry
    >>= (liftIO . sendResponse) . responseBuilder status200 headers
  where
    headers = [(hContentType, "text/plain; version=0.0.4")]

runMetricsApp :: MonadUnliftIO m => Port -> MetricsRegistryCollection -> m ()
runMetricsApp port registry =
    withRunInIO $ \io -> run port $ \r s -> io $ metricsApp registry r s

module Watchdog.Daemon.Process (sourceProcessWithLoggingOutput) where

import           Baulig.Prelude

import           Data.Conduit.Combinators
import           Data.Conduit.Process

import           System.Exit

------------------------------------------------------------------------------
--- Process Conduit
------------------------------------------------------------------------------

sourceProcessWithLoggingOutput
    :: (MonadUnliftIO m, MonadLogger m) => CreateProcess -> m ExitCode
sourceProcessWithLoggingOutput create = fst3
    <$> sourceProcessWithStreams create
                                 (pure ())
                                 (consumer "stdin")
                                 (consumer "stderr")
  where
    fst3 (a, _, _) = a

    consumer source =
        decodeUtf8Lenient .| linesUnbounded .| awaitForever consume
      where
        consume = logNormalNS source

{-# OPTIONS_GHC -Wno-orphans #-}

module Watchdog.Daemon.App
    ( DaemonApp
    , HasDaemonApp(..)
    , withDaemonApp
    , startStreamingReplication
    , cancelStreamingReplication
      -- Re-exports
    , SenderT
    , module Watchdog.Replication.Settings
    , module Watchdog.Daemon.Database.Types
    , module Watchdog.Vault.Database.Sender
    , module Watchdog.Vault.Metrics
    ) where

import           Baulig.Prelude

import           Watchdog.Daemon.DaemonResponse
import           Watchdog.Daemon.Database.Types
import           Watchdog.Daemon.Types
import           Watchdog.LokiClient.Server
import           Watchdog.Replication.Settings
import           Watchdog.Replication.Task
import           Watchdog.Vault.Database.Sender
import           Watchdog.Vault.Metrics

------------------------------------------------------------------------------
--- Streaming replication
------------------------------------------------------------------------------

withAppLock :: MonadUnliftIO m => DaemonT m x -> DaemonT m x
withAppLock func = do
    var <- daAppLock <$> view daemonAppL
    withMVarMasked var $ const func

startStreamingReplication :: (MonadUnliftIO m, MonadThrow m)
                          => LogSource
                          -> ReplicationTaskT (DaemonT m) ()
                          -> DaemonT m DaemonResponse
startStreamingReplication source func = withAppLock $ do
    var <- daReplicationConnection <$> view daemonAppL

    isEmpty <- isNothing <$> readIORef var
    logMessageN $ "Streaming replication: " <> tshow isEmpty

    if isEmpty
        then start var
        else pure $ FailedResponse "Replication already running."
  where
    start var = do
        operation <- startReplicationOperation source func

        writeIORef var $ Just operation

        logMessageN "Started replication operation."

        pure NoResponse

cancelStreamingReplication
    :: (MonadUnliftIO m, MonadThrow m) => DaemonT m DaemonResponse
cancelStreamingReplication = swapOperation >>= \case
    Nothing -> pure $ FailedResponse "No streaming replication is running."
    Just operation -> do
        logMessageN "Canceling streaming replication."

        handle handler $ cancelReplicationOperation operation

        pure NoResponse
  where
    swapOperation = withAppLock $ do
        var <- daReplicationConnection <$> view daemonAppL
        current <- readIORef var
        writeIORef var Nothing
        pure current

    handler (exc :: SomeException) = do
        $logError $ "Failed to cancel streaming replication: " <> tshow exc
        throwM exc

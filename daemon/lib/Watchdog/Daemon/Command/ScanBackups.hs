module Watchdog.Daemon.Command.ScanBackups (runScanBackups) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication

------------------------------------------------------------------------------
--- ScanBackups
------------------------------------------------------------------------------

runScanBackups
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runScanBackups app = mkStarted ScanBackups
    >>= flip (runDaemonCommand app) scanBackups

scanBackups :: DaemonHandler
scanBackups = startStreamingReplication "scan backups" runBackupScan

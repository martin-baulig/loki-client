module Watchdog.Daemon.Command.MinioUpload (runMinioUpload) where

import           Baulig.Prelude                               hiding (find)

import           Data.Conduit.Combinators

import           Network.Minio
import           Network.Wai
import           Network.Wai.Conduit

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication.Backend.MinioConnection

------------------------------------------------------------------------------
--- MinioUpload
------------------------------------------------------------------------------

runMinioUpload :: (MonadUnliftIO m, MonadThrow m)
               => DaemonApp
               -> Request
               -> Text
               -> Bool
               -> SenderT m Response
runMinioUpload app request name check = mkStarted MinioUpload
    >>= flip (runDaemonCommand app) (minioUpload request name check)

minioUpload :: Request -> Text -> Bool -> DaemonHandler
minioUpload request name False = do
    logNormalN $ "MinIO upload: " <> tshow name
    bucket <- view $ minioConfigL . minioConfigBucket
    uploadObject request bucket name
    pure NoResponse

minioUpload request name True = do
    logNormalN $ "MinIO upload: " <> tshow name
    bucket <- view $ minioConfigL . minioConfigBucket
    case requestBodyLength request of
        ChunkedBody -> pure
            $ FailedResponse "Can't do checked upload with chunked request."
        KnownLength len -> checkedUpload request bucket name len

uploadObject :: (MonadUnliftIO m, MonadThrow m)
             => Request
             -> Bucket
             -> Object
             -> DaemonT m ()
uploadObject request bucket object = do
    logNormalN $ "MinIO uploading object: " <> tshow object
    withMinio $ putObject bucket
                          object
                          (sourceRequestBody request)
                          Nothing
                          defaultPutObjectOptions
    logNormalN $ "MinIO upload done: " <> tshow object

checkedUpload :: Request -> Bucket -> Object -> Word64 -> DaemonHandler
checkedUpload request bucket object size =
    inner =<< checkObjectExists bucket object size
  where
    inner ObjectFound = pure NoResponse
    inner (WrongSize objectSize) = pure $ FailedResponse
        $ "Object has wrong size; expected " <> tshow size <> ", but got "
        <> tshow objectSize <> "."
    inner WrongMetadata = pure $ FailedResponse "Object has wrong metadata."
    inner DoesNotExist = do
        uploadObject request bucket object
        logNormalN $ "MinIO upload done: " <> tshow object
        pure NoResponse

------------------------------------------------------------------------------
--- ObjectExists
------------------------------------------------------------------------------

data ObjectExists =
    DoesNotExist | WrongSize Int64 | WrongMetadata | ObjectFound
    deriving stock (Eq, Ord, Show)

checkObjectExists :: (MonadUnliftIO m, MonadThrow m)
                  => Bucket
                  -> Object
                  -> Word64
                  -> DaemonT m ObjectExists
checkObjectExists bucket object size = do
    maybeItem <- withMinio $ runConduit
        $ listObjects bucket (Just object) False .| find check
    logInfoN $ "MinIO check object exists: " <> tshow maybeItem
    case maybeItem of
        Just (ListItemObject item) -> do
            logNormalN $ "Found object: " <> tshow item
            pure $ if oiSize item == fromIntegral size
                   then ObjectFound
                   else WrongSize $ oiSize item
        _ -> do
            logNormalN "Object does not exist."
            pure DoesNotExist
  where
    check (ListItemObject obj) = oiObject obj == object
    check _ = False

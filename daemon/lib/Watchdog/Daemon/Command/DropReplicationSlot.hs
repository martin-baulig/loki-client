module Watchdog.Daemon.Command.DropReplicationSlot
    ( dropReplicationSlotCommand
    ) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication
import           Watchdog.Replication.Protocol.ReplicationSlot
import           Watchdog.Replication.Protocol.Types
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- Drop replication slot
------------------------------------------------------------------------------

dropReplicationSlotCommand :: (MonadUnliftIO m, MonadThrow m)
                           => DaemonApp
                           -> Text
                           -> SenderT m Response
dropReplicationSlotCommand app name = mkStarted BaseBackup
    >>= flip (runDaemonCommand app) handler
  where
    handler = do
        dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL

        runReplicationT dbPath "drop replication slot" $ do
            connection <- connectToReplication =<< view postgresConfigL

            runWithConnection connection $ do
                dropReplicationSlot $ replicationSlotName name
                pure NoResponse

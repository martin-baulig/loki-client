module Watchdog.Daemon.Command.ServerManager
    ( runListServers
    , runStartServer
    , runStopServer
    , runServerStatus
    ) where

import           Baulig.Prelude

import qualified Data.Map.Strict                as M

import           Network.Wai

import           Baulig.Utils.PrettyTable

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Daemon.DaemonResponse
import           Watchdog.Daemon.ServerManager
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Settings.ServerConfig

------------------------------------------------------------------------------
--- List Servers
------------------------------------------------------------------------------

runListServers
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runListServers app = mkStarted ServerManager
    >>= flip (runDaemonCommand app) listServers

runStartServer :: (MonadUnliftIO m, MonadThrow m)
               => DaemonApp
               -> ServerName
               -> SenderT m Response
runStartServer app name = mkStarted ServerManager
    >>= flip (runDaemonCommand app) run
  where
    run = tryAny (startServer name) >>= \case
        Left exc -> pure $ exceptionResponse exc
        Right _ -> pure $ TextResponse "Server started."

runStopServer :: (MonadUnliftIO m, MonadThrow m)
              => DaemonApp
              -> ServerName
              -> SenderT m Response
runStopServer app name = mkStarted ServerManager
    >>= flip (runDaemonCommand app) run
  where
    run = tryAny (stopServer name) >>= \case
        Left exc -> pure $ exceptionResponse exc
        Right _ -> pure $ TextResponse "Server started."

runServerStatus
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runServerStatus app = mkStarted ServerManager
    >>= flip (runDaemonCommand app) getServerStatus

listServers :: DaemonHandler
listServers = do
    config <- view $ daemonConfigL . serverConfigL
    $logNormal [i|List servers: #{display config}|]

    let output = concatMap formatItem $ serverConfigServerList config
    putStrLn output

    pure $ TextResponse output
  where
    formatItem (name, item) =
        [i|\nServer Info (#{serverName name}):\n#{prettyFormatItem item}\n|]

getServerStatus :: DaemonHandler
getServerStatus = do
    statusMap <- view serverManagerStatusMap

    items <- liftIO $ mapM mkPretty $ M.toList statusMap

    let output = "Server Status:\n" <> prettyFormatTable Nothing items <> "\n"

    putStrLn output

    pure $ TextResponse output
  where
    mkPretty (name, status) = PrettyStatus name <$> readTVarIO (ssGoal status)
        <*> readTVarIO (ssState status)

------------------------------------------------------------------------------
--- PrettyStatus
------------------------------------------------------------------------------

data PrettyStatus = PrettyStatus { psName  :: !ServerName
                                 , psGoal  :: !ServerGoal
                                 , psState :: !ServerState
                                 }

instance PrettyTable PrettyStatus where
    prettyTableName _ _ = "Server Status"

    prettyTableHeaders _ = ["Name", "Goal", "State"]

    prettyTableItemToRow this = unpack
        <$> [ serverName $ psName this
            , display $ psGoal this
            , display $ psState this
            ]

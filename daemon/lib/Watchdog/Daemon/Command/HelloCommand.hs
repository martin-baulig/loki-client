module Watchdog.Daemon.Command.HelloCommand (runDaemonHello) where

import           Baulig.Prelude

import           Network.HTTP.Types
import           Network.Wai

import           Watchdog.AppCommand
import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Daemon.DaemonResponse

------------------------------------------------------------------------------
--- HelloCommand
------------------------------------------------------------------------------

runDaemonHello :: (MonadUnliftIO m, MonadThrow m)
               => DaemonApp
               -> DaemonHello
               -> SenderT m Response
runDaemonHello app hello = mkStarted Hello
    >>= flip (runDaemonCommand app) (daemonHello hello)

daemonHello :: DaemonHello -> DaemonHandler
daemonHello HelloOkay = do
    logInfoN "Daemon Hello."
    logInfoN "New Daemon Hello."
    logInfoNS "Hello" "New Daemon Hello #1."
    pure NoResponse

daemonHello HelloTeapot = do
    logWarnN "Daemon Hello - I'm a teapot."
    logDebugN "HELLO TEST"
    logInfoN "HELLO INFO"
    pure $ ErrorResponse (DaemonHttpStatus status418) "I'm a teapot."

daemonHello HelloFailed = do
    logWarnN "Daemon Hello failed."
    pure $ FailedResponse "Hello failed."

daemonHello HelloError = do
    $logError "Daemon Hello error."
    throwString "Daemon Hello error."

module Watchdog.Daemon.Command.MinioStatus (runMinioStatus) where

import           Baulig.Prelude

import           Network.Minio
import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication.Backend.MinioConnection

------------------------------------------------------------------------------
--- MinioStatus
------------------------------------------------------------------------------

runMinioStatus
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runMinioStatus app = mkStarted MinioStatus
    >>= flip (runDaemonCommand app) minioStatus

minioStatus :: DaemonHandler
minioStatus = do
    logNormalN "MinIO status."
    config <- view minioConfigL
    res <- withMinio $ bucketExists $ config ^. minioConfigBucket
    logNormalN $ "MinIO response: " <> tshow res
    pure $ if res then NoResponse else FailedResponse "Bucket does not exist."

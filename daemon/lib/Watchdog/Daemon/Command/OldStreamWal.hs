module Watchdog.Daemon.Command.OldStreamWal (runOldStreamWal) where

import           Baulig.Prelude

import           Network.Wai

import           System.Exit

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Daemon.Process
import           Watchdog.Replication.Database.Backup
import           Watchdog.Replication.Types

------------------------------------------------------------------------------
--- StreamWal
------------------------------------------------------------------------------

runOldStreamWal
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runOldStreamWal app = mkStarted StreamWal
    >>= flip (runDaemonCommand app) streamWal

streamWal :: DaemonHandler
streamWal = do
    backup <- mkRunningBackup
    key <- insertBackup backup
    logNormalN $ "Base backup " <> display key <> "."
    dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL
    config <- view postgresConfigL

    runReplicationT dbPath "old stream wal" $ do
        logMessageN "Running replication operation."

        let temp = baseBackupTempPath config key
        logNormalN $ "Using temporary path " <> display temp
        createBaseBackupTempPath temp
        let create = postgresStreamWalCommandLine config temp
        logNormalN $ "Command line: " <> tshow create
        code <- sourceProcessWithLoggingOutput create
        logNormalN $ "Process finished: " <> tshow code
        case code of
            ExitSuccess -> pure $ FailedResponse "Not yet implemented."
            ExitFailure exit -> pure $ FailedResponse
                $ "Base backup failed with exit code " <> tshow exit <> "."

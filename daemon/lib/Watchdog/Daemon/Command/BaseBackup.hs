module Watchdog.Daemon.Command.BaseBackup (runBaseBackup) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication

------------------------------------------------------------------------------
--- BaseBackup
------------------------------------------------------------------------------

runBaseBackup
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runBaseBackup app = mkStarted BaseBackup
    >>= flip (runDaemonCommand app) baseBackup

baseBackup :: DaemonHandler
baseBackup = startStreamingReplication "base backup" runBaseBackupOperation

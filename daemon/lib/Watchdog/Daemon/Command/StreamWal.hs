module Watchdog.Daemon.Command.StreamWal (runStreamWal, cancelStreamWal) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication

------------------------------------------------------------------------------
--- StreamWal
------------------------------------------------------------------------------

runStreamWal
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runStreamWal app = mkStarted StreamWal
    >>= flip (runDaemonCommand app) streamWal

streamWal :: DaemonHandler
streamWal = startStreamingReplication "stream wal" runStreamReplication

cancelStreamWal
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
cancelStreamWal app = do
    mkStarted CancelStreamWal
        >>= flip (runDaemonCommand app) cancelStreamingReplication

module Watchdog.Daemon.Command.ListReplicationSlots
    ( listReplicationSlotsCommand
    ) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication
import           Watchdog.Replication.Protocol.ListReplicationSlots
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- List replication slots
------------------------------------------------------------------------------

listReplicationSlotsCommand
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
listReplicationSlotsCommand app = mkStarted BaseBackup
    >>= flip (runDaemonCommand app) handler
  where
    handler = do
        dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL

        runReplicationT dbPath "list replication slots" $ do
            connection <- connectToReplicationDatabase =<< view postgresConfigL

            runWithConnection connection $ do
                slots <- listReplicationSlots
                logInfoN
                    $ "Got slots:" <> intercalateDisplay' True "\n    " slots
                updated <- updateReplicationSlots slots
                logInfoN $ "Got updated slots:"
                    <> intercalateDisplay' True "\n    " updated

                pure $ TextResponse $ "Replication slots:\n"
                    <> intercalate "\n" (fmap display updated)

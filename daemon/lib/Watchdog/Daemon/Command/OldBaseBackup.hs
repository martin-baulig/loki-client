module Watchdog.Daemon.Command.OldBaseBackup (runOldBaseBackup) where

import           Baulig.Prelude

import           Network.Wai

import           System.Exit

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Daemon.Process
import           Watchdog.Replication.Database.Backup
import           Watchdog.Replication.Types

------------------------------------------------------------------------------
--- BaseBackup
------------------------------------------------------------------------------

runOldBaseBackup
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runOldBaseBackup app = mkStarted BaseBackup
    >>= flip (runDaemonCommand app) baseBackup

baseBackup :: DaemonHandler
baseBackup = do
    backup <- mkRunningBackup
    key <- insertBackup backup
    logNormalN $ "Base backup " <> display key <> "."

    dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL
    config <- view postgresConfigL

    let temp = baseBackupTempPath config key
    logNormalN $ "Using temporary path " <> display temp

    code <- runReplicationT dbPath "old base backup" $ do
        logMessageN "Running replication operation."

        let create = postgresBaseBackupCommandLine config temp
        logNormalN $ "Command line: " <> tshow create
        sourceProcessWithLoggingOutput create

    logNormalN $ "Process finished: " <> tshow code
    case code of
        ExitSuccess -> uploadBackup key temp
        ExitFailure exit -> pure $ FailedResponse
            $ "Base backup failed with exit code " <> tshow exit <> "."

uploadBackup :: Key Backup -> BaseBackupTempPath -> DaemonHandler
uploadBackup key temp = do
    logNormalN $ "Uploading base backup: " <> display key <> " " <> display temp
    setBackupUploading key
    pure $ FailedResponse "Not yet implemented."

module Watchdog.Daemon.Command.RetryCommand (retryCommand) where

import           Baulig.Prelude

import           Database.Persist

import           Watchdog.Api.Resample
import           Watchdog.Api.Stream
import           Watchdog.Api.Timestamp
import           Watchdog.Daemon.App
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.Command
import           Watchdog.LokiClient.Connection

------------------------------------------------------------------------------
--- Daemon
------------------------------------------------------------------------------

retryCommand :: (MonadUnliftIO m, MonadThrow m) => SenderT m ()
retryCommand = do
    logInfoN "Retrying failed uploads."
    errors <- getErrorMessages
    logInfoN $ "Got error chunks: " <> tshow (length errors)
    logDebugN $ "Error chunks: " <> tshow (errors <&> entityKey)
    res <- sum <$> traverse retryStream errors
    logInfoN $ "Uploaded " <> display res <> " chunks."
    when (res == length errors && not (null errors)) retryCommand

retryStream
    :: (MonadUnliftIO m, MonadThrow m) => Entity Message -> SenderT m Int
retryStream message = do
    logDebugN $ "Error message data: " <> tshow (entityKey message)
    time <- currentTimestamp
    push <- PushRaw <$> resampleStream time (messageBody $ entityVal message)
    connection <- view lokiConnectionL
    res <- tryAny $ createRequest connection push >>= performRequestWithNoResult
    sender <- ask
    case res of
        Left exc -> do
            $logError $ "Failed to re-upload stream: " <> display exc
            setRunningGauge sender Problems
            pure 0
        Right () -> do
            setResampled (entityKey message) time
            setRunningGauge sender Up
            pure 1

resampleStream
    :: (MonadIO m, MonadThrow m) => Timestamp -> ByteString -> m ByteString
resampleStream = resampleFromJSON (Proxy :: Proxy Streams)


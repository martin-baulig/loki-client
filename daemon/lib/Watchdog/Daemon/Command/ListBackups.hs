module Watchdog.Daemon.Command.ListBackups (runListBackups) where

import           Baulig.Prelude

import           Network.Wai

import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonCommand
import           Watchdog.Replication.Generation
import           Watchdog.Replication.Task

------------------------------------------------------------------------------
--- BaseBackup
------------------------------------------------------------------------------

runListBackups
    :: (MonadUnliftIO m, MonadThrow m) => DaemonApp -> SenderT m Response
runListBackups app = mkStarted BaseBackup
    >>= flip (runDaemonCommand app) listBackups

listBackups :: DaemonHandler
listBackups = do
    dbPath <- DatabasePath . walConfigDatabasePath <$> view walConfigL

    runReplicationT dbPath "list backups" $ do
        backups <- listGenerations

        logMessageN
            $ "Base Backups:" <> intercalateDisplay' True "\n    " backups

        pure $ TextResponse $ "Base Backups:\n"
            <> intercalate "\n" (fmap display backups)

module Watchdog.Daemon.DaemonResponse
    ( DaemonResponse(..)
    , DaemonHttpStatus(..)
    , DaemonHttpResponse(..)
    , daemonHttpResponse
    , updateCommandFromResponse
    , formatDaemonResponse
    , exceptionResponse
    , isSuccessStatus
    ) where

import           Baulig.Prelude

import           Data.Aeson

import qualified Database.Persist.Sqlite          as Sql

import           Network.HTTP.Types
import           Network.Wai

import           Watchdog.Api
import qualified Watchdog.Daemon.Database.Command as D.C
import           Watchdog.Daemon.Database.Types

------------------------------------------------------------------------------
--- DaemonHttpStatus
------------------------------------------------------------------------------

newtype DaemonHttpStatus = DaemonHttpStatus { unDaemonHttpStatus :: Status }
    deriving stock (Eq, Generic, Show)

instance Display DaemonHttpStatus where
    display (DaemonHttpStatus status) = "[" <> tshow (statusCode status) <> ": "
        <> decodeUtf8 (statusMessage status) <> "]"

instance ToJSON DaemonHttpStatus where
    toJSON (DaemonHttpStatus status) =
        object [ "code" .= statusCode status
               , "message" .= decodeUtf8 (statusMessage status)
               ]

instance FromJSON DaemonHttpStatus where
    parseJSON = withObject "DaemonHttpStatus"
        $ \o -> mkStatus' <$> o .: "code" <*> o .: "message"
      where
        mkStatus' code msg = DaemonHttpStatus $ mkStatus code $ encodeUtf8 msg

isSuccessStatus :: DaemonHttpStatus -> Bool
isSuccessStatus (statusCode . unDaemonHttpStatus -> code) =
    code == 200 || code == 204

------------------------------------------------------------------------------
--- DaemonResponse
------------------------------------------------------------------------------

data DaemonResponse = NoResponse
                    | TextResponse !Text
                    | FailedResponse !Text
                    | ErrorResponse !DaemonHttpStatus !Text
    deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON)

formatDaemonResponse :: DaemonResponse -> Text
formatDaemonResponse = \case
    NoResponse -> "No Response."
    TextResponse message -> "Text Response:\n" <> message
    FailedResponse message -> "Failed Response:\n" <> message
    ErrorResponse status message ->
        "Error Response: " <> display status <> "\n" <> message

exceptionResponse :: SomeException -> DaemonResponse
exceptionResponse exc =
    ErrorResponse (DaemonHttpStatus status500)
                  [i|Caught Exception:\n#{displayException exc}|]

updateCommandFromResponse
    :: MonadUnliftIO m
    => DaemonDatabase
    -> Sql.Key Command
    -> DaemonResponse
    -> m Command
updateCommandFromResponse db key = \case
    NoResponse -> updateCommand db key D.C.Success Nothing
    TextResponse text -> updateCommand db key D.C.Success $ Just text
    FailedResponse text -> updateCommand db key D.C.Failed $ Just text
    ErrorResponse _ message -> updateCommand db key D.C.Error $ Just message

------------------------------------------------------------------------------
--- DaemonHttpResponse
------------------------------------------------------------------------------

data DaemonHttpResponse =
    DaemonHttpResponse { dhrStatus   :: !DaemonHttpStatus
                       , dhrKey      :: !(Sql.Key Command)
                       , dhrCommand  :: !Command
                       , dhrResponse :: !DaemonResponse
                       , dhrStream   :: !Stream
                       }
    deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON)

daemonHttpResponse :: MonadUnliftIO m
                   => DaemonDatabase
                   -> Sql.Key Command
                   -> DaemonResponse
                   -> Stream
                   -> m Response
daemonHttpResponse db key response stream = do
    command <- updateCommandFromResponse db key response
    pure $ responseLBS httpStatus [] $ encode
        $ DaemonHttpResponse status key command response stream
  where
    httpStatus = unDaemonHttpStatus status

    status = case response of
        ErrorResponse status' _ -> status'
        _ -> DaemonHttpStatus ok200

module Watchdog.Daemon.Request
    ( daemonApiPath
    , performDaemonRequest
    , runDaemonClientCommand
    ) where

import           Baulig.Prelude

import qualified Data.Aeson                         as A

import           Network.HTTP.Client
import qualified Network.HTTP.Conduit               as HC
import           Network.HTTP.Simple
import qualified Network.URI                        as URI
import           Network.URI.Lens

import           Watchdog.AppCommand
import           Watchdog.Daemon.ClientApp
import           Watchdog.Daemon.DaemonResponse
import           Watchdog.LokiClient.Client.Request
import           Watchdog.LokiClient.Client.Types
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Settings.ServerConfig

------------------------------------------------------------------------------
--- DaemonApiPath
------------------------------------------------------------------------------

daemonApiPath :: DaemonClientCommand -> ApiPath
daemonApiPath (DaemonHello hello) = case hello of
    HelloOkay -> "/hello"
    HelloTeapot -> "/hello-teapot"
    HelloFailed -> "/hello-failed"
    HelloError -> "/hello-error"

daemonApiPath MinioStatusCommand = "/daemon/minio-status"
daemonApiPath (MinioUploadCommand _ False _) = "/daemon/minio-upload/blob"
daemonApiPath (MinioUploadCommand _ True _) = "/daemon/minio-upload/checked"
daemonApiPath OldBaseBackupCommand = "/daemon/old-base-backup"
daemonApiPath OldStreamWalCommand = "/daemon/old-stream-wal"
daemonApiPath BaseBackupCommand = "/daemon/base-backup"
daemonApiPath StreamWalCommand = "/daemon/stream-wal"
daemonApiPath CancelStreamWalCommand = "/daemon/cancel-stream-wal"
daemonApiPath ScanBackupsCommand = "/daemon/scan-backups"
daemonApiPath ListBackupsCommand = "/daemon/list-backups"
daemonApiPath ListReplicationSlotsCommand = "/daemon/list-replication-slots"
daemonApiPath (DropReplicationSlotCommand _) = "/daemon/drop-replication-slot"
daemonApiPath DaemonRetry = "/daemon/retry"
daemonApiPath DaemonQuit = "/daemon/quit"

daemonApiPath (ServerManagerCommand cmd) =
    fromString $ "/daemon/server-manager/" <> case cmd of
        ListServersCommand -> "list"
        ServerStatusCommand -> "status"
        StartServerCommand _ -> "start"
        StopServerCommand _ -> "stop"

------------------------------------------------------------------------------
--- Daemon Request
------------------------------------------------------------------------------

createApiRequest :: (MonadThrow m, MonadReader env m, HasDaemonClientApp env)
                 => ApiPath
                 -> m Request
createApiRequest (ApiPath path) = do
    uri <- view daemonClientUri <&> uriPathLens .~ unpack path

    let textUri = URI.uriToString id uri ""
    (setRequestManager . getHttpManager <$> view daemonClientAppL)
        <*> parseRequest textUri

createDaemonRequest
    :: (MonadIO m, MonadThrow m, MonadReader env m, HasDaemonClientApp env)
    => DaemonClientCommand
    -> m Request
createDaemonRequest command =
    createApiRequest path >>= setDaemonRequestBody command
  where
    path = daemonApiPath command

setDaemonRequestBody
    :: MonadIO m => DaemonClientCommand -> Request -> m Request
setDaemonRequestBody command request
    | MinioUploadCommand name _ Nothing <- command = pure
        $ addToRequestQueryString (queryName name) $ setRequestMethod "POST"
        $ setRequestBody (HC.requestBodySourceChunked $ sourceHandle stdin)
                         request

    | MinioUploadCommand name _ (Just file) <- command = pure
        $ addToRequestQueryString (queryName name) $ setRequestMethod "POST"
        $ setRequestBodyFile (unpack file) request

    | DropReplicationSlotCommand name <- command = pure
        $ addToRequestQueryString (queryName name) request

    | ServerManagerCommand (StartServerCommand name)
      <- command = pure $ addToRequestQueryString (queryName $ serverName name)
        $ setRequestMethod "POST" request

    | ServerManagerCommand (StopServerCommand name)
      <- command = pure $ addToRequestQueryString (queryName $ serverName name)
        $ setRequestMethod "POST" request

    | otherwise = pure request
  where
    queryName name = [("name", Just $ encodeUtf8 name)]

performDaemonRequest
    :: (MonadIO m, MonadThrow m, MonadLogger m) => Request -> m ()
performDaemonRequest request = do
    logDebugN $ "Performing daemon request: " <> tshow request
    response <- httpBS request
    logDebugN $ "Daemon response: " <> displayResponse response

    body <- A.throwDecode $ fromStrict $ getResponseBody response

    logDebugN $ "Daemon response body: " <> tshow body

    handleDaemonResponse body

------------------------------------------------------------------------------
--- handleDaemonResponse
------------------------------------------------------------------------------

handleDaemonResponse
    :: (MonadIO m, MonadLogger m) => DaemonHttpResponse -> m ()
handleDaemonResponse response = do
    logWithoutLoc "" level $ toLogStr $ "Daemon command "
        <> display (dhrKey response) <> " " <> display (dhrCommand response)
        <> " " <> message <> ":\n\n"
        <> formatDaemonResponse (dhrResponse response)
    replayLog key $ dhrStream response
  where
    key = "command " <> display (dhrKey response)

    message = case dhrResponse response of
        FailedResponse _ -> "failed"
        ErrorResponse _ _ -> "failed"
        _ -> "finished successfully"

    level = case dhrResponse response of
        FailedResponse _ -> LevelError
        ErrorResponse _ _ -> LevelError
        _ -> LevelOther "message"

------------------------------------------------------------------------------
--- runDaemonClientCommand
------------------------------------------------------------------------------

runDaemonClientCommand
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , MonadReader env m
       , HasDaemonClientApp env
       )
    => DaemonClientCommand
    -> m ()
runDaemonClientCommand command
    | hasNoResponse = performRequestWithNoResult =<< createDaemonRequest command
  where
    hasNoResponse = command == DaemonQuit

runDaemonClientCommand command =
    createDaemonRequest command >>= performDaemonRequest

module Watchdog.Daemon.ClientApp
    ( DaemonClientApp
    , HasDaemonClientApp(..)
    , resolveDaemonClientApp
    ) where

import           Baulig.Prelude

import           Network.HTTP.Client

import           Baulig.Utils.PrettyPrint

import           Watchdog.App
import           Watchdog.Common.Sockets
import           Watchdog.LokiClient.StreamContext
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Settings.LokiConfig
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- DaemonClientApp
------------------------------------------------------------------------------

data DaemonClientApp = DaemonClientApp { dcaApp     :: !App
                                       , dcaConfig  :: !DaemonClientConfig
                                       , dcaManager :: !Manager
                                       }

class (HasApp x, HasDaemonClientConfig x, HasHttpManager x)
    => HasDaemonClientApp x where
    daemonClientAppL :: Lens' x DaemonClientApp

instance HasDaemonClientApp DaemonClientApp where
    daemonClientAppL = id

instance HasAppSettings DaemonClientApp where
    appSettingsL = appL . appSettingsL

instance HasColorConfig DaemonClientApp where
    colorConfigL = appL . colorConfigL

instance HasStreamContext DaemonClientApp where
    streamContextL = appL . streamContextL

instance HasLokiConfig DaemonClientApp where
    lokiConfigL = appL . lokiConfigL

instance HasLogOptions DaemonClientApp where
    logOptionsL = appL . logOptionsL

instance HasDaemonAddress DaemonClientApp where
    daemonAddressL = daemonClientConfigL . daemonAddressL

instance HasTlsSettings DaemonClientApp where
    tlsSettingsL = daemonClientConfigL . tlsSettingsL

instance HasDaemonClientConfig DaemonClientApp where
    daemonClientConfigL = lens dcaConfig (\x y -> x { dcaConfig = y })

instance HasHttpManager DaemonClientApp where
    getHttpManager = dcaManager

instance HasApp DaemonClientApp where
    appL = lens dcaApp (\x y -> x { dcaApp = y })

resolveDaemonClientApp
    :: (MonadAppConfig m, MonadReader env m, HasApp env) => m DaemonClientApp
resolveDaemonClientApp = do
    app <- view appL
    config <- resolveDaemonClientConfig
        =<< getAppConfigFile "daemon-client-config.yaml"
    DaemonClientApp app config <$> createManager config
  where
    createManager config
        | config ^. daemonClientConfigUseSocket =
            resolveSocketManager $ config ^. daemonSocketPath
    createManager config = newTlsManager $ Just $ config ^. tlsSettingsL

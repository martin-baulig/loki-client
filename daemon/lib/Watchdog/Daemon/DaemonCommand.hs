module Watchdog.Daemon.DaemonCommand
    ( DaemonResponse(..)
    , DaemonHandler
    , runDaemonCommand
      -- re-export
    , DaemonT
    , module Watchdog.Daemon.Database.Command
    ) where

import           Baulig.Prelude

import           Database.Persist
import           Database.Persist.Sqlite

import           Network.HTTP.Types
import           Network.Wai

import           Watchdog.Api
import           Watchdog.Daemon.App
import           Watchdog.Daemon.DaemonResponse
import           Watchdog.Daemon.Database.Command
import           Watchdog.Daemon.Types
import           Watchdog.LokiClient.Server.Internal
import           Watchdog.LokiClient.StreamContext

------------------------------------------------------------------------------
--- DaemonCommand
------------------------------------------------------------------------------

type DaemonHandler = forall m. (MonadUnliftIO m, MonadThrow m, HasCallStack)
    => DaemonT m DaemonResponse

modifyCommandContext
    :: Key Command -> Command -> StreamContext -> StreamContext
modifyCommandContext key command =
    addLabelToContext label . addMetadataToContext meta
  where
    label = mkLabel "command" $ tshow $ commandType command

    meta = mkLabel "commandId" $ tshow $ fromSqlKey key

runDaemonCommand :: MonadUnliftIO m
                 => DaemonApp
                 -> Command
                 -> DaemonT m DaemonResponse
                 -> SenderT m Response
runDaemonCommand app command func = do
    tryAny action >>= \case
        Right response -> pure response
        Left exc -> errorResponse exc
  where
    action = withDaemonContext app command $ do
        key <- entityKey <$> getCommand
        logInfoN $ "Command " <> display key <> ": " <> display command
        res <- func
        logInfoN $ "Command done:\n\n" <> formatDaemonResponse res
        pure res

    printExc exc = "Command threw an exception:\n" <> showLBS exc

    errorResponse (printExc -> message) = do
        $logError $ decodeUtf8 message
        pure $ simpleResponse status500 $ fromStrict message

    simpleResponse status = responseLBS status []

    showLBS = encodeUtf8 . tshow

withDaemonContext :: MonadUnliftIO m
                  => DaemonApp
                  -> Command
                  -> DaemonT m DaemonResponse
                  -> SenderT m Response
withDaemonContext app command func = do
    sender <- askSender
    key <- runDatabaseOp (app ^. daemonDatabase) $ insert command
    let newSender =
            over streamContextL (modifyCommandContext key command) sender
    daemon <- Daemon app newSender (Entity key command) <$> newTVarIO mempty
    result <- lift $ runDaemonT daemon inner
    messages <- reverse <$> readTVarIO (daemonMessages daemon)
    logInfoN $ "Daemon command done: " <> tshow (length messages)
    let stream = streamFromValues' (senderContext newSender) messages
    daemonHttpResponse (app ^. daemonDatabase) key result stream
  where
    inner = tryAny func >>= \case
        Right result -> pure result
        Left exc -> do
            $logError $ "Daemon command failed: " <> display exc
            pure $ ErrorResponse (DaemonHttpStatus status500) $ tshow exc


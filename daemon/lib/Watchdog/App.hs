module Watchdog.App
    ( App
    , HasApp(..)
    , runApp
    , module Watchdog.Settings.AppConfig
    ) where

import           Baulig.Prelude

import           Data.Time.LocalTime

import           System.Environment.XDG.BaseDir

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.StreamContext
import           Watchdog.Settings.AppConfig
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.LokiConfig

------------------------------------------------------------------------------
--- AppSetup
------------------------------------------------------------------------------

data App = App { _appSettings      :: !AppSettings
               , _appCurrentTime   :: !ZonedTime
               , _appConfigDir     :: !FilePath
               , _appLokiConfig    :: !LokiConfig
               , _appColorConfig   :: !ColorConfig
               , _appLogOptions    :: !LogOptions
               , _appStreamContext :: !StreamContext
               }

------------------------------------------------------------------------------
--- HasApp
------------------------------------------------------------------------------

class ( HasAppSettings env
      , HasLokiConfig env
      , HasLogOptions env
      , HasColorConfig env
      , HasStreamContext env
      ) => HasApp env where
    appL :: Lens' env App

    appSettings :: SimpleGetter env AppSettings
    appSettings = appL . to _appSettings

    appCurrentTime :: SimpleGetter env ZonedTime
    appCurrentTime = appL . to _appCurrentTime

instance HasApp App where
    appL = id

instance HasAppSettings App where
    appSettingsL = lens _appSettings (\x y -> x { _appSettings = y })

instance HasLokiConfig App where
    lokiConfigL = lens _appLokiConfig (\x y -> x { _appLokiConfig = y })

instance HasLogOptions App where
    logOptionsL = lens _appLogOptions (\x y -> x { _appLogOptions = y })

instance HasColorConfig App where
    colorConfigL = lens _appColorConfig (\x y -> x { _appColorConfig = y })

instance HasStreamContext App where
    streamContextL =
        lens _appStreamContext (\x y -> x { _appStreamContext = y })

------------------------------------------------------------------------------
--- App
------------------------------------------------------------------------------

runApp :: MonadUnliftIO m => ReaderT App (LoggingT m) () -> m ()
runApp inner = do
    settings <- parseCommandLineArguments

    app <- readConfig settings
    runStderrLoggingT $ runReaderT inner app

appStreamContext :: MonadIO m => AppSettings -> LokiConfig -> m StreamContext
appStreamContext settings config = do
    let configLabels = fromMaybe mempty $ config ^. lokiConfigLabels
    standard <- standardLabels
    let settingsLabels = fromListOfLabels $ appSettingsLabels settings

    pure $ mkStreamContext CurrentTimestamp
        $ configLabels <> settingsLabels <> standard

readConfig :: MonadIO m => AppSettings -> m App
readConfig settings = do
    zonedTime <- liftIO getZonedTime

    configDir <- liftIO $ getUserConfigDir "watchdog"

    lokiConfig <- resolveLokiConfig
        $ fromMaybe (configDir </> "loki-config.yaml")
        $ appSettingsLokiConfig settings

    context <- appStreamContext settings lokiConfig

    options <- defaultLogOptions

    let logFilter
            | appSettingsDebug settings = FilterDebug
            | appSettingsVerbose settings = FilterInfo
            | otherwise = FilterNormal

    pure $ App { _appSettings      = settings
               , _appCurrentTime   = zonedTime
               , _appLokiConfig    = lokiConfig
               , _appConfigDir     = configDir
               , _appColorConfig   = defaultColorConfig
               , _appLogOptions    = options & logOptionsFilter .~ logFilter
               , _appStreamContext = context
               }

instance MonadAppConfig (ReaderT App (LoggingT IO)) where
    appConfigDirectory = _appConfigDir <$> view appL

    getAppConfigFile path = getPath <$> view appL
      where
        getPath app = fromMaybe (_appConfigDir app </> path)
            $ appSettingsDaemonClientConfig $ app ^. appSettings

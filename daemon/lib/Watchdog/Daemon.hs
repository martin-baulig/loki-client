module Watchdog.Daemon
    ( DaemonApp
    , HasDaemonApp(..)
    , runDaemonCommand
    , runDaemon
    ) where

import           Baulig.Prelude

import           Network.HTTP.Types
import           Network.Socket
import           Network.Wai
import           Network.Wai.Handler.Warp
import qualified Network.Wai.Handler.WarpTLS                  as WT
import           Network.Wai.Handler.WebSockets
import qualified Network.WebSockets                           as WS

import           Watchdog.App
import           Watchdog.AppCommand
import           Watchdog.Common.Sockets
import           Watchdog.Daemon.App
import           Watchdog.Daemon.ClientApp
import           Watchdog.Daemon.Command.BaseBackup
import           Watchdog.Daemon.Command.DropReplicationSlot
import           Watchdog.Daemon.Command.HelloCommand
import           Watchdog.Daemon.Command.ListBackups
import           Watchdog.Daemon.Command.ListReplicationSlots
import           Watchdog.Daemon.Command.MinioStatus
import           Watchdog.Daemon.Command.MinioUpload
import           Watchdog.Daemon.Command.OldBaseBackup
import           Watchdog.Daemon.Command.OldStreamWal
import           Watchdog.Daemon.Command.RetryCommand
import           Watchdog.Daemon.Command.ScanBackups
import           Watchdog.Daemon.Command.ServerManager
import           Watchdog.Daemon.Command.StreamWal
import           Watchdog.Daemon.MetricsApp
import           Watchdog.Daemon.Request
import           Watchdog.Daemon.ServerManager
import           Watchdog.LokiClient.Server
import           Watchdog.Settings.DaemonConfig
import           Watchdog.Settings.ServerConfig
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- Daemon
------------------------------------------------------------------------------

runDaemonCommand
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLoggerIO m
       , MonadReader env m
       , MonadAppConfig m
       , HasApp env
       )
    => DaemonCommand
    -> m ()
runDaemonCommand RunDaemon = withDaemonApp runDaemon
runDaemonCommand (DaemonClient client) = runStderrLoggingT
    $ lift resolveDaemonClientApp >>= runReaderT (runDaemonClientCommand client)

------------------------------------------------------------------------------
--- Internal
------------------------------------------------------------------------------

runDaemon
    :: (MonadUnliftIO m, MonadThrow m, MonadLoggerIO m) => DaemonApp -> m ()
runDaemon env = do
    daemonRegistry <- createDaemonRegistry $ env ^. daemonDatabase

    withSender env
               (env ^. streamContextL)
               (env ^. logOptionsL)
               (env ^. senderDatabasePath)
               mempty $ do
        logWarnN "Daemon starting."

        let serverManager = env ^. serverManagerL

        sender <- ask

        initializeMetrics sender

        raceN [ runDaemonApp
              , runDaemonInetApp
              , runMetricsApp 9201
                $ daemonRegistry ^. metricsRegistryCollectionL
                <> sender ^. metricsRegistryCollectionL
              , lift $ runServerManager serverManager
              ]

        logWarnN "Daemon done."
  where
    initializeMetrics registry = do
        setRunningGauge registry Starting
        (pending, failed, messages)
            <- triple <$> countPending <*> countErrors <*> countMessages
        logWarnN $ "Found " <> displayCount "pending message" pending <> ", "
            <> displayCount "error" failed <> " and "
            <> displayCount "regular message" messages <> "."
        setRunningGauge registry Up

    runDaemonApp = runSocket Nothing $ env ^. daemonSocketPath

    runDaemonInetApp = do
        tls <- toWarpTlsSettings $ env ^. tlsServerSettingsL
        runSocket (Just tls) $ env ^. daemonHostAddress

    displayCount d 0 = "no " <> d <> "s"
    displayCount d 1 = "1 " <> d
    displayCount d c = display c <> " " <> d <> "s"

    triple a b c = (a, b, c)

    runSocket :: forall m x. (ToSocketAddress x, MonadUnliftIO m, MonadThrow m)
              => Maybe WT.TLSSettings
              -> x
              -> SenderT m ()
    runSocket maybeTls addr = bracket (bindListenSocket addr)
                                      (liftIO . close)
                                      (daemonApp env defaultSettings maybeTls)

raceN :: MonadUnliftIO m => [m ()] -> m ()
raceN [] = pure ()
raceN (x : xs) = foldl' race_ x xs

daemonApp :: (MonadUnliftIO m, MonadThrow m)
          => DaemonApp
          -> Settings
          -> Maybe WT.TLSSettings
          -> Socket
          -> SenderT m ()
daemonApp ctx settings maybeTls sock = withRunInIO $ \io -> inner io
  where
    inner io = case maybeTls of
        Just tls -> WT.runTLSSocket tls settings sock app
        Nothing -> runSettingsSocket settings sock app
      where
        app = websocketsOr WS.defaultConnectionOptions wsApp backupApp

        backupApp req sendResponse = io $ regularApp ctx sock req sendResponse

    wsApp pending_conn = do
        conn <- WS.acceptRequest pending_conn
        WS.sendTextData conn ("Hello, client!" :: Text)

regularApp :: (MonadUnliftIO m, MonadThrow m)
           => DaemonApp
           -> Socket
           -> Request
           -> (Response -> IO x)
           -> SenderT m x
regularApp app sock req sendResponse = do
    logInfoN $ "Regular app: " <> tshow req
    handleRequest $ parseDaemonRequest req
  where
    handleRequest (Left errorRes) = liftIO $ sendResponse errorRes

    handleRequest (Right (DaemonHello hello)) =
        liftIO . sendResponse =<< runDaemonHello app hello

    handleRequest (Right DaemonRetry) = runCommand_ retryCommand

    handleRequest (Right DaemonQuit) = do
        logWarnN "Received shutdown command."
        finally (simpleResponse status204 "Daemon exiting.")
                (liftIO $ close sock)

    handleRequest (Right MinioStatusCommand) =
        liftIO . sendResponse =<< runMinioStatus app

    handleRequest (Right (MinioUploadCommand name check _)) =
        liftIO . sendResponse =<< runMinioUpload app req name check

    handleRequest (Right OldBaseBackupCommand) =
        liftIO . sendResponse =<< runOldBaseBackup app

    handleRequest (Right OldStreamWalCommand) =
        liftIO . sendResponse =<< runOldStreamWal app

    handleRequest (Right BaseBackupCommand) =
        liftIO . sendResponse =<< runBaseBackup app

    handleRequest (Right StreamWalCommand) =
        liftIO . sendResponse =<< runStreamWal app

    handleRequest (Right CancelStreamWalCommand) =
        liftIO . sendResponse =<< cancelStreamWal app

    handleRequest (Right ScanBackupsCommand) =
        liftIO . sendResponse =<< runScanBackups app

    handleRequest (Right ListBackupsCommand) =
        liftIO . sendResponse =<< runListBackups app

    handleRequest (Right ListReplicationSlotsCommand) =
        liftIO . sendResponse =<< listReplicationSlotsCommand app

    handleRequest (Right (DropReplicationSlotCommand name)) =
        liftIO . sendResponse =<< dropReplicationSlotCommand app name

    handleRequest (Right (ServerManagerCommand ListServersCommand)) =
        liftIO . sendResponse =<< runListServers app

    handleRequest (Right (ServerManagerCommand ServerStatusCommand)) =
        liftIO . sendResponse =<< runServerStatus app

    handleRequest (Right (ServerManagerCommand (StartServerCommand name))) =
        liftIO . sendResponse =<< runStartServer app name

    handleRequest (Right (ServerManagerCommand (StopServerCommand name))) =
        liftIO . sendResponse =<< runStopServer app name

    runCommand_ action = tryAny action >>= \case
        Right () -> simpleResponse status204 ""
        Left exc -> errorResponse exc

    printExc exc = "Command threw an exception:\n" <> showLBS exc

    errorResponse (printExc -> message) = do
        $logError $ decodeUtf8 message
        simpleResponse status500 $ fromStrict message

    simpleResponse status message =
        liftIO $ sendResponse $ responseLBS status [] message

parseDaemonRequest :: Request -> Either Response DaemonClientCommand
parseDaemonRequest request
    | requireGet request && requestMethod request /= "GET" = Left invalidMethod
parseDaemonRequest request
    | requestMethod request == "GET" = case rawPathInfo request of
        "/hello" -> Right $ DaemonHello HelloOkay
        "/hello-teapot" -> Right $ DaemonHello HelloTeapot
        "/hello-failed" -> Right $ DaemonHello HelloFailed
        "/hello-error" -> Right $ DaemonHello HelloError
        "/retry" -> Right DaemonRetry
        "/daemon/quit" -> Right DaemonQuit
        "/daemon/minio-status" -> Right MinioStatusCommand
        "/daemon/old-base-backup" -> Right OldBaseBackupCommand
        "/daemon/old-stream-wal" -> Right OldStreamWalCommand
        "/daemon/base-backup" -> Right BaseBackupCommand
        "/daemon/scan-backups" -> Right ScanBackupsCommand
        "/daemon/list-backups" -> Right ListBackupsCommand
        "/daemon/stream-wal" -> Right StreamWalCommand
        "/daemon/cancel-stream-wal" -> Right CancelStreamWalCommand
        "/daemon/list-replication-slots" -> Right ListReplicationSlotsCommand
        "/daemon/drop-replication-slot" ->
            parseQueryNameArg request DropReplicationSlotCommand
        "/daemon/server-manager/list" ->
            Right $ ServerManagerCommand ListServersCommand
        "/daemon/server-manager/status" ->
            Right $ ServerManagerCommand ServerStatusCommand
        _ -> Left notFound

    | requestMethod request == "POST"
      && rawPathInfo request == "/daemon/minio-upload/blob" =
        parseQueryNameArg request
        $ \name -> MinioUploadCommand name False Nothing

    | requestMethod request == "POST"
      && rawPathInfo request == "/daemon/minio-upload/checked" =
        parseQueryNameArg request
        $ \name -> MinioUploadCommand name True Nothing

    | requestMethod request == "POST"
      && rawPathInfo request == "/daemon/server-manager/start" =
        parseServerNameArg request StartServerCommand

    | requestMethod request == "POST"
      && rawPathInfo request == "/daemon/server-manager/stop" =
        parseServerNameArg request StopServerCommand

    | otherwise = Left notFound

parseServerNameArg :: Request
                   -> (ServerName -> ServerManagerCommand)
                   -> Either Response DaemonClientCommand
parseServerNameArg request func = case queryString request of
    [ ("name", Just name) ]
        | Right name' <- mkServerName $ decodeUtf8 name ->
            Right $ ServerManagerCommand $ func name'
    _ -> Left invalidRequest

parseQueryNameArg :: Request
                  -> (Text -> DaemonClientCommand)
                  -> Either Response DaemonClientCommand
parseQueryNameArg request func = case queryString request of
    [ ("name", Just name) ] -> Right $ func $ decodeUtf8 name
    _ -> Left invalidRequest

requireGet :: Request -> Bool
requireGet (rawPathInfo -> "/daemon/minio-upload/blob") = False
requireGet (rawPathInfo -> "/daemon/minio-upload/checked") = False
requireGet (rawPathInfo -> "/daemon/server-manager/start") = False
requireGet (rawPathInfo -> "/daemon/server-manager/stop") = False
requireGet _ = True

invalidRequest :: Response
invalidRequest = responseLBS status400 [] "Invalid request."

invalidMethod :: Response
invalidMethod = responseLBS status400 [] "Invalid method."

notFound :: Response
notFound = responseLBS status404 [] "Not found."

showLBS :: Show x => x -> ByteString
showLBS = encodeUtf8 . tshow

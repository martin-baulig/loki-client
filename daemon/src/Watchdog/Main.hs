module Watchdog.Main (main) where

import           Baulig.Prelude

import           Network.HTTP.Client.OpenSSL

import           Baulig.Utils.PrettyPrint

import           Watchdog.App
import           Watchdog.AppCommand
import           Watchdog.Daemon
import           Watchdog.LokiClient.Command
import           Watchdog.LokiClient.Connection
import           Watchdog.LokiClient.StreamContext
import           Watchdog.Settings.AppSettings
import           Watchdog.Settings.LokiConfig
import           Watchdog.Vault.ZoneDatabase.Command

------------------------------------------------------------------------------
--- Main
------------------------------------------------------------------------------

main :: IO ()
main = withOpenSSL $ runApp $ do
    command <- appSettingsCommand <$> view appSettings
    logInfoN $ "Got command: " <> tshow command
    putStrLn "MAIN"

    case command of
        GetLabelsCommand -> runLokiCommand GetLabels
        GetLabelValuesCommand label -> runLokiCommand $ GetLabelValues label
        PushCommand push -> runLokiCommand $ PushText push
        PushLoremCommand count -> runLokiCommand $ PushLoremIpsum count
        PushFileCommand file -> runLokiCommand $ PushFile file
        QueryCommand query -> runLokiCommand $ Query query
        QueryRangeCommand query -> runLokiCommand $ QueryRange query
        TailCommand query -> runLokiCommand $ Tail query
        DaemonCommand daemonCommand -> runDaemonCommand daemonCommand
        ZoneCommand zoneCommand -> runZoneDatabaseCommand zoneCommand

runLokiCommand
    :: ( MonadLogger m
       , MonadThrow m
       , MonadUnliftIO m
       , MonadReader env m
       , HasLokiConfig env
       , HasLogOptions env
       , HasColorConfig env
       , HasStreamContext env
       , IsCommand x
       )
    => x
    -> m ()
runLokiCommand command = do
    connection <- mkLokiConnection =<< view lokiConfigL
    runCommand connection command

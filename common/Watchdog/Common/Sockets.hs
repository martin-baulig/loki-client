module Watchdog.Common.Sockets
    ( DaemonHostAddress
    , DaemonSocketAddress(..)
    , ToSocketAddress(..)
    , getHostAddress
    , getPortNumber
    , printHostAddress
    , resolveSocketManager
    , bindListenSocket
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Attoparsec.Text as A

import           Network.HTTP.Client
import           Network.Socket

------------------------------------------------------------------------------
--- ToSocketAddress
------------------------------------------------------------------------------

class (Eq x, Show x, Display x) => ToSocketAddress x where
    toSocketAddress :: x -> SockAddr

    toSocketFamily :: x -> Family

------------------------------------------------------------------------------
--- DaemonHostAddress
------------------------------------------------------------------------------

data DaemonHostAddress = DaemonHostAddress HostAddress PortNumber
    deriving stock (Eq, Show)

instance Display DaemonHostAddress where
    display (DaemonHostAddress address port) =
        "[" <> tshow address <> " " <> tshow port <> "]"

instance FromJSON DaemonHostAddress where
    parseJSON =
        coerce <$> withText "HostAddress" $ \v -> case A.parseOnly parser v of
            Left message -> error $ "Parser failed: " <> message
            Right result -> pure result
      where
        parser = toAddress <$> word <* sep <*> word <* sep <*> word <* sep
            <*> word <* A.char ':' <*> port

        word :: A.Parser Word8
        word = A.decimal

        port :: A.Parser PortNumber
        port = A.decimal

        sep = A.char '.'

        toAddress a b c d = DaemonHostAddress $ tupleToHostAddress (a, b, c, d)

instance ToSocketAddress DaemonHostAddress where
    toSocketAddress (DaemonHostAddress address port) =
        SockAddrInet port address

    toSocketFamily _ = AF_INET

getPortNumber :: DaemonHostAddress -> PortNumber
getPortNumber (DaemonHostAddress _ port) = port

getHostAddress :: DaemonHostAddress -> HostAddress
getHostAddress (DaemonHostAddress host _) = host

printHostAddress :: DaemonHostAddress -> String
printHostAddress (hostAddressToTuple . getHostAddress -> (a, b, c, d)) =
    show a <> "." <> show b <> "." <> show c <> "." <> show d

------------------------------------------------------------------------------
--- DaemonSocketAddress
------------------------------------------------------------------------------

newtype DaemonSocketAddress = DaemonSocketAddress FilePath
    deriving stock (Eq, Show)
    deriving newtype FromJSON

instance Display DaemonSocketAddress where
    display (DaemonSocketAddress path) = "[" <> tshow path <> "]"

instance ToSocketAddress DaemonSocketAddress where
    toSocketAddress (DaemonSocketAddress path) = SockAddrUnix path

    toSocketFamily _ = AF_UNIX

------------------------------------------------------------------------------
--- resolveSocketManager
------------------------------------------------------------------------------

resolveSocketManager :: (MonadIO m, ToSocketAddress x) => x -> m Manager
resolveSocketManager address = liftIO $ newManager settings
  where
    settings =
        defaultManagerSettings { managerRawConnection = createConnection }
      where
        createConnection = return $ \_ _ _ -> bracketOnError open close inner

        open = socket (toSocketFamily address) Stream defaultProtocol

        inner sock = do
            connect sock $ toSocketAddress address
            socketConnection sock 8192

------------------------------------------------------------------------------
--- bindListenSocket
------------------------------------------------------------------------------

bindListenSocket
    :: (MonadIO m, MonadLogger m, ToSocketAddress x) => x -> m Socket
bindListenSocket address = do
    sock <- liftIO $ socket family Stream 0
    liftIO $ bind sock $ toSocketAddress address
    liftIO $ listen sock 1
    logInfoN $ "Listening at " <> display address
    pure sock
  where
    family = toSocketFamily address

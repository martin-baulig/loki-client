module Watchdog.Settings.LokiConfig
    ( resolveLokiConfig
    , socketLokiURI
    , LokiURI
    , LokiConfig
    , HasLokiConfig(..)
    , SenderDatabasePath
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.Tagged
import qualified Data.Yaml                      as Y

import           Network.URI                    (URI, parseURI)
import           Network.URI.Static

import           Watchdog.Api.Label
import           Watchdog.Settings.DatabasePath
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- SenderDatabasePath
------------------------------------------------------------------------------

data SenderDatabasePathTag

type SenderDatabasePath = Tagged SenderDatabasePathTag DatabasePath

------------------------------------------------------------------------------
--- LokiURI
------------------------------------------------------------------------------

newtype LokiURI = LokiURI URI
    deriving stock (Eq, Show, Generic)

instance FromJSON LokiURI where
    parseJSON = coerce <$> withText "URI" $ \v -> do
        let maybeURI = parseURI $ unpack v
        maybe (error "Bad URI") pure maybeURI

------------------------------------------------------------------------------
--- LokiConfig
------------------------------------------------------------------------------

data LokiConfig =
    LokiConfig { lcUri      :: !LokiURI
               , lcDatabase :: !SenderDatabasePath
               , lcTls      :: !(Maybe TlsSettings)
               , lcLabels   :: !(Maybe Labels)
               , lcMetadata :: !(Maybe Labels)
               , lcUseColor :: !Bool
               , lcPrint    :: !Bool
               , lcDebug    :: !Bool
               }
    deriving (Eq, Generic, Show)

instance FromJSON LokiConfig where
    parseJSON (Y.Object v) =
        LokiConfig <$> v .: "loki-uri" <*> v .: "database-path" <*> v .:? "tls"
        <*> v .:? "labels" <*> v .:? "metadata" <*> v .:? "use-color" .!= True
        <*> v .:? "print" .!= True <*> v .:? "debug" .!= False
    parseJSON _ = error "Invalid LokiConfig."

------------------------------------------------------------------------------
--- LokiConfig
------------------------------------------------------------------------------

resolveLokiConfig :: MonadIO m => FilePath -> m LokiConfig
resolveLokiConfig = Y.decodeFileThrow

socketLokiURI :: LokiURI
socketLokiURI = LokiURI [uri|http://localhost/|]

------------------------------------------------------------------------------
--- HasLokiConfig
------------------------------------------------------------------------------

class HasLokiConfig env where
    lokiConfigL :: Lens' env LokiConfig

    lokiConfigUri :: SimpleGetter env URI
    lokiConfigUri = lokiConfigL . to (coerce lcUri)

    lokiConfigDatabasePath :: SimpleGetter env SenderDatabasePath
    lokiConfigDatabasePath = lokiConfigL . to lcDatabase

    lokiConfigLabels :: SimpleGetter env (Maybe Labels)
    lokiConfigLabels = lokiConfigL . to lcLabels

    lokiConfigMetadata :: SimpleGetter env (Maybe Labels)
    lokiConfigMetadata = lokiConfigL . to lcMetadata

    lokiConfigUseColor :: Lens' env Bool
    lokiConfigUseColor = lokiConfigL
        . lens lcUseColor (\x y -> x { lcUseColor = y })

    lokiConfigPrint :: Lens' env Bool
    lokiConfigPrint = lokiConfigL . lens lcPrint (\x y -> x { lcPrint = y })

    lokiConfigDebug :: Lens' env Bool
    lokiConfigDebug = lokiConfigL . lens lcDebug (\x y -> x { lcDebug = y })

    lokiConfigTls :: SimpleGetter env (Maybe TlsSettings)
    lokiConfigTls = lokiConfigL . to lcTls

instance HasLokiConfig LokiConfig where
    lokiConfigL = id


module Watchdog.Settings.ServerConfig
    ( ServerInfo(..)
    , ServerName(serverName)
    , ServerConfig(..)
    , RestartPolicy(..)
    , HasServerConfig(..)
    , mkServerName
    ) where

import           Baulig.Prelude

import           Control.Monad.Fail

import           Data.Aeson
import qualified Data.Aeson.Key                 as K
import qualified Data.Aeson.KeyMap              as KM
import           Data.Aeson.Types               (Parser)
import           Data.Tagged

import           System.Clock

import           Text.Regex.TDFA

import           Baulig.Utils.PrettyTable

import           Watchdog.Settings.DatabasePath

------------------------------------------------------------------------------
--- ServerName
------------------------------------------------------------------------------

newtype ServerName = ServerName { serverName :: Text }
    deriving stock (Show, Eq, Ord)

instance Display ServerName where
    display this = display $ this <:> serverName this

validServerNameRegex :: Text
validServerNameRegex = "^[a-z]+(-[a-z]+)*[0-9]*$"

mkServerName :: Text -> Either String ServerName
mkServerName name
    | unpack name =~ unpack validServerNameRegex = Right (ServerName name)
    | otherwise = Left $ "Invalid server name: " ++ unpack name

instance FromJSON ServerName where
    parseJSON = withText "ServerName" $ \txt ->
        either fail pure (mkServerName txt)

instance ToJSON ServerName where
    toJSON (ServerName name) = String name

------------------------------------------------------------------------------
--- RestartPolicy
------------------------------------------------------------------------------

data RestartPolicy =
    RestartPolicy { restartPolicyDelay       :: !(Maybe Int)
                    -- ^ Wait N seconds before restarting
                  , restartPolicyMaxFailures :: !(Maybe (Int, TimeSpec))
                    -- ^ (Max failures, time window in seconds)
                  , restartPolicyMaxRestarts :: !(Maybe (Int, TimeSpec))
                    -- ^ (Max restarts, time window in seconds)
                  }
    deriving stock (Eq, Generic, Show)

instance FromJSON RestartPolicy where
    parseJSON = withObject "RestartPolicy" $ \v -> do
        restartPolicyDelay <- v .:? "delay"
        restartPolicyMaxFailures <- parseTimeSpecPair =<< v .:? "maxFailures"
        restartPolicyMaxRestarts <- parseTimeSpecPair =<< v .:? "maxRestarts"
        pure RestartPolicy { .. }

parseTimeSpecPair :: Maybe [Int] -> Parser (Maybe (Int, TimeSpec))
parseTimeSpecPair = \case
    Nothing -> pure Nothing
    Just [ count, sec ] -> pure
        $ Just (count, fromNanoSecs (fromIntegral sec * 1000000000))
    _ -> fail "Expected a list with two integers: [count, time_in_seconds]"

------------------------------------------------------------------------------
--- ServerInfo
------------------------------------------------------------------------------

data ServerInfo = ServerInfo { serverInfoPath          :: !Text
                             , serverInfoArguments     :: ![Text]
                             , serverInfoRestartPolicy :: !(Maybe RestartPolicy)
                             }
    deriving stock (Eq, Generic, Show)

instance Display ServerInfo where
    display this =
        display $ this <:> serverInfoPath this <@> serverInfoArguments this

instance FromJSON ServerInfo where
    parseJSON = withObject "ServerInfo" $ \v -> ServerInfo <$> v .: "path"
        <*> v .:? "arguments" .!= [] <*> v .:? "restartPolicy"

------------------------------------------------------------------------------
--- PrettyItem Instance
------------------------------------------------------------------------------

instance Display (ServerName, ServerInfo) where
    display (name, info) = display $ name <:> info

instance PrettyItem ServerInfo where
    prettyItemName _ = "Server Info"

    prettyItemHeaders _ = ["Path", "Arguments"]

    prettyItemToElements this =
        unpack <$> [serverInfoPath this, unwords $ serverInfoArguments this]

------------------------------------------------------------------------------
--- ServerDatabasePath
------------------------------------------------------------------------------

data ServerDatabasePathTag

type ServerDatabasePath = Tagged ServerDatabasePathTag DatabasePath

------------------------------------------------------------------------------
--- ServerConfig
------------------------------------------------------------------------------

data ServerConfig =
    ServerConfig { serverConfigDatabase   :: !ServerDatabasePath
                 , serverConfigServerList :: ![(ServerName, ServerInfo)]
                 }
    deriving stock (Eq, Generic, Show)

instance Display ServerConfig where
    display this = display
        $ this <:> serverConfigDatabase this <@> serverConfigServerList this

instance FromJSON ServerConfig where
    parseJSON = withObject "ServerConfig" $ \obj -> do
        serverConfigDatabase <- obj .: "databasePath"
        serverConfigServerList
            <- traverse parseEntry . KM.toList =<< obj .: "serverList"
        pure ServerConfig { .. }
      where
        parseEntry (K.toText -> key, value) = do
            serverName <- either fail pure (mkServerName key)
            serverInfo <- parseJSON value
            pure (serverName, serverInfo)

------------------------------------------------------------------------------
--- HasServerConfig
------------------------------------------------------------------------------

class HasServerConfig x where
    serverConfigL :: SimpleGetter x ServerConfig

instance HasServerConfig ServerConfig where
    serverConfigL = id

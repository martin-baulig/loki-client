module Watchdog.Settings.TlsSettings
    ( TlsSettings
    , HasTlsSettings(..)
    , TlsServerSettings
    , HasTlsServerSettings(..)
    , newTlsManager
    , readCertificate
    , setClientCertificate
    , setTrustedCaCertificate
    , toWarpTlsSettings
    ) where

import           Baulig.Prelude

import           Control.Monad.Extra

import           Data.Aeson
import qualified Data.ByteString.Lazy               as BSL
import           Data.X509
import qualified Data.Yaml                          as Y

import           Network.HTTP.Client
import           Network.HTTP.Client.OpenSSL
import           Network.TLS
import qualified Network.Wai.Handler.WarpTLS        as W

import qualified OpenSSL.Session                    as SSL
import           OpenSSL.X509

import           Watchdog.Settings.PostgresSettings

------------------------------------------------------------------------------
--- TlsSettings
------------------------------------------------------------------------------

data TlsSettings = TlsSettings { trustedCaCertificatePath :: !FilePath
                               , clientCertificatePath    :: !FilePath
                               , clientCertificateKeyPath :: !FilePath
                               }
    deriving (Eq, Generic, Show)

instance FromJSON TlsSettings where
    parseJSON (Y.Object v) = TlsSettings <$> v .: "trustedCaCertificatePath"
        <*> v .: "clientCertificatePath" <*> v .: "clientCertificateKeyPath"
    parseJSON _ = error "Invalid TlsSettings configuration."

------------------------------------------------------------------------------
--- HasTlsSettings
------------------------------------------------------------------------------

class HasTlsSettings env where
    tlsSettingsL :: Lens' env TlsSettings

instance HasTlsSettings TlsSettings where
    tlsSettingsL = id

------------------------------------------------------------------------------
--- TlsServerSettings
------------------------------------------------------------------------------

data TlsServerSettings =
    TlsServerSettings { serverCertificatePath :: !FilePath
                      , serverCertificateKeyPath :: !FilePath
                      , serverCertificateTrustedClientCert :: !FilePath
                      }
    deriving (Eq, Generic, Show)

instance FromJSON TlsServerSettings where
    parseJSON (Y.Object v) = TlsServerSettings <$> v .: "serverCertificatePath"
        <*> v .: "serverCertificateKeyPath" <*> v .: "clientCertificatePath"
    parseJSON _ = error "Invalid TlsServerSettings configuration."

------------------------------------------------------------------------------
--- HasTlsServerSettings
------------------------------------------------------------------------------

class HasTlsServerSettings env where
    tlsServerSettingsL :: Lens' env TlsServerSettings

instance HasTlsServerSettings TlsServerSettings where
    tlsServerSettingsL = id

------------------------------------------------------------------------------
--- Reading Certificates
------------------------------------------------------------------------------

readCertificate :: MonadIO m => FilePath -> m X509
readCertificate path = liftIO $ BSL.readFile path >>= readDerX509

setTrustedCaCertificate :: MonadIO m => TlsSettings -> SSL.SSLContext -> m ()
setTrustedCaCertificate settings context = liftIO $ do
    let trustedCA = trustedCaCertificatePath settings
    SSL.contextSetCAFile context trustedCA

setClientCertificate :: MonadIO m => TlsSettings -> SSL.SSLContext -> m ()
setClientCertificate settings context = liftIO $ do
    cert <- readCertificate $ clientCertificatePath settings
    SSL.contextSetCertificate context cert
    SSL.contextSetPrivateKeyFile context $ clientCertificateKeyPath settings

newTlsManager :: MonadIO m => Maybe TlsSettings -> m Manager
newTlsManager maybeSettings = liftIO $ do
    context <- defaultMakeContext defaultOpenSSLSettings
    whenJust maybeSettings $ \settings -> do
        setTrustedCaCertificate settings context
        setClientCertificate settings context
    newManager $ opensslManagerSettings $ pure context

toWarpTlsSettings :: MonadIO m => TlsServerSettings -> m W.TLSSettings
toWarpTlsSettings (TlsServerSettings serverCert key trustedClient) = update
    <$> liftIO (BSL.readFile trustedClient)
  where
    settings = W.tlsSettings serverCert key

    update trusted = settings { W.tlsAllowedVersions = [TLS13]
                              , W.tlsWantClientCert  = True
                              , W.tlsServerHooks     = hooks
                              }
      where
        hooks = def { onClientCertificate = pure . validate }

        equalsTrusted cert = toStrict trusted == encodeSignedObject cert

        validate (CertificateChain []) =
            CertificateUsageReject CertificateRejectAbsent

        validate (CertificateChain [ cert ]) =
            if equalsTrusted cert
            then CertificateUsageAccept
            else CertificateUsageReject CertificateRejectUnknownCA

        validate _ = CertificateUsageReject
            $ CertificateRejectOther "Got multiple client certificates."

instance HasPostgresSettings TlsSettings where
    pgConnectionString tls =
        unwords [ "sslcert=" ++ clientCertificatePath tls
                , "sslkey=" ++ clientCertificateKeyPath tls
                , "sslrootcert=" ++ trustedCaCertificatePath tls
                , "sslmode=verify-ca"
                ]

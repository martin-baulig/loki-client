module Watchdog.Settings.DatabasePath (DatabasePath(..)) where

import           Baulig.Prelude

import           Data.Tagged

------------------------------------------------------------------------------
--- DatabasePath
------------------------------------------------------------------------------

newtype DatabasePath = DatabasePath FilePath
    deriving stock (Eq, Show)
    deriving newtype FromJSON

instance Display DatabasePath where
    display (DatabasePath path) = [i|[DatabasePath #{path}]|]

instance Display (Tagged x DatabasePath) where
    display = display . untag

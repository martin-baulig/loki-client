module Watchdog.Settings.PostgresSettings (HasPostgresSettings(..)) where

import           Baulig.Prelude

------------------------------------------------------------------------------
--- HasPostgresSettings
------------------------------------------------------------------------------

class HasPostgresSettings x where
    pgConnectionString :: x -> String

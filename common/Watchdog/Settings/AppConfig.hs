module Watchdog.Settings.AppConfig (MonadAppConfig(..)) where

import           Baulig.Prelude

------------------------------------------------------------------------------
--- HasAppConfig
------------------------------------------------------------------------------

class MonadIO m => MonadAppConfig m where
    appConfigDirectory :: m FilePath

    getAppConfigFile :: FilePath -> m FilePath
    getAppConfigFile path = getPath <$> appConfigDirectory
      where
        getPath root = root </> path

module Watchdog.Api.Stream
    ( StreamValue(..)
    , Stream(..)
    , Streams(..)
    , EncodeStreams(..)
    , CanPrintLogLine(..)
    , timestampedValue
    , timestampedValueWithMetadata
    , timestampedValueWithMetadata'
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Vector              as V

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api.Label
import           Watchdog.Api.Resample
import           Watchdog.Api.Timestamp

------------------------------------------------------------------------------
--- StreamValue
------------------------------------------------------------------------------

data StreamValue = StreamValue !Timestamp !Text !(Maybe Labels)
    deriving stock (Show, Generic)

instance FromJSON StreamValue where
    parseJSON = withArray "StreamValue" $ \a -> do
        ts <- parseJSON $ a V.! 0
        text <- parseJSON $ a V.! 1
        meta <- if V.length a == 3
                then Just <$> parseJSON (a V.! 2)
                else pure Nothing
        return $ StreamValue ts text meta

instance ToJSON StreamValue where
    toJSON (StreamValue ts text Nothing) =
        Array $ V.fromList [toJSON ts, toJSON text]

    toJSON (StreamValue ts text (Just metadata)) =
        Array $ V.fromList [toJSON ts, toJSON text, toJSON metadata]

instance HasTimestamp StreamValue where
    timestamp (StreamValue ts _ _) = ts

instance CanResample StreamValue where
    resample time (StreamValue old message maybeMeta) =
        StreamValue time message $ Just mapMeta
      where
        new = mkLabel "resampled" $ showTimestamp old

        mapMeta = addLabel new $ fromMaybe mempty maybeMeta

timestampedValue :: MonadIO m => Text -> m StreamValue
timestampedValue message =
    currentTimestamp >>= \ts -> pure $ StreamValue ts message Nothing

timestampedValueWithMetadata
    :: MonadIO m => Text -> [(Text, Text)] -> m StreamValue
timestampedValueWithMetadata message labels = currentTimestamp
    >>= \ts -> pure $ StreamValue ts message $ Just $ fromListOfKeyValue labels

timestampedValueWithMetadata' :: MonadIO m => Text -> [Label] -> m StreamValue
timestampedValueWithMetadata' message labels = currentTimestamp
    >>= \ts -> pure $ StreamValue ts message $ Just $ fromListOfLabels labels

instance HasPrettyPrint StreamValue where
    prettyFormat (StreamValue time text maybeMetadata) = do
        printHeader "Value: "
        prettyFormat time
        forM_ maybeMetadata $ \metadata -> do
            printHeader "\nMetadata:"
            prettyFormat metadata
        printText $ "\n" <> text <> "\n"

instance CanReplayLog StreamValue where
    replayLog _ (StreamValue _ _ Nothing) = pure ()

    replayLog source (StreamValue _ text (Just (getLogLevel' -> level))) =
        logWithoutLoc source level text

------------------------------------------------------------------------------
--- Stream
------------------------------------------------------------------------------

data Stream = Stream !Labels ![StreamValue]
    deriving stock (Generic, Show)

instance ToJSON Stream where
    toJSON (Stream label values) =
        object ["stream" .= label, "values" .= values]

instance FromJSON Stream where
    parseJSON =
        withObject "Stream" $ \o -> Stream <$> o .: "stream" <*> o .: "values"

instance HasPrettyPrint Stream where
    prettyFormat (Stream labels values) = do
        printHeader "Stream: "
        prettyFormat labels
        printText "\n"
        printInterspersed "\n" values

instance CanResample Stream where
    resample time (Stream labels values) =
        Stream labels $ values <&> resample time

instance CanReplayLog Stream where
    replayLog source (Stream _ values) = forM_ values $ replayLog source

------------------------------------------------------------------------------
--- Streams
------------------------------------------------------------------------------

newtype Streams = Streams [Stream]
    deriving stock (Generic, Show)

instance ToJSON Streams where
    toJSON (Streams streams) = object ["streams" .= streams]

instance FromJSON Streams where
    parseJSON = withObject "Streams" $ \o -> Streams <$> o .: "streams"

instance HasPrettyPrint Streams where
    prettyFormat (Streams streams) = printInterspersed "\n" streams

instance CanResample Streams where
    resample time (Streams streams) = Streams $ streams <&> resample time

instance CanReplayLog Streams where
    replayLog source (Streams streams) = forM_ streams $ replayLog source

------------------------------------------------------------------------------
--- EncodeStreams
------------------------------------------------------------------------------

class EncodeStreams x where
    encodeStreams :: x -> LazyByteString

instance EncodeStreams Stream where
    encodeStreams = encode . Streams . singleton

instance EncodeStreams [Stream] where
    encodeStreams = encode . Streams

instance EncodeStreams Streams where
    encodeStreams = encode

------------------------------------------------------------------------------
--- CanPrintLogLine
------------------------------------------------------------------------------

class CanPrintLogLine x where
    printLogLine :: ( MonadIO m
                    , MonadReader env m
                    , HasLogOptions env
                    , HasColorConfig env
                    )
                 => x
                 -> m ()

instance CanPrintLogLine StreamValue where
    printLogLine value
        | StreamValue _ text (Just labels) <- value, (getLogLevel' -> level)
          <- labels = prettyPrint (level, getLogSource labels, text)
        | otherwise = putStrLn "NO LABEL"

instance CanPrintLogLine Stream where
    printLogLine (Stream _ values) = forM_ values printLogLine

instance CanPrintLogLine Streams where
    printLogLine (Streams streams) = forM_ streams printLogLine

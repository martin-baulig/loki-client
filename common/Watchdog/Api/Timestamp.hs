{-# LANGUAGE QuasiQuotes #-}

{-# LANGUAGE TemplateHaskell #-}

module Watchdog.Api.Timestamp
    ( Timestamp
    , HasTimestamp(..)
    , UseTimestamp(..)
    , currentTimestamp
    , showTimestamp
    , createTimestamp
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.Time.Clock
import           Data.Time.Clock.POSIX
import           Data.Time.Format.ISO8601

import           Database.Persist.TH

import           Baulig.Utils.PrettyPrint

------------------------------------------------------------------------------
--- Timestamp
------------------------------------------------------------------------------

newtype Timestamp = Timestamp NominalDiffTime
    deriving stock (Eq, Generic, Read, Show)

instance Display Timestamp where
    display this
        | Timestamp time <- this = display $ this <:!> time

derivePersistField "Timestamp"

instance ToJSON Timestamp where
    toJSON (Timestamp time) = String $ tshow nanoseconds
      where
        nanoseconds :: Integer
        nanoseconds = round $ toRational time

instance FromJSON Timestamp where
    parseJSON =
        withText "Timestamp" $ \(unpack -> value) -> case readMay value of
            Just time -> pure $ Timestamp $ fromInteger time
            Nothing -> error $ "Invalid timestamp value: " ++ value

class HasTimestamp x where
    timestamp :: x -> Timestamp

instance HasTimestamp Timestamp where
    timestamp = id

currentTimestamp :: MonadIO m => m Timestamp
currentTimestamp = liftIO $ Timestamp . (1e9 *) <$> getPOSIXTime

showTimestamp :: Timestamp -> Text
showTimestamp (Timestamp time) = pack formatted
  where
    utcSeconds = posixSecondsToUTCTime $ time / 1e9

    formatted = "[" <> iso8601Show utcSeconds <> "]"

instance HasPrettyPrint Timestamp where
    prettyFormat = printTimestamp . showTimestamp

------------------------------------------------------------------------------
--- UseTimestamp
------------------------------------------------------------------------------

data UseTimestamp = CurrentTimestamp | FixedTimestamp !Timestamp
    deriving stock (Generic, Show)

createTimestamp :: MonadIO m => UseTimestamp -> m Timestamp
createTimestamp CurrentTimestamp = currentTimestamp
createTimestamp (FixedTimestamp fixed) = pure fixed

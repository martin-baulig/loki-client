module Watchdog.Api.Label
    ( Label(..)
    , Labels(..)
    , parseLabel
    , mkLabel
    , fromListOfLabels
    , fromListOfKeyValue
    , toListOfKeyValue
    , addLabel
    , fromLabel
    , getLogLevel'
    , getLogSource
    ) where

import           Baulig.Prelude

import           Data.Aeson
import qualified Data.Aeson.Key           as DAK
import qualified Data.Aeson.KeyMap        as DAK
import qualified Data.Attoparsec.Text     as A
import qualified Data.Yaml                as Y

import           Baulig.Utils.PrettyPrint

------------------------------------------------------------------------------
--- Label
------------------------------------------------------------------------------

data Label = Label !Text !Text
    deriving stock (Eq, Show, Generic)

instance HasPrettyPrint Label where
    prettyFormat (Label key value) = do
        printLabel $ "{" <> key <> "="
        printLabelValue value
        printLabel "}"

parseLabel :: A.Parser Label
parseLabel = do
    key' <- pack <$> A.many1 A.letter
    _ <- A.char '='
    Label key' <$> A.takeText

mkLabel :: Text -> Text -> Label
mkLabel = Label

------------------------------------------------------------------------------
--- Labels
------------------------------------------------------------------------------

newtype Labels = Labels (HashMap Text Text)
    deriving stock (Eq, Show, Generic)

instance Semigroup Labels where
    Labels a <> Labels b = Labels $ a <> b

instance Monoid Labels where
    mempty = Labels mempty

instance ToJSON Labels where
    toJSON (Labels labels) = object (formatLabel <$> mapToList labels)
      where
        formatLabel :: (Text, Text) -> (Key, Value)
        formatLabel (key, value) = (fromString $ unpack key, toJSON value)

instance FromJSON Labels where
    parseJSON (Y.Object (DAK.toList -> kvList)) = do
        Labels . mapFromList <$> mapM parseItem kvList
      where
        parseItem (DAK.toText -> key, value) = (,) key <$> parseJSON value

    parseJSON _ = error "Invalid Label."

instance HasPrettyPrint Labels where
    prettyFormat (Labels labels) =
        printInterspersed " " (uncurry Label <$> mapToList labels)

fromListOfLabels :: [Label] -> Labels
fromListOfLabels labels = Labels $ mapFromList
    $ fmap (\(Label k v) -> (k, v)) labels

fromListOfKeyValue :: [(Text, Text)] -> Labels
fromListOfKeyValue = Labels . mapFromList

toListOfKeyValue :: Labels -> [(Text, Text)]
toListOfKeyValue (Labels hm) = mapToList hm

addLabel :: Label -> Labels -> Labels
addLabel (Label key value) (Labels old) = Labels $ insertMap key value old

fromLabel :: Label -> Labels
fromLabel (Label key value) = Labels $ singletonMap key value

------------------------------------------------------------------------------
--- Log Levels
------------------------------------------------------------------------------

getLogLevel' :: Labels -> LogLevel
getLogLevel' (Labels hm) = case lookup "severity" hm of
    Nothing -> LevelOther "message"
    Just "debug" -> LevelDebug
    Just "info" -> LevelInfo
    Just "warn" -> LevelWarn
    Just "error" -> LevelError
    Just other -> LevelOther other

getLogSource :: Labels -> Maybe LogSource
getLogSource (Labels hm) = lookup "source" hm

module Watchdog.Api.Resample (CanResample(..), resampleFromJSON) where

import           Baulig.Prelude

import           Data.Aeson

import           Watchdog.Api.Timestamp

------------------------------------------------------------------------------

class (FromJSON x, ToJSON x) => CanResample x where
    resample :: Timestamp -> x -> x

withProxy :: Proxy x -> x -> x
withProxy _ = id

resampleFromJSON :: (MonadIO m, MonadThrow m, CanResample x)
                 => Proxy x
                 -> Timestamp
                 -> ByteString
                 -> m ByteString
resampleFromJSON proxy time input = toStrict . encode . withProxy proxy
    . resample time <$> throwDecode (fromStrict input)

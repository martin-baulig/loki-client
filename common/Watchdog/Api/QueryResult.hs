module Watchdog.Api.QueryResult (ResultType(..), QueryResult(..)) where

import           Baulig.Prelude

import           Data.Aeson

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api.Stream

------------------------------------------------------------------------------
--- ResultType
------------------------------------------------------------------------------

data ResultType = VectorResult | StreamsResult
    deriving stock (Enum, Eq, Generic, Ord, Show)

instance FromJSON ResultType where
    parseJSON = withText "ResultType" $ \case
        "vector" -> pure VectorResult
        "streams" -> pure StreamsResult
        other -> error $ "Unknown result type:" ++ unpack other

------------------------------------------------------------------------------
--- QueryResult
------------------------------------------------------------------------------

data QueryResult = QueryResult !ResultType ![Stream]
    deriving stock (Generic, Show)

instance FromJSON QueryResult where
    parseJSON = withObject "QueryResult" $ \o -> do
        QueryResult <$> o .: "resultType" <*> o .: "result"

------------------------------------------------------------------------------
--- printQueryResult
------------------------------------------------------------------------------

instance HasPrettyPrint QueryResult where
    prettyFormat (QueryResult _ streams) = do
        printTitle $ "\n" <> sep <> "\nQuery result:\n" <> sep <> "\n"
        printInterspersed "\n\n-----\n" streams
        printTitle $ sep <> "\n"
      where
        sep = replicate 80 '='

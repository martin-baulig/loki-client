module Watchdog.Api
    (  -- Response types
      ResponseStatus
    , ApiResponse
    , decodeApiResponse
      -- Modules
    , module Watchdog.Api.Timestamp
    , module Watchdog.Api.Label
    , module Watchdog.Api.QueryResult
    , module Watchdog.Api.Stream
    ) where

import           Baulig.Prelude

import           Data.Aeson

import           Watchdog.Api.Label
import           Watchdog.Api.QueryResult
import           Watchdog.Api.Stream
import           Watchdog.Api.Timestamp

------------------------------------------------------------------------------
--- ResponseStatus
------------------------------------------------------------------------------

data ResponseStatus = SuccessStatus | ErrorStatus !Text
    deriving stock (Show, Generic)

instance FromJSON ResponseStatus where
    parseJSON = withText "Foo" $ \case
        "success" -> return SuccessStatus
        other -> return $ ErrorStatus other

------------------------------------------------------------------------------
--- API Response
------------------------------------------------------------------------------

data ApiResponse x = ApiResponse !ResponseStatus !x
    deriving stock (Show, Generic)

instance FromJSON x => FromJSON (ApiResponse x) where
    parseJSON = withObject "ApiResponse" $ \o -> do
        ApiResponse <$> o .: "status" <*> o .: "data"

------------------------------------------------------------------------------
--- Helper methods
------------------------------------------------------------------------------

decodeApiResponse :: FromJSON x => Proxy x -> LazyByteString -> x
decodeApiResponse _ body = case eitherDecode body of
    Right (ApiResponse SuccessStatus value) -> value
    Right (ApiResponse (ErrorStatus status) _) ->
        error $ "API request returned unknown status: " <> unpack status
    Left message -> error $ "Failed to parse API response: " ++ message

module Watchdog.Utils.IPUtils
    (
     readIPv4
    , readIPv4Range
    , cidrToEndIP
    , isIPInCIDR
    , isIPInRanges
    ) where

import           Baulig.Prelude

import           Data.Bits
import           Data.IP


import           Text.Read

------------------------------------------------------------------------------
--- Utils
------------------------------------------------------------------------------

readIPv4Range :: String -> Maybe (AddrRange IPv4)
readIPv4Range s = case break (== '/') s of
    (ipStr, '/' : maskStr) -> do
        ip <- readIPv4 ipStr
        mask' <- readMaybe maskStr
        Just (makeAddrRange ip mask')
    _ -> Nothing

readIPv4 :: String -> Maybe IPv4
readIPv4 s = case mapM readMaybe (split '.' s) of
    Just [ a, b, c, d ]
        | all (<= 255) [a, b, c, d] -> Just (toIPv4 [a, b, c, d])
    _ -> Nothing

split :: Char -> String -> [String]
split _ "" = []
split delim s = let (first', rest) = break (== delim) s
                in
                    first' : case rest of
                        [] -> []
                        (_ : xs) -> split delim xs

isIPInRanges :: IPv4 -> [AddrRange IPv4] -> Bool
isIPInRanges = any . isIPInCIDR

-- Convert IPv4 to Word32 for arithmetic
ipv4ToWord32 :: IPv4 -> Word32
ipv4ToWord32 ip = case fromIPv4 ip of
    [ a, b, c, d ] -> fromIntegral a `shiftL` 24 .|. fromIntegral b `shiftL` 16
        .|. fromIntegral c `shiftL` 8 .|. fromIntegral d
    _ -> error "fromIPv4 returned an unexpected number of elements"

-- Convert Word32 back to IPv4
word32ToIPv4 :: Word32 -> IPv4
word32ToIPv4 w = toIPv4 [ fromIntegral (w `shiftR` 24 .&. 0xFF)
                        , fromIntegral (w `shiftR` 16 .&. 0xFF)
                        , fromIntegral (w `shiftR` 8 .&. 0xFF)
                        , fromIntegral (w .&. 0xFF)
                        ]

-- Compute the last IP in a CIDR range
cidrToEndIP :: IPv4 -> Int -> IPv4
cidrToEndIP startIp maskBits =
    let startNum = ipv4ToWord32 startIp
        mask' = complement (shiftL 1 (32 - maskBits) - 1)  -- Subnet mask
        endNum = startNum .|. complement mask'             -- Compute last IP in range
    in
        word32ToIPv4 endNum

-- Check if an IP falls within a CIDR range
isIPInCIDR :: IPv4 -> AddrRange IPv4 -> Bool
isIPInCIDR ip range =
    let (start, maskBits) = addrRangePair range
        end = cidrToEndIP start maskBits
        ipNum = ipv4ToWord32 ip
    in
        ipv4ToWord32 start <= ipNum && ipNum <= ipv4ToWord32 end


module Watchdog.Utils.ConduitUtils
    ( foldTransformConduit
    , filterMapC
    , filterMapMC
    , invokeM2
    , invokeM3
    , invokeM4
    , displaySink
    , yieldMaybe
    ) where

import           Baulig.Prelude

------------------------------------------------------------------------------
--- invokeM2, invokeM3, invokeM4
------------------------------------------------------------------------------

invokeM2 :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
invokeM2 func f1 f2 = do
    a1 <- f1
    a2 <- f2
    func a1 a2

invokeM3 :: Monad m => (a -> b -> c -> m d) -> m a -> m b -> m c -> m d
invokeM3 func f1 f2 f3 = do
    a1 <- f1
    a2 <- f2
    a3 <- f3
    func a1 a2 a3

invokeM4
    :: Monad m => (a -> b -> c -> d -> m e) -> m a -> m b -> m c -> m d -> m e
invokeM4 func f1 f2 f3 f4 = do
    a1 <- f1
    a2 <- f2
    a3 <- f3
    a4 <- f4
    func a1 a2 a3 a4

------------------------------------------------------------------------------
--- foldTransformConduit
------------------------------------------------------------------------------

foldTransformConduit
    :: Monad m => (a -> b -> m (c, b)) -> b -> ConduitT a c m ()
foldTransformConduit func = loop
  where
    loop current = await >>= \case
        Nothing -> pure ()
        Just item -> do
            (mapped, next) <- lift $ func item current
            yield mapped
            loop next

------------------------------------------------------------------------------
--- filterMapC
------------------------------------------------------------------------------

yieldMaybe :: Monad m => Maybe o -> ConduitT i o m ()
yieldMaybe (Just item) = yield item
yieldMaybe Nothing = pure ()

filterMapC :: Monad m => (a -> Maybe b) -> ConduitT a b m ()
filterMapC func = awaitForever $ yieldMaybe . func

filterMapMC :: Monad m => (a -> m (Maybe b)) -> ConduitT a b m ()
filterMapMC func = awaitForever $ \item -> lift (func item) >>= yieldMaybe

------------------------------------------------------------------------------
--- displaySink
------------------------------------------------------------------------------

displaySink
    :: (MonadIO m, MonadLogger m, Display x) => Text -> ConduitT x Void m ()
displaySink message =
    awaitForever $ \item -> logMessageN $ message <> ": " <> display item

module Watchdog.Utils.InternalError
    ( InternalErrorException(..)
    , internalErrorException
    ) where

import           Baulig.Prelude

import           GHC.Stack

------------------------------------------------------------------------------
--- ReplicationException
------------------------------------------------------------------------------

data InternalErrorException where
    InternalErrorException
        :: HasCallStack => !Text -> CallStack -> InternalErrorException

instance Exception InternalErrorException

instance Show InternalErrorException where
    show = unpack . display

instance Display InternalErrorException where
    display (InternalErrorException msg stack) = "[InternalErrorException "
        <> display msg <> "]\n" <> fromString (prettyCallStack stack) <> "\n"

internalErrorException :: HasCallStack => Text -> InternalErrorException
internalErrorException msg = InternalErrorException msg callStack

module Watchdog.Utils.AsyncOperation
    ( AsyncOperation
    , startAsyncOperation
    , waitForAsyncOperation
    , finishAsyncOperation
    , cancelAsyncOperation
    ) where

import           Baulig.Prelude

import           UnliftIO.Concurrent

import           Watchdog.Utils.ConduitUtils

------------------------------------------------------------------------------
--- AsyncOperation
------------------------------------------------------------------------------

data AsyncOperation a b =
    AsyncOperation { aoName     :: !Text
                   , aoCancel   :: !(a -> IO ())
                   , aoReady    :: !(TMVar (Maybe SomeException))
                   , aoInstance :: !(TVar (Maybe a))
                   , aoAsync    :: !(Async b)
                   }

instance (Typeable a, Typeable b) => Display (AsyncOperation a b) where
    display this = display $ this <:> aoName this

startAsyncOperation
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m)
    => Text
    -> m a
    -> (a -> m ())
    -> (a -> m b)
    -> (SomeException -> m ())
    -> (a -> IO ())
    -> m (AsyncOperation a b)
startAsyncOperation name initFunc finalFunc mainFunc errorFunc cancelFunc =
    invokeM2 inner newEmptyTMVarIO (newTVarIO Nothing)
  where
    inner readyVar instanceVar =
        AsyncOperation name cancelFunc readyVar instanceVar
        <$> async runOperation
      where
        runOperation = withException (bracket initFunc finalFunc runMain)
            $ \exc -> finally (printException exc >> errorFunc exc)
                              (setException exc)

        setException exc = atomically $ do
            void $ tryPutTMVar readyVar $ Just exc
            writeTVar instanceVar Nothing

        setReady value = atomically $ do
            void $ tryPutTMVar readyVar Nothing
            writeTVar instanceVar $ Just value

        runMain value = setReady value >> mainFunc value

waitForAsyncOperation
    :: ( Typeable a
       , Typeable b
       , MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , HasCallStack
       )
    => AsyncOperation a b
    -> m ()
waitForAsyncOperation this = do
    logVerboseN $ "Wait for async operation: " <> display this
    atomically (readTMVar $ aoReady this) >>= \case
        Just exc -> do
            printException exc
            throwM exc
        Nothing -> logVerboseN "Replication operation ready."

finishAsyncOperation
    :: ( Typeable a
       , Typeable b
       , MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , HasCallStack
       )
    => AsyncOperation a b
    -> m b
finishAsyncOperation this = do
    logVerboseN $ "Waiting for async operation to finish: " <> display this
    waitCatch (aoAsync this) >>= \case
        Left exc -> do
            printException exc
            throwM exc
        Right result -> pure result

cancelAsyncOperation
    :: (Typeable a, Typeable b, MonadUnliftIO m, MonadLogger m)
    => AsyncOperation a b
    -> m ()
cancelAsyncOperation this = do
    logMessageN $ "Canceling async operation: " <> display this
    readTVarIO (aoInstance this) >>= \case
        Just value -> do
            liftIO $ aoCancel this value
            logMessageN "Waiting 5 seconds for operations to shut down cleanly."
            threadDelay 5000000
        Nothing -> pure ()
    logMessageN "Canceling async operation."
    cancel $ aoAsync this

module Watchdog.Vault.ZoneDatabase
    ( PureZoneDatabase
    , OverrideConfig
    , HasOverrideConfig(..)
    , ZoneDatabaseConfig
    , loadZoneDatabaseFromConfig
    , loadZoneDatabaseFromConfigFile
    , module Watchdog.Vault.ZoneDatabase.Lookup
    ) where

import           Baulig.Prelude

import           Watchdog.Vault.ZoneDatabase.Lookup
import           Watchdog.Vault.ZoneDatabase.Override
import           Watchdog.Vault.ZoneDatabase.Pure
import           Watchdog.Vault.ZoneDatabase.Types

------------------------------------------------------------------------------
--- Load Pure Zone Database
------------------------------------------------------------------------------

loadZoneDatabaseFromConfigFile
    :: (MonadUnliftIO m, MonadLoggerIO m) => FilePath -> m PureZoneDatabase
loadZoneDatabaseFromConfigFile path = do
    config <- resolveZoneDatabaseConfig path
    withReadOnlyZoneManager config loadPureZoneDatabase

loadZoneDatabaseFromConfig :: (MonadUnliftIO m, MonadLoggerIO m)
                           => ZoneDatabaseConfig
                           -> m PureZoneDatabase
loadZoneDatabaseFromConfig config =
    withReadOnlyZoneManager config loadPureZoneDatabase

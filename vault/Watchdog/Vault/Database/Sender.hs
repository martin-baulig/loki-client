module Watchdog.Vault.Database.Sender
    ( SenderDatabase
    , SenderDatabasePath
    , SenderDatabaseCounter
    , SenderDatabaseGauge
    , SenderDatabaseMultiGauge
    , SenderRegistry
    , SenderRunningGauge
    , MonadSenderDatabase(..)
    , withSenderDatabase
    , Message(messageBody)
    , MessageStatus(..)
    , createSenderRegistry
    , insertPendingStream
    , getPendingMessages
    , getErrorMessages
    , countPending
    , countErrors
    , countMessages
    , updateStatus
    , setResampled
    , module Watchdog.Vault.Database.Types
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Database.Persist
import           Database.Persist.Sqlite

import           Watchdog.Api.Stream
import           Watchdog.Api.Timestamp
import           Watchdog.Settings.LokiConfig
import           Watchdog.Vault.Database.Message
import           Watchdog.Vault.Database.Types
import           Watchdog.Vault.Metrics.Registry
import           Watchdog.Vault.Metrics.Running
import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- SenderDatabase
------------------------------------------------------------------------------

class MonadUnliftIO m => MonadSenderDatabase m where
    runSenderDatabase :: ReaderT SqlBackend IO x -> m x

data SenderTag

type SenderRunningGauge = RunningGauge SenderTag

type SenderDatabase = Tagged SenderTag Database

type SenderDatabaseCounter = DatabaseCounter SenderTag SenderTag

type SenderDatabaseGauge = DatabaseGauge SenderTag SenderTag

type SenderDatabaseMultiGauge = DatabaseMultiGauge SenderTag SenderTag

withSenderDatabase :: (MonadUnliftIO m, MonadLogger m)
                   => SenderDatabasePath
                   -> (SenderDatabase -> m x)
                   -> m x
withSenderDatabase (retag @_ @_ @SenderTag -> path) = withDatabase path $ do
    runMigration migrateMessage

------------------------------------------------------------------------------
--- SenderRegistry
------------------------------------------------------------------------------

data SenderRegistry =
    SenderRegistry { srRunning  :: !(RunningGauge SenderTag)
                   , srRegistry :: !(MetricsRegistry SenderTag)
                   }

instance HasRunningGauge SenderRegistry where
    setRunningGauge = setRunningGauge . srRunning

instance HasMetricsRegistry SenderTag SenderRegistry where
    metricsRegistryL = to srRegistry

instance HasMetricsRegistryCollection SenderRegistry where
    metricsRegistryCollectionL = to srRegistry . metricsRegistryCollectionL

countMessagesMetric :: SenderDatabase -> SenderDatabaseCounter
countMessagesMetric db = DatabaseCounter db
                                         "loki_client_message_count"
                                         "Number of successful messages."
                                         [MessageStatus ==. Success]

pendingGauge :: SenderDatabase -> [MetricLabel] -> SenderDatabaseGauge
pendingGauge db = DatabaseGauge db
                                "loki_client_pending"
                                "Number of pending messages."
                                [MessageStatus ==. Pending]

errorGauge :: SenderDatabase -> [MetricLabel] -> SenderDatabaseGauge
errorGauge db = DatabaseGauge db
                              "loki_client_errors"
                              "Number of error messages."
                              [MessageStatus ==. Failed]

createSenderRegistry
    :: MonadIO m => SenderDatabase -> [MetricLabel] -> m SenderRegistry
createSenderRegistry senderDb extraLabels = do
    srRunning <- runningGauge "loki_client" mempty
    srRegistry
        <- createMetricsRegistry extraLabels
                                 [ AnyMetric srRunning
                                 , AnyMetric $ countMessagesMetric senderDb
                                 , AnyMetric $ pendingGauge senderDb mempty
                                 , AnyMetric $ errorGauge senderDb mempty
                                 ]

    pure SenderRegistry { .. }

------------------------------------------------------------------------------
--- Database
------------------------------------------------------------------------------

insertPendingStream
    :: MonadUnliftIO m => SenderDatabase -> Stream -> m (Key Message)
insertPendingStream db value = runDatabaseOp db $ insert =<< mkMessage value

getPendingMessages :: MonadSenderDatabase m => m [Entity Message]
getPendingMessages =
    runSenderDatabase $ selectList [MessageStatus ==. Pending] []

getErrorMessages :: MonadSenderDatabase m => m [Entity Message]
getErrorMessages =
    runSenderDatabase $ selectList [MessageStatus ==. Failed] [LimitTo 25]

countPending :: MonadSenderDatabase m => m Int
countPending = runSenderDatabase $ count [MessageStatus ==. Pending]

countErrors :: MonadSenderDatabase m => m Int
countErrors = runSenderDatabase $ count [MessageStatus ==. Failed]

countMessages :: MonadSenderDatabase m => m Int
countMessages = runSenderDatabase $ count [MessageStatus ==. Success]

updateStatus :: MonadSenderDatabase m => Key Message -> MessageStatus -> m ()
updateStatus key status =
    runSenderDatabase $ update key [MessageStatus =. status]

setResampled :: MonadSenderDatabase m => Key Message -> Timestamp -> m ()
setResampled key time = runSenderDatabase
    $ update key [MessageStatus =. Success, MessageResampled =. Just time]

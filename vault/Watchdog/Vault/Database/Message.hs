{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Vault.Database.Message where

import           Baulig.Prelude

import           Data.Aeson              (encode)
import           Data.Digest.Adler32

import           Database.Persist.Sqlite
import           Database.Persist.TH

import           Watchdog.Api.Stream
import           Watchdog.Api.Timestamp

------------------------------------------------------------------------------
--- Message
------------------------------------------------------------------------------

data MessageStatus = Pending | Failed | Success
    deriving stock (Enum, Eq, Generic, Read, Show)

derivePersistField "MessageStatus"

share [mkPersist sqlSettings, mkMigrate "migrateMessage"]
      [persistLowerCase|
Message
    timestamp Timestamp
    serial Word32
    status MessageStatus
    body ByteString
    resampled Timestamp Maybe
    UniqueSerial timestamp serial
    deriving Show
|]

mkMessage :: MonadIO m => Stream -> m Message
mkMessage value = do
    now <- currentTimestamp
    let body = encode $ Streams [value]
    pure $ Message now (adler32 body) Pending (toStrict body) Nothing

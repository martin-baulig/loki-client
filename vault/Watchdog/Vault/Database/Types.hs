module Watchdog.Vault.Database.Types
    ( Database
    , runDatabaseOp
    , withDatabase
    , withReadOnlyDatabase
    , module Watchdog.Settings.DatabasePath
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Database.Persist.Sqlite

import           Watchdog.Settings.DatabasePath
import           Watchdog.Utils.InternalError

------------------------------------------------------------------------------
--- Database
------------------------------------------------------------------------------

data Database =
    Database { dbLock :: !(MVar ()), dbBackend :: !(TVar (Maybe SqlBackend)) }

runDatabaseOp
    :: MonadUnliftIO m => Tagged d Database -> ReaderT SqlBackend IO x -> m x
runDatabaseOp (untag -> database)
              action = bracket_ (takeMVar lock) (putMVar lock ()) $ do
    readTVarIO (dbBackend database) >>= \case
        Just backend -> liftIO $ runReaderT action backend
        Nothing -> throwIO
            $ internalErrorException "Database has already been closed."
  where
    lock = dbLock database

withDatabase :: (MonadUnliftIO m, MonadLogger m)
             => Tagged s DatabasePath
             -> ReaderT SqlBackend IO ()
             -> (Tagged s Database -> m x)
             -> m x
withDatabase path =
    withDatabase' path $ mkSqliteConnectionInfo rawPath & walEnabled .~ True
  where
    DatabasePath (pack -> rawPath) = untag path

withReadOnlyDatabase :: (MonadUnliftIO m, MonadLogger m)
                     => Tagged s DatabasePath
                     -> (Tagged s Database -> m x)
                     -> m x
withReadOnlyDatabase path = withDatabase' path info $ pure ()
  where
    DatabasePath rawPath = untag path

    info = mkSqliteConnectionInfo [i|file:#{rawPath}?immutable=1|]

withDatabase' :: (MonadUnliftIO m, MonadLogger m)
              => Tagged s DatabasePath
              -> SqliteConnectionInfo
              -> ReaderT SqlBackend IO ()
              -> (Tagged s Database -> m x)
              -> m x
withDatabase' (untag -> DatabasePath path) info initialize main = do
    logDebugN [i|With Database: #{info}|]

    withRunInIO $ \io -> runSqliteInfo info
        $ flip onException (liftIO $ io $ logInfoN "ERROR") $ do
            (liftIO . io)
                $ logDebugN [i|Got dababase connection: #{path} - #{info}|]

            backend <- ask

            mapReaderT liftIO initialize

            (liftIO . io) $ logDebugN [i|Database #{path} initialized.|]

            database <- Database <$> newMVar () <*> newTVarIO (Just backend)

            result <- liftIO $ io $ main $ Tagged database

            (liftIO . io) $ logDebugN [i|Database #{path} done.|]

            atomically $ writeTVar (dbBackend database) Nothing

            pure result

{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Watchdog.Vault.Database.IPBlock where

import           Baulig.Prelude

import           Data.Bits
import           Data.IP

import           Database.Persist.Sqlite
import           Database.Persist.TH

------------------------------------------------------------------------------
--- IPBlock
------------------------------------------------------------------------------

share [mkPersist sqlSettings, mkMigrate "migrateIpBlock"]
      [persistLowerCase|
IpBlock
    startIp Word32
    endIp Word32
    country Text
    override Bool default=False

    deriving Show
|]

------------------------------------------------------------------------------
--- Utils
------------------------------------------------------------------------------

ipv4ToWord32 :: IPv4 -> Word32
ipv4ToWord32 ip = case fromIPv4 ip of
    [ a, b, c, d ] -> fromIntegral a `shiftL` 24 .|. fromIntegral b `shiftL` 16
        .|. fromIntegral c `shiftL` 8 .|. fromIntegral d

    _ -> error "fromIPv4 returned an unexpected number of elements"

word32ToIPv4 :: Word32 -> IPv4
word32ToIPv4 w = toIPv4 [ fromIntegral (w `shiftR` 24 .&. 0xFF)
                        , fromIntegral (w `shiftR` 16 .&. 0xFF)
                        , fromIntegral (w `shiftR` 8 .&. 0xFF)
                        , fromIntegral (w .&. 0xFF)
                        ]

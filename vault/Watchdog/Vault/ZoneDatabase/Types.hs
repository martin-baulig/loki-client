module Watchdog.Vault.ZoneDatabase.Types
    ( ZoneDatabase
    , ZoneDatabaseConfig(..)
    , ZoneDatabasePath
    , ZoneManager
    , ZoneManagerT
    , MonadZoneDatabase(..)
    , resolveZoneDatabaseConfig
    , runZoneManagerT
    , withZoneManager
    , withReadOnlyZoneManager
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.Tagged
import qualified Data.Yaml                            as Y

import           Database.Persist.Sqlite              hiding (LogFunc)

import           Watchdog.Vault.Database.IPBlock
import           Watchdog.Vault.Database.Types
import           Watchdog.Vault.ZoneDatabase.Override

------------------------------------------------------------------------------
--- ZoneDatabaseConfig
------------------------------------------------------------------------------

data ZoneDatabaseConfig =
    ZoneDatabaseConfig { zoneDatabasePath :: !ZoneDatabasePath
                       , overrideConfig   :: !OverrideConfig
                       }
    deriving stock (Eq, Show, Generic)

instance FromJSON ZoneDatabaseConfig where
    parseJSON = withObject "ZoneDatabaseConfig" $ \o ->
        ZoneDatabaseConfig <$> o .: "database-path" <*> o .: "override-config"

resolveZoneDatabaseConfig :: MonadIO m => FilePath -> m ZoneDatabaseConfig
resolveZoneDatabaseConfig = Y.decodeFileThrow

------------------------------------------------------------------------------
--- ZoneDatabase
------------------------------------------------------------------------------

class (MonadLoggerIO m, MonadUnliftIO m) => MonadZoneDatabase m where
    runZoneDatabase :: ReaderT SqlBackend IO x -> m x

    askOverrideConfig :: m OverrideConfig

data ZoneDatabaseTag

type ZoneDatabase = Tagged ZoneDatabaseTag Database

type ZoneDatabasePath = Tagged ZoneDatabaseTag DatabasePath

withZoneDatabase :: (MonadUnliftIO m, MonadLogger m)
                 => ZoneDatabasePath
                 -> (ZoneDatabase -> m x)
                 -> m x
withZoneDatabase (retag @_ @_ @ZoneDatabaseTag -> path) = withDatabase path $ do
    runMigration migrateIpBlock

withReadOnlyZoneDatabase :: (MonadUnliftIO m, MonadLogger m)
                         => ZoneDatabasePath
                         -> (ZoneDatabase -> m x)
                         -> m x
withReadOnlyZoneDatabase (retag @_ @_ @ZoneDatabaseTag -> path) =
    withReadOnlyDatabase path

------------------------------------------------------------------------------
--- ZoneManager
------------------------------------------------------------------------------

data ZoneManager = ZoneManager { zmConfig   :: !ZoneDatabaseConfig
                               , zmDatabase :: !ZoneDatabase
                               , zmLogFunc  :: !LogFunc
                               }

------------------------------------------------------------------------------
--- ZoneManagerT
------------------------------------------------------------------------------

newtype ZoneManagerT m x =
    ZoneManagerT { unZoneManagerT :: ReaderT ZoneManager m x }
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadReader ZoneManager, MonadThrow, MonadTrans)

instance MonadIO m => MonadLogger (ZoneManagerT m) where
    monadLoggerLog loc source level msg = do
        logFunc <- asks zmLogFunc
        liftIO $ logFunc loc source level $ toLogStr msg

instance MonadUnliftIO m => MonadZoneDatabase (ZoneManagerT m) where
    runZoneDatabase func = do
        db <- asks zmDatabase
        runDatabaseOp db func

    askOverrideConfig = asks $ overrideConfig . zmConfig

instance MonadIO m => MonadLoggerIO (ZoneManagerT m) where
    askLoggerIO = asks zmLogFunc

runZoneManagerT :: ZoneManager -> ZoneManagerT m x -> m x
runZoneManagerT manager = flip runReaderT manager . unZoneManagerT

withZoneManager :: (MonadUnliftIO m, MonadLoggerIO m)
                => ZoneDatabaseConfig
                -> ZoneManagerT m x
                -> m x
withZoneManager config func = do
    withZoneDatabase (zoneDatabasePath config) $ \db -> do
        manager <- ZoneManager config db <$> askLoggerIO
        runZoneManagerT manager func

withReadOnlyZoneManager :: (MonadUnliftIO m, MonadLoggerIO m)
                        => ZoneDatabaseConfig
                        -> ZoneManagerT m x
                        -> m x
withReadOnlyZoneManager config func = do
    withReadOnlyZoneDatabase (zoneDatabasePath config) $ \db -> do
        manager <- ZoneManager config db <$> askLoggerIO
        runZoneManagerT manager func

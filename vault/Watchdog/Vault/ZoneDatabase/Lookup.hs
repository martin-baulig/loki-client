module Watchdog.Vault.ZoneDatabase.Lookup
    ( ZoneLookupResult(..)
    , lookupIPFromDatabase
    , prettyFormatLookupResult
    , ansiFormatLookupResult
    , lokiLabelsFromLookupResult
    ) where

import           Baulig.Prelude

import qualified Data.ByteString.Char8                 as B8
import           Data.IP
import qualified Data.Map                              as Map

import           System.Console.ANSI

import           Watchdog.Api
import           Watchdog.Utils.IPUtils
import           Watchdog.Vault.Database.IPBlock
import           Watchdog.Vault.ZoneDatabase.Countries
import           Watchdog.Vault.ZoneDatabase.Override
import           Watchdog.Vault.ZoneDatabase.Pure
import           Watchdog.Vault.ZoneDatabase.Utils

------------------------------------------------------------------------------
--- ZoneLookupResult
------------------------------------------------------------------------------

-- | ZoneDatabase lookup result.
data ZoneLookupResult = InternalNetwork !IPv4 !Text
                      | TrustedIP !IPv4 !Text
                      | RegionalISP !IPv4 !Text
                      | CustomLabel !IPv4 !Text
                      | CountryZone !IPv4 !Text !(Maybe Text)
                      | NotFound !IPv4
    deriving stock (Eq, Generic, Show)

-- | Pretty format ZoneLookupResult.
prettyFormatLookupResult :: ZoneLookupResult -> Text
prettyFormatLookupResult = \case
    InternalNetwork ip label -> [i|#{ip} → Internal Network #{label}|]
    TrustedIP ip label -> [i|#{ip} → Trusted IP: #{label}|]
    RegionalISP ip label -> [i|#{ip} → Regional ISP: #{label}|]
    CustomLabel ip label -> [i|#{ip} → Custom Label: #{label}|]
    CountryZone ip country (Just region) ->
        [i|#{ip} → Country: #{country} (Region: #{region})|]
    CountryZone ip country Nothing ->
        [i|#{ip} → Country: #{country} (No Region Available)|]
    NotFound ip -> [i|#{ip} → Not Found|]

-- | Format ZoneLookupResult with ANSI colors.
ansiFormatLookupResult :: Bool -> ZoneLookupResult -> ByteString
ansiFormatLookupResult useColor = \case
    InternalNetwork ip name -> format Cyan [i|#{ip} (#{name})|]
    TrustedIP ip name -> format Green [i|#{ip} (#{name})|]
    RegionalISP ip name -> format Yellow [i|#{ip} (#{name})|]
    CustomLabel ip label -> format Blue [i|#{ip} (#{label})|]
    CountryZone ip country (Just region) -> [i|#{ip} (#{region} - #{country})|]
    CountryZone ip country Nothing -> [i|#{ip} (#{country})|]
    NotFound ip -> format Magenta [i|#{ip}|]
  where
    format color text
        | useColor = B8.concat $ ansiColor color text
        | otherwise = text

-- | Compute Loki [Label] list from ZoneLookupResult.
lokiLabelsFromLookupResult :: ZoneLookupResult -> [Label]
lokiLabelsFromLookupResult = \case
    InternalNetwork _ name -> withLabel "internal" name
    TrustedIP _ name -> withLabel "trusted" name
    RegionalISP _ name -> withLabel "regional" name
    CustomLabel _ name -> withLabel "custom" name
    CountryZone _ country (Just region) ->
        mkLabel "region" region : withLabel "zone" country
    CountryZone _ country Nothing -> withLabel "unknown" country
    _ -> mempty
  where
    withLabel name value =
        [mkLabel "zone_type" name, mkLabel "zone_label" value]

-- | RFC 1918 Private Address Ranges
rfc1918Ranges :: [AddrRange IPv4]
rfc1918Ranges = [ makeAddrRange (toIPv4 [10, 0, 0, 0]) 8
                , makeAddrRange (toIPv4 [172, 16, 0, 0]) 12
                , makeAddrRange (toIPv4 [192, 168, 0, 0]) 16
                ]

-- | Lookup IPv4 address from PureZoneDatabase.
lookupIPFromDatabase :: PureZoneDatabase -> IPv4 -> ZoneLookupResult
lookupIPFromDatabase db ip
    | Just name <- Map.lookup ip (trustedIps overrides) = TrustedIP ip name

    | Just label <- lookupRangeMap ip (customLabels overrides) =
        CustomLabel ip label

    | Just label <- lookupRangeMap ip (internalNetworks overrides) =
        InternalNetwork ip label

    | isIPInRanges ip rfc1918Ranges =
        InternalNetwork ip "RFC 1918 Private Network"

    | Just label <- lookupRangeMap ip (regionalIsp overrides) =
        RegionalISP ip label

    | Just block <- lookupIPPure db ip, country
      <- ipBlockCountry block = case Map.lookup country countryInfoMap of
        Just match -> CountryZone ip (fst match) (Just $ snd match)
        Nothing -> CountryZone ip country Nothing

    | otherwise = NotFound ip
  where
    overrides = db ^. overrideConfigL

module Watchdog.Vault.ZoneDatabase.Command
    ( ZoneDatabaseCommand
    , zoneDatabaseCommandParser
    , runZoneDatabaseCommand
    ) where

import           Baulig.Prelude

import           Data.IP

import           Options.Applicative

import           System.Log.FastLogger

import           Baulig.Utils.PrettyLogger

import           Watchdog.Settings.AppConfig
import           Watchdog.Vault.ZoneDatabase.Lookup
import           Watchdog.Vault.ZoneDatabase.Parser
import           Watchdog.Vault.ZoneDatabase.Pure
import           Watchdog.Vault.ZoneDatabase.Types
import           Watchdog.Vault.ZoneDatabase.Utils

------------------------------------------------------------------------------
--- ZoneDatabaseCommand
------------------------------------------------------------------------------

data ZoneDatabaseCommand =
    ParseZoneFile !FilePath | ScanZoneDirectory !FilePath | LookupIP !IPv4
    deriving stock (Eq, Show)

zoneDatabaseCommandParser :: Parser ZoneDatabaseCommand
zoneDatabaseCommandParser = hsubparser $ commandGroup "Zone database commands:"
    <> command "parse-zone"
               (info (ParseZoneFile <$> argument str (metavar "FILE"))
                     (progDesc "Parse a IP zone file."))
    <> command "scan-zones"
               (info (ScanZoneDirectory <$> argument str (metavar "DIR"))
                     (progDesc "Scan all zone files in directory."))
    <> command "lookup-ip"
               (info (LookupIP <$> argument parseIPv4 (metavar "IPv4_ADDRESS"))
                     (progDesc "Look up an IPv4 address in the database."))

------------------------------------------------------------------------------
--- ParseZoneFile
------------------------------------------------------------------------------

runParseZoneFile :: MonadUnliftIO m => FilePath -> ZoneManagerT m ()
runParseZoneFile path = do
    logImportantN [i|Parse zone file: #{path}|]
    zones <- parseSingleZoneFile path
    logImportantN [i|Parse zone file done: #{length zones}|]

------------------------------------------------------------------------------
--- ScanZoneDirectory
------------------------------------------------------------------------------

runScanZoneDirectory :: MonadUnliftIO m => FilePath -> ZoneManagerT m ()
runScanZoneDirectory path = do
    logImportantN [i|Scan zone directory: #{path}|]
    zones <- scanZoneDirectory path
    logImportantN [i|Scan zone directory done: #{length zones}|]
    storeParsedZones zones

------------------------------------------------------------------------------
--- LookupIpInDatabase
------------------------------------------------------------------------------

runLookupIpInDatabase :: MonadUnliftIO m => IPv4 -> ZoneManagerT m ()
runLookupIpInDatabase ip = do
    logInfoN [i|Looking up IP: #{ip}|]

    config <- askOverrideConfig

    logNormalN [i|Got override config:\n#{config}|]

    pureDb <- loadPureZoneDatabase

    let result = lookupIPFromDatabase pureDb ip

    logNormalN [i|Lookup IP done: #{prettyFormatLookupResult result}|]

------------------------------------------------------------------------------
--- Run Zone Database Command
------------------------------------------------------------------------------

runZoneDatabaseCommand
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadReader env m
       , HasLogOptions env
       , MonadAppConfig m
       )
    => ZoneDatabaseCommand
    -> m ()
runZoneDatabaseCommand command' = do
    logger <- liftIO $ newStdoutLoggerSet defaultBufSize
    options <- view logOptionsL
    prettyLogger <- liftIO $ makePrettyLogger def options logger

    config <- resolveZoneDatabaseConfig =<< getAppConfigFile "zone-config.yaml"

    flip runLoggingT (prettyMessageLogger Nothing prettyLogger) $ case command' of
            ParseZoneFile path -> withZoneManager config $ runParseZoneFile path

            ScanZoneDirectory dir -> withZoneManager config $ runScanZoneDirectory dir

            LookupIP ip -> withReadOnlyZoneManager config $ runLookupIpInDatabase ip

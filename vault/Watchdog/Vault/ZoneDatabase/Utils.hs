module Watchdog.Vault.ZoneDatabase.Utils
    ( parseIPv4
    , readIPv4
    , cidrToEndIP
    , isIPInCIDR
    , deduplicateCIDRs
    , detectOverlappingRanges
    , ansiColor
    ) where

import           Baulig.Prelude

import qualified Data.ByteString.Char8  as B8
import           Data.IP
import qualified Data.Set               as Set

import           Options.Applicative

import           System.Console.ANSI

import           Watchdog.Utils.IPUtils

------------------------------------------------------------------------------
--- Utils
------------------------------------------------------------------------------

parseIPv4 :: ReadM IPv4
parseIPv4 = eitherReader $ \s -> case readIPv4 s of
    Just ip -> Right ip
    Nothing -> Left "Invalid IPv4 address format."

deduplicateCIDRs :: [(AddrRange IPv4, Text)] -> [(AddrRange IPv4, Text)]
deduplicateCIDRs entries =
    let uniqueRanges = Set.toList . Set.fromList $ map fst entries
    in
        [(cidr, country)
        | (cidr, country) <- entries
        , cidr `elem` uniqueRanges
        ]

------------------------------------------------------------------------------
--- Detect overlapping ranges
------------------------------------------------------------------------------

-- | Detects overlapping CIDR ranges
detectOverlappingRanges :: MonadLogger m
                        => [(AddrRange IPv4, Text)]
                        -> m [(AddrRange IPv4, AddrRange IPv4)]
detectOverlappingRanges entries = do
    logWarnN [i|Checking for overlapping CIDR ranges in #{length entries} entries...|]

    -- Convert CIDR ranges to (startIP, endIP)
    let rangePairs = [(addrRangePair range, range) | (range, _) <- entries]
        correctedRanges = [((start, cidrToEndIP start mask'), range)
                          | ((start, mask'), range) <- rangePairs
                          ]

    -- Sort by start IP
    let sortedRanges = sortBy (comparing (fst . fst)) correctedRanges

    -- Find overlapping ranges
    let overlaps = findOverlaps sortedRanges []

    logWarnN [i|Found #{length overlaps} overlapping CIDR ranges.|]

    return overlaps

-- Helper function: Find overlaps in a sorted list
findOverlaps :: [((IPv4, IPv4), AddrRange IPv4)]
             -> [(AddrRange IPv4, AddrRange IPv4)]
             -> [(AddrRange IPv4, AddrRange IPv4)]
findOverlaps [] acc = acc
findOverlaps [ _ ] acc = acc
findOverlaps (((_, end1), range1) : ((start2, end2), range2) : rest) acc
    | start2 <= end1 =
        findOverlaps (((start2, end2), range2) : rest) ((range1, range2) : acc)
    | otherwise = findOverlaps (((start2, end2), range2) : rest) acc

------------------------------------------------------------------------------
--- Pretty Printing
------------------------------------------------------------------------------

ansiColor :: Color -> ByteString -> [ByteString]
ansiColor color bs = [ B8.pack $ setSGRCode [SetColor Foreground Dull color]
                     , bs
                     , B8.pack $ setSGRCode [Reset]
                     ]

module Watchdog.Vault.ZoneDatabase.Override
    ( OverrideConfig(..)
    , HasOverrideConfig(..)
    , RangeMap
    , lookupRangeMap
    ) where

import           Baulig.Prelude

import           Control.Monad.Fail

import           Data.Aeson
import           Data.Aeson.Types       (Parser)
import           Data.IP
import qualified Data.Map               as Map

import           Watchdog.Utils.IPUtils

------------------------------------------------------------------------------
--- OverrideConfig
------------------------------------------------------------------------------

type RangeMap = Map.Map (AddrRange IPv4) Text

-- | Custom IP override settings.
data OverrideConfig =
    OverrideConfig { internalNetworks :: !RangeMap             -- ^ Internal/private IP ranges
                   , trustedIps       :: !(Map.Map IPv4 Text)  -- ^ Specific trusted IPs
                   , regionalIsp      :: !RangeMap             -- ^ Home region ISP ranges
                   , customLabels     :: !RangeMap             -- ^ IP Range → Custom Label
                   }
    deriving (Eq, Show, Generic)

instance FromJSON OverrideConfig where
    parseJSON = withObject "OverrideConfig" $ \obj -> do
        internalNetworks <- parseRangeMap "internal-networks" obj
        trustedIps <- parseIPMapping "trusted-ips" obj
        regionalIsp <- parseRangeMap "regional-isp" obj
        customLabels <- parseRangeMap "custom-labels" obj
        return OverrideConfig { .. }

parseIPMapping :: Key -> Object -> Parser (Map.Map IPv4 Text)
parseIPMapping key obj = do
    rawMap <- obj .:? key .!= Map.empty
    pure $ Map.fromList $ mapMaybe parseEntry (Map.toList rawMap)
  where
    parseEntry (ipText, name) = case readIPv4 ipText of
        Just ip -> Just (ip, name)
        Nothing -> fail [i|Invalid IPv4 address in #{ipText}|]

parseRangeMap :: Key -> Object -> Parser RangeMap
parseRangeMap key obj = do
    rawMap <- obj .:? key .!= Map.empty
    return $ Map.fromList $ mapMaybe parseEntry (Map.toList rawMap)
  where
    parseEntry (cidrText, label) = case readIPv4Range cidrText of
        Just range -> Just (range, label)
        Nothing -> fail [i|Invalid CIDR notation in #{key}: #{cidrText}|]

-- | Look up an IP in the range map by checking each CIDR range
lookupRangeMap :: IPv4 -> RangeMap -> Maybe Text
lookupRangeMap ip customLabels = snd
    <$> find (\(cidr, _) -> isIPInCIDR ip cidr) (Map.toList customLabels)

------------------------------------------------------------------------------
--- HasOverrideConfig
------------------------------------------------------------------------------

class HasOverrideConfig x where
    overrideConfigL :: SimpleGetter x OverrideConfig

instance HasOverrideConfig OverrideConfig where
    overrideConfigL = id

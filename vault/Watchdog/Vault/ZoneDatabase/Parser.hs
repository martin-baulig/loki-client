module Watchdog.Vault.ZoneDatabase.Parser
    ( parseSingleZoneFile
    , scanZoneDirectory
    , storeParsedZones
    ) where

import           Baulig.Prelude

import           Data.IP
import qualified Data.Set                          as Set
import qualified Data.Text.IO                      as TIO

import           Database.Persist.Sqlite           hiding (LogFunc)

import           System.Directory
import           System.FilePath

import           Watchdog.Utils.IPUtils
import           Watchdog.Vault.Database.IPBlock
import           Watchdog.Vault.ZoneDatabase.Types
import           Watchdog.Vault.ZoneDatabase.Utils

------------------------------------------------------------------------------
--- Parse Zone File
------------------------------------------------------------------------------

parseSingleZoneFile
    :: MonadLoggerIO m => FilePath -> m [(AddrRange IPv4, Text)]
parseSingleZoneFile path = do
    logInfoN [i|Parsing zone file: #{path}|]

    -- Read file
    content <- liftIO $ TIO.readFile path
    let lines' = lines content
        parsed =
            mapMaybe (fmap (, countryCode) . readIPv4Range . unpack) lines'
        countryCode = takeWhile (/= '.') . pack $ takeFileName path
        uniqueParsed = removeDuplicates parsed

    -- Logging
    logInfoN [i|Total lines read: #{length lines'}|]
    logInfoN [i|Valid CIDR ranges: #{length parsed}|]
    logInfoN [i|Unique CIDR ranges after deduplication: #{length uniqueParsed}|]

    -- Warn if duplicates were found
    let duplicates = length parsed - length uniqueParsed
    if duplicates > 0
        then logWarnN [i|Removed #{duplicates} duplicate CIDR entries.|]
        else logInfoN "No duplicates found."

    return uniqueParsed

removeDuplicates :: (Ord a) => [a] -> [a]
removeDuplicates xs = Set.toList (Set.fromList xs)

scanZoneDirectory :: MonadLoggerIO m => FilePath -> m [(AddrRange IPv4, Text)]
scanZoneDirectory dir = do
    logInfoN [i|Scanning directory: #{dir}|]

    -- Get all files in the directory
    files <- liftIO $ listDirectory dir
    let isValidZoneFile f = takeExtension f == ".zone"
            && takeFileName f /= "zz.zone" && takeFileName f /= ".zone"
        zoneFiles = [dir </> f | f <- files, isValidZoneFile f]

    logInfoN [i|Found #{length zoneFiles} .zone files.|]

    -- Parse all files
    results <- mapM parseSingleZoneFile zoneFiles

    -- Combine all results
    let allEntries = concat results

    logInfoN [i|Parsed a total of #{length allEntries} CIDR ranges.|]

    overlaps <- detectOverlappingRanges allEntries
    forM_ overlaps $ logWarnN . formatOverlap

    return allEntries

formatOverlap :: (AddrRange IPv4, AddrRange IPv4) -> Text
formatOverlap (r1, r2) = [i|Overlap detected: #{r1} overlaps with #{r2}|]

------------------------------------------------------------------------------
--- Store Parsed Zones
------------------------------------------------------------------------------

-- | Store parsed CIDR ranges in SQLite
storeParsedZones :: MonadZoneDatabase m => [(AddrRange IPv4, Text)] -> m ()
storeParsedZones entries = do
    logInfoN [i|Inserting #{length entries} CIDR ranges into database.|]

    runZoneDatabase $ do
        -- Insert all entries in a single batch
        insertMany_ [IpBlock (ipv4ToWord32 start)
                             (ipv4ToWord32 $ cidrToEndIP start end)
                             country
                             False
                    | (range, country) <- entries
                    , let (start, end) = addrRangePair range
                    ]

    logInfoN "Finished inserting CIDR ranges."

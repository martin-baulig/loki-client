module Watchdog.Vault.ZoneDatabase.Pure
    ( PureZoneDatabase
    , loadPureZoneDatabase
    , lookupIPPure
    ) where

import           Baulig.Prelude

import           Data.IP
import qualified Data.Vector                          as V

import           Database.Persist.Sqlite

import           Watchdog.Vault.Database.IPBlock
import           Watchdog.Vault.ZoneDatabase.Override
import           Watchdog.Vault.ZoneDatabase.Types

------------------------------------------------------------------------------
--- Pure Database
------------------------------------------------------------------------------

data PureZoneDatabase = PureZoneDatabase { pzdOverride :: !OverrideConfig
                                         , pzdDatabase :: !(Vector IpBlock)
                                         }

instance HasOverrideConfig PureZoneDatabase where
    overrideConfigL = to pzdOverride

loadPureZoneDatabase :: MonadZoneDatabase m => m PureZoneDatabase
loadPureZoneDatabase = do
    override <- askOverrideConfig
    records <- runZoneDatabase $ selectList [] [Asc IpBlockStartIp]
    pure $ PureZoneDatabase override $ V.fromList $ map entityVal records

-- | Lookup an IP in the in-memory database using binary search
lookupIPPure :: PureZoneDatabase -> IPv4 -> Maybe IpBlock
lookupIPPure (pzdDatabase -> db) (ipv4ToWord32 -> ip) =
    binarySearch 0 (V.length db - 1)
  where
    binarySearch lo hi
        | lo > hi = Nothing
        | otherwise = let mid = (lo + hi) `div` 2
                          block = db V.! mid
                      in
                          if isIPInRange block
                          then Just block
                          else if ip < ipBlockStartIp block
                               then binarySearch lo (mid - 1)
                               else binarySearch (mid + 1) hi

    isIPInRange block = ipBlockStartIp block <= ip && ip <= ipBlockEndIp block

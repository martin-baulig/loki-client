module Watchdog.Vault.Metrics.Registry
    ( createMetricsRegistry
    , formatAllMetrics
    , module Watchdog.Vault.Metrics.Registry.Types
    ) where

import           Baulig.Prelude                           hiding (any, filter)

import qualified Data.ByteString.Builder                  as BB
import qualified Data.HashMap.Strict                      as HM
import           Data.Text.Encoding

import           Network.BSD

import           Watchdog.Vault.Metrics.Registry.Internal
import           Watchdog.Vault.Metrics.Registry.Types
import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- All Metrics
------------------------------------------------------------------------------

createMetricsRegistry
    :: MonadIO m => [MetricLabel] -> [AnyMetric r] -> m (MetricsRegistry r)
createMetricsRegistry labels allMetrics = do
    hostLabel <- MetricLabel "host" . pack <$> liftIO getHostName
    let allLabels = hostLabel : labels

    pure $ MetricsRegistry allLabels
        $ HM.fromListWith (++)
                          [(metricMetadata m, [AnyMetric m])
                          | AnyMetric m <- allMetrics
                          ]

------------------------------------------------------------------------------
--- Format Metrics
------------------------------------------------------------------------------

data AnyMetric' where
    AnyMetric' :: AnyMetric r -> AnyMetric'

type ExtraLabels = [MetricLabel]

type AllMetrics = [AnyMetric']

type JoinedMap = HashMap MetricMetadata [(ExtraLabels, AllMetrics)]

joinAnyRegistries :: [AnyMetricsRegistry] -> JoinedMap
joinAnyRegistries = foldl' mergeRegistry HM.empty
  where
    mergeRegistry :: JoinedMap -> AnyMetricsRegistry -> JoinedMap
    mergeRegistry acc
                  (AnyMetricsRegistry (MetricsRegistry extraLabels metrics)) =
        let wrapped = HM.map (map AnyMetric') metrics
            converted = HM.map (\ms -> [(extraLabels, ms)]) wrapped
        in
            HM.unionWith (++) acc converted

formatAllMetrics
    :: MonadUnliftIO m => MetricsRegistryCollection -> m BB.Builder
formatAllMetrics (MetricsRegistryCollection registryList) = do
    mconcat <$> mapM formatBuilder
                     (HM.toList $ joinAnyRegistries $ toList registryList)
  where
    formatBuilder (metadata, list) = do
        let metadataLines = encodeMetadata metadata
        values <- forM list encodeItem
        pure $ mconcat $ metadataLines : values

encodeItem :: MonadUnliftIO m => (ExtraLabels, AllMetrics) -> m BB.Builder
encodeItem (extraLabels, metrics) = do
    formatted <- mapM (`formatAnyValue` extraLabels) metrics
    let textLines = concat formatted  -- Flatten the list of lists
    pure $ mconcat (map encodeTextWithNewline textLines)
  where
    encodeTextWithNewline :: Text -> BB.Builder
    encodeTextWithNewline txt = encodeUtf8Builder txt <> BB.char7 '\n'

formatAnyValue :: MonadUnliftIO m => AnyMetric' -> [MetricLabel] -> m [Text]
formatAnyValue (AnyMetric' (AnyMetric any)) = getFormattedValue any

encodeMetadata :: MetricMetadata -> BB.Builder
encodeMetadata (MetricMetadata name type' help) =
    let helpLine = [i|\# HELP #{name} #{help}\n|]
        typeLine = [i|\# TYPE #{name} #{typeName type'}\n|]
    in
        encodeUtf8Builder $ helpLine <> typeLine
  where
    typeName = \case
        Counter -> "counter" :: Text
        Gauge -> "gauge"
        Summary -> "summary"

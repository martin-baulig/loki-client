module Watchdog.Vault.Metrics.Registry.Types
    ( MetricsRegistry
    , MetricsRegistryCollection
    , HasMetricsRegistry(..)
    , HasMetricsRegistryCollection(..)
    , metricsRegistryCollection
    , addMetricsRegistry
    ) where

import           Watchdog.Vault.Metrics.Registry.Internal

module Watchdog.Vault.Metrics.Registry.Internal
    ( MetricsRegistry(..)
    , HasMetricsRegistry(..)
    , AnyMetricsRegistry(..)
    , HasAnyMetricsRegistry(..)
    , MetricsRegistryCollection(..)
    , HasMetricsRegistryCollection(..)
    , metricsRegistryCollection
    , addMetricsRegistry
    ) where

import           Baulig.Prelude

import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- MetricsRegistry
------------------------------------------------------------------------------

data MetricsRegistry r =
    MetricsRegistry { mrExtraLabels :: ![MetricLabel]
                    , mrMetrics     :: !(HashMap MetricMetadata [AnyMetric r])
                    }

------------------------------------------------------------------------------
--- HasMetricsRegistry
------------------------------------------------------------------------------

class HasMetricsRegistry r x where
    metricsRegistryL :: SimpleGetter x (MetricsRegistry r)

instance HasMetricsRegistry r (MetricsRegistry r) where
    metricsRegistryL = id

------------------------------------------------------------------------------
--- MetricsRegistryCollection
------------------------------------------------------------------------------

newtype MetricsRegistryCollection =
    MetricsRegistryCollection (NonEmpty AnyMetricsRegistry)
    deriving newtype Semigroup

metricsRegistryCollection :: MetricsRegistry r -> MetricsRegistryCollection
metricsRegistryCollection =
    MetricsRegistryCollection . singleton . AnyMetricsRegistry

addMetricsRegistry :: MetricsRegistryCollection
                   -> MetricsRegistry r
                   -> MetricsRegistryCollection
addMetricsRegistry (MetricsRegistryCollection list) new =
    MetricsRegistryCollection $ AnyMetricsRegistry new `cons` list

------------------------------------------------------------------------------
--- HasMetricsRegistryCollection
------------------------------------------------------------------------------

class HasMetricsRegistryCollection x where
    metricsRegistryCollectionL :: SimpleGetter x MetricsRegistryCollection

instance HasMetricsRegistryCollection MetricsRegistryCollection where
    metricsRegistryCollectionL = id

instance HasMetricsRegistryCollection AnyMetricsRegistry where
    metricsRegistryCollectionL = to $ MetricsRegistryCollection . singleton

instance HasMetricsRegistryCollection (MetricsRegistry r) where
    metricsRegistryCollectionL = to metricsRegistryCollection

------------------------------------------------------------------------------
--- AnyMetricsRegistry
------------------------------------------------------------------------------

data AnyMetricsRegistry where
    AnyMetricsRegistry :: MetricsRegistry r -> AnyMetricsRegistry

class HasAnyMetricsRegistry x where
    anyMetricsRegistryL :: SimpleGetter x AnyMetricsRegistry

instance HasAnyMetricsRegistry AnyMetricsRegistry where
    anyMetricsRegistryL = id

instance HasAnyMetricsRegistry (MetricsRegistry r) where
    anyMetricsRegistryL = to AnyMetricsRegistry

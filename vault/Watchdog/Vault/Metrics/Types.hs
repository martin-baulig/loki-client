module Watchdog.Vault.Metrics.Types
    ( FormatMetricValue(..)
    , MetricLabel(..)
    , MetricType(..)
    , Metric(..)
    , MetricMetadata(..)
    , DatabaseCounter(..)
    , DatabaseGauge(..)
    , DatabaseMultiGauge(..)
    , SimpleGauge(..)
    , AnyMetric(..)
    , setSimpleGauge
    ) where

import           Baulig.Prelude

import           Data.Tagged

import           Database.Persist
import           Database.Persist.Sqlite

import           Watchdog.Vault.Database.Types

------------------------------------------------------------------------------
--- FormatMetricValue
------------------------------------------------------------------------------

class FormatMetricValue x where
    formatMetricValue :: x -> Text

instance FormatMetricValue Int where
    formatMetricValue = tshow

instance FormatMetricValue Float where
    formatMetricValue = tshow

------------------------------------------------------------------------------
--- MetricLabel
------------------------------------------------------------------------------

data MetricLabel = MetricLabel !Text !Text
    deriving stock (Eq, Generic, Show)
    deriving anyclass Hashable

formatLabels :: [MetricLabel] -> Text
formatLabels labels
    | null labels = ""
formatLabels labels = "{" <> inner <> "}"
  where
    inner = intercalate "," $ map mapLabel labels

    mapLabel (MetricLabel key value) = key <> "=" <> tshow value

------------------------------------------------------------------------------
--- MetricMetadata
------------------------------------------------------------------------------

data MetricMetadata = MetricMetadata { metricName :: !Text
                                     , metricType :: !MetricType
                                     , metricHelp :: !Text
                                     }
    deriving stock (Eq, Generic, Show)
    deriving anyclass Hashable

------------------------------------------------------------------------------
--- Database Metrics
------------------------------------------------------------------------------

data MetricType = Counter | Gauge | Summary
    deriving stock (Eq, Generic, Show)
    deriving anyclass Hashable

class Metric r x | x -> r where
    type MetricValue x

    metricMetadata :: x -> MetricMetadata

    metricLabels :: x -> [MetricLabel]

    formatMetric :: x -> [MetricLabel] -> MetricValue x -> [Text]

    default formatMetric :: FormatMetricValue (MetricValue x)
                         => x
                         -> [MetricLabel]
                         -> MetricValue x
                         -> [Text]
    formatMetric = defaultFormatMetric

    getMetricValue :: MonadUnliftIO m => x -> m (MetricValue x)

    getFormattedValue :: MonadUnliftIO m => x -> [MetricLabel] -> m [Text]
    getFormattedValue metric extraLabels =
        getMetricValue metric <&> formatMetric metric extraLabels

defaultFormatMetric :: (Metric r x, FormatMetricValue (MetricValue x))
                    => x
                    -> [MetricLabel]
                    -> MetricValue x
                    -> [Text]
defaultFormatMetric metric extraLabels value =
    [formatValue extraLabels metric value]

formatValue
    :: (Metric r x, FormatMetricValue y) => [MetricLabel] -> x -> y -> Text
formatValue labels metric value = metricName metadata
    <> formatLabels (labels <> metricLabels metric) <> " "
    <> formatMetricValue value
  where
    metadata = metricMetadata metric

------------------------------------------------------------------------------
--- Database Counter
------------------------------------------------------------------------------

data DatabaseCounter r d = forall x. PersistRecordBackend x SqlBackend
    => DatabaseCounter !(Tagged d Database) !Text !Text ![Filter x]

instance Metric r (DatabaseCounter r d) where
    type MetricValue (DatabaseCounter r d) = Int

    metricMetadata (DatabaseCounter _ name help _) =
        MetricMetadata name Counter help

    metricLabels _ = mempty

    getMetricValue (DatabaseCounter db _ _ filter') =
        runDatabaseOp db $ count filter'

------------------------------------------------------------------------------
--- Database Gauge
------------------------------------------------------------------------------

data DatabaseGauge r d = forall x. PersistRecordBackend x SqlBackend
    => DatabaseGauge !(Tagged d Database)
                     !Text
                     !Text
                     ![Filter x]
                     ![MetricLabel]

instance Metric r (DatabaseGauge r d) where
    type MetricValue (DatabaseGauge r d) = Int

    metricMetadata (DatabaseGauge _ name help _ _) =
        MetricMetadata name Gauge help

    metricLabels (DatabaseGauge _ _ _ _ labels) = labels

    getMetricValue (DatabaseGauge db _ _ filter' _) =
        runDatabaseOp db $ count filter'

------------------------------------------------------------------------------
--- Database MultiGauge
------------------------------------------------------------------------------

data DatabaseMultiGauge r d x =
    DatabaseMultiGauge { dmgDatabase   :: !(Tagged d Database)
                       , dmgName       :: !Text
                       , dmgHelp       :: !Text
                       , dmgBaseLabels :: ![MetricLabel]
                       , dmgFilters    :: !(NonEmpty ( NonEmpty MetricLabel
                                                     , NonEmpty (Filter x)
                                                     ))
                       }

instance PersistRecordBackend x SqlBackend
    => Metric r (DatabaseMultiGauge r d x) where
    type MetricValue (DatabaseMultiGauge r d x) =
        NonEmpty (NonEmpty MetricLabel, Int)

    metricMetadata metric =
        MetricMetadata (dmgName metric) Gauge (dmgHelp metric)

    metricLabels = dmgBaseLabels

    formatMetric metric extraLabels values = toList (map format values)
      where
        format (label, value) =
            formatValue (extraLabels <> toList label) metric value

    getMetricValue gauge = runDatabaseOp (dmgDatabase gauge)
        $ forM (dmgFilters gauge) $ \(labels, filters) -> (labels, )
        <$> count (toList filters)

------------------------------------------------------------------------------
--- Simple Gauge
------------------------------------------------------------------------------

data SimpleGauge r x = SimpleGauge !Text !Text ![MetricLabel] !(TVar x)

instance FormatMetricValue x => Metric r (SimpleGauge r x) where
    type MetricValue (SimpleGauge r x) = x

    metricMetadata (SimpleGauge name help _ _) = MetricMetadata name Gauge help

    metricLabels (SimpleGauge _ _ labels _) = labels

    getMetricValue (SimpleGauge _ _ _ tvar) = readTVarIO tvar

setSimpleGauge :: MonadIO m => SimpleGauge r x -> x -> m ()
setSimpleGauge (SimpleGauge _ _ _ var) = atomically . writeTVar var

------------------------------------------------------------------------------
--- AnyMetric
------------------------------------------------------------------------------

data AnyMetric r = forall x. Metric r x => AnyMetric x

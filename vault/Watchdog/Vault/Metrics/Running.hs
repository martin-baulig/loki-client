{-# LANGUAGE UndecidableInstances #-}

module Watchdog.Vault.Metrics.Running
    ( RunningValue(..)
    , RunningGauge
    , HasRunningGauge(..)
    , runningGauge
    ) where

import           Baulig.Prelude

import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- RunningGauge
------------------------------------------------------------------------------

data RunningValue = Offline | Starting | Busy | Problems | Error | Up
    deriving stock (Enum, Eq, Generic, Ord, Show)

instance FormatMetricValue RunningValue where
    formatMetricValue = \case
        Offline -> "0.0"
        Starting -> "0.2"
        Busy -> "0.4"
        Problems -> "0.6"
        Error -> "0.8"
        Up -> "1.0"

newtype RunningGauge r = RunningGauge (SimpleGauge r RunningValue)
    deriving newtype (Metric r)

runningGauge :: MonadIO m => Text -> [MetricLabel] -> m (RunningGauge r)
runningGauge name labels = RunningGauge
    . SimpleGauge (name <> "_running") "Current running status." labels
    <$> newTVarIO Busy

------------------------------------------------------------------------------
--- HasRunningGauge
------------------------------------------------------------------------------

class HasRunningGauge r where
    setRunningGauge :: MonadIO m => r -> RunningValue -> m ()

    withRunningGauge :: MonadUnliftIO m => r -> m x -> m x
    withRunningGauge this = bracket_ (setRunning Up) (setRunning Offline)
      where
        setRunning = setRunningGauge this

instance HasRunningGauge (RunningGauge r) where
    setRunningGauge (RunningGauge running) = setSimpleGauge running

module Watchdog.LokiClient.LokiLogger
    ( LokiLoggerConfig
    , HasLokiLoggerConfig(..)
    , LokiLogger
    , withLokiLogger
    , lokiLoggerSendLogMessage
      -- re-exports
    , HasStreamContext(..)
    , StreamContext
    , logWaiRequestToLoki
    , formatLogMessage
    ) where

import           Baulig.Prelude

import qualified Data.Yaml                                  as Y

import           Network.Wai

import           Watchdog.LokiClient.LokiLogger.Format
import           Watchdog.LokiClient.LokiLogger.Types
import           Watchdog.LokiClient.Server.Sender.Internal
import           Watchdog.LokiClient.Server.StreamContext
import           Watchdog.Vault.ZoneDatabase

------------------------------------------------------------------------------
--- LokiLoggerConfig
------------------------------------------------------------------------------

loadZoneConfig :: MonadIO m => LokiLoggerConfig -> m (Maybe ZoneDatabaseConfig)
loadZoneConfig config
    | Just path <- llcZoneConfigFile config = Just <$> Y.decodeFileThrow path
    | otherwise = pure Nothing

logWaiRequestToLoki :: MonadIO m => LokiLogger -> Request -> Response -> m ()
logWaiRequestToLoki logger req res = do
    value <- formatLokiLogMessage logger req res
    lokiLoggerSendStreamValue logger req value

------------------------------------------------------------------------------
--- LokiClient Logger
------------------------------------------------------------------------------

withLokiLogger :: (MonadUnliftIO m, MonadThrow m, MonadLoggerIO m)
               => LokiLoggerConfig
               -> StreamContext
               -> LogOptions
               -> (LokiLogger -> m x)
               -> m x
withLokiLogger config context options func = do
    zoneDatabase <- mapM loadZoneDatabaseFromConfig =<< loadZoneConfig config

    withSender config context options (llcDatabase config) (llcLabels config)
        $ inner zoneDatabase
  where
    inner zoneDatabase = do
        case zoneDatabase of
            Just db -> $logImportant [i|LokiLogger: #{db ^. overrideConfigL}|]
            Nothing -> pure ()
        logger <- asks $ LokiLogger config context options zoneDatabase
        lift $ func logger


module Watchdog.LokiClient.Connection
    ( LokiConnection
    , HasLokiConnection(..)
    , mkLokiConnection
    ) where

import           Baulig.Prelude

import           Network.HTTP.Client

import           Watchdog.Settings.LokiConfig
import           Watchdog.Settings.TlsSettings

------------------------------------------------------------------------------
--- LokiConnection
------------------------------------------------------------------------------

data LokiConnection =
    LokiConnection { lcConfig :: !LokiConfig, lcManager :: !Manager }

instance HasLokiConfig LokiConnection where
    lokiConfigL = lens lcConfig (\x y -> x { lcConfig = y })

mkLokiConnection :: MonadIO m => LokiConfig -> m LokiConnection
mkLokiConnection config = LokiConnection config
    <$> newTlsManager (config ^. lokiConfigTls)

------------------------------------------------------------------------------
--- HasLokiConnection
------------------------------------------------------------------------------

class HasLokiConfig env => HasLokiConnection env where
    lokiConnectionL :: SimpleGetter env LokiConnection

    lokiConnectionManager :: SimpleGetter env Manager
    lokiConnectionManager = lokiConnectionL . to lcManager

instance HasLokiConnection LokiConnection where
    lokiConnectionL = id

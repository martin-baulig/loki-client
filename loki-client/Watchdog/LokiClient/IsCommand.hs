module Watchdog.LokiClient.IsCommand (IsCommand(..)) where

import           Baulig.Prelude

import qualified Data.Text                                as T

import           Network.HTTP.Client                      (Request)

import           Baulig.Utils.PrettyPrint

import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.Connection
import           Watchdog.LokiClient.Server.StreamContext

------------------------------------------------------------------------------
--- IsCommand
------------------------------------------------------------------------------

class (Show x, Show (CommandResponse x), FromJSON (CommandResponse x))
    => IsCommand x where
    data CommandResponse x

    createRequest :: ( MonadUnliftIO m
                     , MonadThrow m
                     , MonadLogger m
                     , MonadReader env m
                     , HasStreamContext env
                     )
                  => LokiConnection
                  -> x
                  -> m Request

    performRequest :: (MonadUnliftIO m, MonadLogger m)
                   => Proxy x
                   -> Request
                   -> m (CommandResponse x)
    performRequest _ = performApiRequest Proxy

    formatResponse :: CommandResponse x -> Text
    formatResponse = tshow

    printResponse :: ( MonadIO m
                     , MonadLogger m
                     , MonadReader env m
                     , HasLogOptions env
                     , HasColorConfig env
                     )
                  => CommandResponse x
                  -> m ()
    printResponse response = logInfoN
        $ "Command response:\n" <> sep <> "\n" <> formatted <> "\n" <> sep
      where
        sep = replicate 40 '='

        formatted = T.strip $ formatResponse response

    runCommand :: ( MonadUnliftIO m
                  , MonadThrow m
                  , MonadLogger m
                  , MonadReader env m
                  , HasLogOptions env
                  , HasColorConfig env
                  , HasStreamContext env
                  )
               => LokiConnection
               -> x
               -> m ()
    runCommand connection command = createRequest connection command
        >>= performRequest proxy >>= printResponse
      where
        proxy :: Proxy x
        proxy = Proxy

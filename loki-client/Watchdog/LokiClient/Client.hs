module Watchdog.LokiClient.Client
    ( createApiRequest
    , withWebSocketRequest
    , module Watchdog.LokiClient.Client.Exception
    , module Watchdog.LokiClient.Client.Request
    ) where

import           Baulig.Prelude

import           Network.HTTP.Simple                  hiding (Proxy)
import qualified Network.URI                          as URI
import           Network.URI.Lens

import           Watchdog.LokiClient.Client.Exception
import           Watchdog.LokiClient.Client.Request
import           Watchdog.LokiClient.Client.Types
import           Watchdog.LokiClient.Connection
import           Watchdog.Settings.LokiConfig

------------------------------------------------------------------------------
--- Client
------------------------------------------------------------------------------

createApiRequest :: MonadThrow m => LokiConnection -> ApiPath -> m Request
createApiRequest connection (ApiPath path) =
    setRequestManager manager <$> parseRequest textUri
  where
    uri = connection ^. lokiConfigUri & uriPathLens .~ unpack path

    textUri = URI.uriToString id uri ""

    manager = connection ^. lokiConnectionManager

withWebSocketRequest
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, FromJSON x)
    => LokiConnection
    -> Request
    -> ConduitM x Void m ()
    -> m ()
withWebSocketRequest = withWebSocketRequestManager . view lokiConnectionManager


module Watchdog.LokiClient.Command.Push (Push(..)) where

import           Baulig.Prelude

import qualified Data.Text.IO                             as TIO

import           Network.HTTP.Simple
import           Network.HTTP.Types.Header

import           System.Directory

import           Watchdog.Api
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand
import           Watchdog.LokiClient.Server.StreamContext
import qualified Watchdog.LokiClient.Utils.LoremIpsum     as Lorem

------------------------------------------------------------------------------
--- Push
------------------------------------------------------------------------------

data Push = PushText !Text
          | PushLoremIpsum !Int
          | PushFile !FilePath
          | PushStream !Stream
          | PushRaw !ByteString
    deriving stock (Show, Generic)

createPushRequestBody
    :: ( MonadUnliftIO m
       , MonadThrow m
       , MonadLogger m
       , MonadReader env m
       , HasStreamContext env
       )
    => Push
    -> m LazyByteString
createPushRequestBody push
    | PushText text <- push = do
        body <- encodeStreams <$> streamFromText [text]
        logDebugN $ "GOT BODY: " <> decodeUtf8 (toStrict body)
        pure body

    | PushFile path <- push = do
        contents <- liftIO $ TIO.readFile path
        size <- liftIO $ getFileSize path
        let labels = fromListOfKeyValue [ ("fileName", pack path)
                                        , ("fileSize", tshow size)
                                        ]
        stream <- streamValueWithMetadata contents labels
        body <- encodeStreams <$> streamFromValue stream
        logDebugN $ "GOT BODY: " <> decodeUtf8 (toStrict body)
        pure body

    | PushLoremIpsum count <- push = do
        lorem <- Lorem.generateParagraphs count
        encodeStreams <$> streamFromText lorem

    | PushStream stream <- push = do
        let body = encodeStreams stream
        logDebugN $ "GOT BODY: " <> decodeUtf8 (toStrict body)
        pure body

    | PushRaw body <- push = do
        logDebugN $ "GOT BODY: " <> decodeUtf8 body
        pure $ fromStrict body

instance IsCommand Push where
    data CommandResponse Push = PushResponse ()
            deriving stock (Show, Generic)
            deriving (FromJSON)

    createRequest connection push = do
        request <- setRequestMethod "POST"
            . addRequestHeader hContentType "application/json"
            <$> createApiRequest connection "/loki/api/v1/push"
        flip setRequestBodyLBS request <$> createPushRequestBody push

    performRequest _ request = performRequestWithNoResult request
        >> pure (PushResponse ())

    printResponse _ = pure ()

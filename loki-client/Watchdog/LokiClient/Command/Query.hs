module Watchdog.LokiClient.Command.Query (Query(..)) where

import           Baulig.Prelude

import           Network.HTTP.Client

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand

------------------------------------------------------------------------------
--- Query
------------------------------------------------------------------------------

newtype Query = Query Text
    deriving stock (Generic, Show)

instance IsCommand Query where
    data CommandResponse Query = QueryResponse !QueryResult
            deriving stock (Generic, Show)
            deriving (FromJSON)

    createRequest connection (Query query) =
        setQueryString [("query", Just $ encodeUtf8 query)]
        <$> createApiRequest connection "/loki/api/v1/query"

    printResponse (QueryResponse result) = prettyPrint result

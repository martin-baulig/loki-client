module Watchdog.LokiClient.Command.QueryRange (QueryRange(..)) where

import           Baulig.Prelude

import           Network.HTTP.Client

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand

------------------------------------------------------------------------------
--- Query
------------------------------------------------------------------------------

newtype QueryRange = QueryRange Text
    deriving stock (Generic, Show)

instance IsCommand QueryRange where
    data CommandResponse QueryRange = QueryResponse !QueryResult
            deriving stock (Generic, Show)
            deriving (FromJSON)

    createRequest connection (QueryRange query) =
        setQueryString [("query", Just $ encodeUtf8 query)]
        <$> createApiRequest connection "/loki/api/v1/query_range"

    printResponse (QueryResponse result) = prettyPrint result

module Watchdog.LokiClient.Command.GetLabels (GetLabels(..)) where

import           Baulig.Prelude

import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand

------------------------------------------------------------------------------
--- GetLabels
------------------------------------------------------------------------------

data GetLabels = GetLabels
    deriving stock (Show, Generic)

instance IsCommand GetLabels where
    data CommandResponse GetLabels = GetLabelsResponse [Text]
            deriving stock (Show, Generic)
            deriving (FromJSON)

    createRequest connection _ =
        createApiRequest connection "/loki/api/v1/labels"

    formatResponse (GetLabelsResponse labels) = unlines labels

module Watchdog.LokiClient.Command.Tail (Tail(..)) where

import           Baulig.Prelude

import           Network.HTTP.Client

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand

------------------------------------------------------------------------------
--- Tail
------------------------------------------------------------------------------

newtype Tail = Tail Text
    deriving stock (Generic, Show)

webSocketSink :: ( MonadIO m
                 , MonadLogger m
                 , MonadReader env m
                 , HasLogOptions env
                 , HasColorConfig env
                 )
              => ConduitM Streams Void m ()
webSocketSink = loop
  where
    loop = do
        logDebugN "Tail sink."
        message <- await
        case message of
            Just streams -> do
                logDebugN $ "Got message: " <> tshow streams
                lift $ prettyPrint streams
                loop
            Nothing -> logInfoN "Tail sink done."

instance IsCommand Tail where
    data CommandResponse Tail = TailResponse !QueryResult
            deriving stock (Generic, Show)
            deriving (FromJSON)

    createRequest connection (Tail command) =
        setQueryString [("query", Just $ encodeUtf8 command)]
        <$> createApiRequest connection "/loki/api/v1/tail"

    performRequest _ _ = error "Should not be called directly."

    runCommand connection query = do
        request <- createRequest connection query
        withWebSocketRequest connection request webSocketSink

    printResponse (TailResponse result) = prettyPrint result

module Watchdog.LokiClient.Command.GetLabelValues (GetLabelValues(..)) where

import           Baulig.Prelude

import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.IsCommand

------------------------------------------------------------------------------
--- GetLabels
------------------------------------------------------------------------------

newtype GetLabelValues = GetLabelValues Text
    deriving stock (Show, Generic)

instance IsCommand GetLabelValues where
    data CommandResponse GetLabelValues = GetLabelValuesResponse [Text]
            deriving stock (Show, Generic)
            deriving (FromJSON)

    createRequest connection (GetLabelValues label) =
        createApiRequest connection $ fromString
        $ "/loki/api/v1/label/" <> unpack label <> "/values"

    formatResponse (GetLabelValuesResponse values) = unlines values

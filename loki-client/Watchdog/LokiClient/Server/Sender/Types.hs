module Watchdog.LokiClient.Server.Sender.Types
    ( Sender(..)
    , HasSender(..)
    , SenderT(..)
    , SenderConstraints
    , runSenderT
    , askSender
    ) where

import           Baulig.Prelude

import           Baulig.Utils.PrettyPrint

import           Watchdog.LokiClient.Connection
import           Watchdog.LokiClient.Server.Logging
import           Watchdog.LokiClient.Server.Sender.Channel
import           Watchdog.LokiClient.Server.StreamContext
import           Watchdog.Settings.LokiConfig
import           Watchdog.Vault.Database.Sender
import           Watchdog.Vault.Metrics.Registry
import           Watchdog.Vault.Metrics.Running

------------------------------------------------------------------------------
--- Sender
------------------------------------------------------------------------------

data Sender =
    Sender { senderDatabase      :: !SenderDatabase
           , senderContext       :: !StreamContext
           , senderChannel       :: !SenderChannel
           , senderLogFilterFunc :: !(LogSource -> LogLevel -> Bool)
           , senderLogSource     :: !(Maybe LogSource)
           , senderLogFunc       :: !SenderLogFunc
           , senderLokiConfig    :: !LokiConfig
           , senderLogOptions    :: !LogOptions
           , senderMetrics       :: !SenderRegistry
           , senderColorConfig   :: !ColorConfig
           , senderConnection    :: !LokiConnection
           }

class HasSender x where
    senderL :: Lens' x Sender

instance HasSender Sender where
    senderL = id

instance HasRunningGauge Sender where
    setRunningGauge = setRunningGauge . senderMetrics

instance HasMetricsRegistryCollection Sender where
    metricsRegistryCollectionL = to senderMetrics . metricsRegistryCollectionL

------------------------------------------------------------------------------
--- SenderT
------------------------------------------------------------------------------

newtype SenderT m x = SenderT { unSenderT :: ReaderT Sender m x }
    deriving newtype (Applicative, Functor, Monad, MonadIO, MonadUnliftIO
                    , MonadThrow, MonadReader Sender, MonadTrans)

instance MonadUnliftIO m => MonadSenderDatabase (SenderT m) where
    runSenderDatabase action = SenderT $ do
        sender <- ask
        runDatabaseOp (senderDatabase sender) action

instance HasLogOptions Sender where
    logOptionsL = lens senderLogOptions (\x y -> x { senderLogOptions = y })

instance HasLokiConfig Sender where
    lokiConfigL = lens senderLokiConfig (\x y -> x { senderLokiConfig = y })

instance HasLokiConnection Sender where
    lokiConnectionL = to senderConnection

instance HasColorConfig Sender where
    colorConfigL = lens senderColorConfig (\x y -> x { senderColorConfig = y })

instance HasStreamContext Sender where
    streamContextL = lens senderContext (\x y -> x { senderContext = y })

instance MonadIO m => MonadLogger (SenderT m) where
    monadLoggerLog loc source level msg = inner =<< ask
      where
        inner sender = liftIO
            $ when (senderLogFilterFunc sender effectiveSource level)
            $ senderLogFuncLog func loc effectiveSource level msg
          where
            effectiveSource = case senderLogSource sender of
                Nothing -> source
                Just source' -> if null source then source' else source

            func = senderLogFunc sender

runSenderT :: Sender -> SenderT m x -> m x
runSenderT sender (SenderT func) = runReaderT func sender

askSender :: Monad m => SenderT m Sender
askSender = SenderT ask

------------------------------------------------------------------------------
--- SenderConstraints
------------------------------------------------------------------------------

type SenderConstraints x = (HasLokiConfig x, HasColorConfig x, HasCallStack)

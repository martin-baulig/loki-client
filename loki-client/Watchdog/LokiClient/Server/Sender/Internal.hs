module Watchdog.LokiClient.Server.Sender.Internal
    ( withSender
    , sendMessage
    , sendMessageWithStreamContext
    ) where

import           Baulig.Prelude

import qualified Control.Exception                         as E

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.Client
import           Watchdog.LokiClient.Command.Push
import           Watchdog.LokiClient.Connection
import           Watchdog.LokiClient.IsCommand
                 (IsCommand(createRequest))
import           Watchdog.LokiClient.Server.Logging
import           Watchdog.LokiClient.Server.Sender.Channel
import           Watchdog.LokiClient.Server.Sender.Types
import           Watchdog.LokiClient.Server.StreamContext
import           Watchdog.Settings.LokiConfig
import           Watchdog.Vault.Database.Sender
import           Watchdog.Vault.Metrics.Running
import           Watchdog.Vault.Metrics.Types

------------------------------------------------------------------------------
--- Sender Main Loop
------------------------------------------------------------------------------

filterMessageC :: (MonadIO m, MonadReader env m, HasStreamContext env)
               => ConduitT UploadItem Stream m ()
filterMessageC = awaitForever inner
  where
    inner (UploadValue value) = inner $ UploadMultiple [value]
    inner (UploadMultiple values) = yield =<< lift (streamFromValues values)
    inner (UploadStream stream) = yield stream

liftSenderInternalT :: MonadIO m => SenderT m x -> SenderT m x
liftSenderInternalT = local (\s -> s { senderLogFilterFunc = filter' s })
  where
    filter' sender _ level
        | senderLokiConfig sender ^. lokiConfigDebug = True
        | otherwise = level > LevelDebug

uploadStreamC :: (MonadUnliftIO m, MonadThrow m)
              => ConduitT Stream Void (SenderT m) ()
uploadStreamC = awaitForever $ lift . inner
  where
    inner stream = do
        liftSenderInternalT $ logDebugN $ "Sender main loop: " <> tshow stream
        sender <- ask
        when (sender ^. lokiConfigPrint) $ printLogLine stream
        hFlush stdout
        hFlush stderr
        registry <- asks senderMetrics
        cnc <- asks senderConnection
        key <- insertPendingStream (senderDatabase sender) stream
        request <- liftSenderInternalT $ createRequest cnc $ PushStream stream
        tryAny (liftSenderInternalT $ performRequestWithNoResult request)
            >>= \case
                Left exc -> do
                    $logError $ "Upload failed: " <> tshow exc
                    setRunningGauge registry Problems
                    updateStatus key Failed
                Right () -> do
                    liftSenderInternalT $ logDebugN "Upload done."
                    setRunningGauge registry Up
                    updateStatus key Success

senderMainC :: (MonadUnliftIO m, MonadThrow m) => SenderT m ()
senderMainC = do
    channel <- senderChannel <$> ask
    runConduit $ readChannelC channel .| filterChannelC defaultFilterParams
        .| filterMessageC .| uploadStreamC
    logWarnN "Server main loop done."

closeSender :: MonadIO m => Sender -> m ()
closeSender = closeChannel . senderChannel

sendMessage :: MonadIO m => Sender -> StreamValue -> m ()
sendMessage sender value = sendStream (senderChannel sender)
    $ streamFromValue' (senderContext sender) value

sendMessageWithStreamContext
    :: MonadIO m => Sender -> StreamContext -> StreamValue -> m ()
sendMessageWithStreamContext sender context value =
    sendStream (senderChannel sender) $ streamFromValue' context value

sendMessage'
    :: MonadIO m => StreamContext -> SenderChannel -> StreamValue -> m ()
sendMessage' context channel value =
    sendStream channel $ streamFromValue' context value

exceptionHandler :: (MonadIO m) => SomeException -> SenderT m ()
exceptionHandler exc = case fromException exc of
    Just (SomeAsyncException aexc) -> case cast aexc of
        Just E.UserInterrupt -> logInfoN "User interrupt, exiting normally."
        _other -> $logError $ "Caught asynchronous exception: " <> printExc
    Nothing -> $logError $ "Caught sync exception: " <> printExc
  where
    printExc = pack $ displayException exc

withSender
    :: (MonadUnliftIO m, MonadThrow m, MonadLoggerIO m, SenderConstraints env)
    => env
    -> StreamContext
    -> LogOptions
    -> SenderDatabasePath
    -> [MetricLabel]
    -> SenderT m x
    -> m x
withSender env context logOptions path extraLabels func =
    flip runReaderT env $ withSenderDatabase path $ \db -> do
        channel <- newChannel
        outerLogger <- askSenderLogFunc

        lokiConfig <- view lokiConfigL

        sender <- Sender db
                         context
                         channel
                         (logLevelFilter $ logOptions ^. logOptionsFilter)
                         Nothing
                         (makeContextLogFunc context
                          $ sendMessage' context channel)
                         lokiConfig
                         logOptions <$> createSenderRegistry db extraLabels
            <*> view colorConfigL <*> mkLokiConnection lokiConfig

        let innerSender = sender { senderLogFunc   = outerLogger
                                 , senderLogSource = Just "sender"
                                 }

        lift $ withAsync (runSenderT innerSender senderMainC) $ \main ->
            runSenderT sender $ withRunningGauge (senderMetrics sender) $ do
                trySyncOrAsync func >>= \case
                    Left exc -> do
                        exceptionHandler exc
                        throwIO exc

                    Right res -> do
                        logInfoN "Exited normally."
                        closeSender sender
                        wait main
                        pure res

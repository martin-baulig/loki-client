module Watchdog.LokiClient.Server.Sender.Process
    ( runWithProcess
    , senderSink
    ) where

import           Baulig.Prelude

import           UnliftIO.Process

import           Watchdog.Api
import           Watchdog.LokiClient.Server.Sender.Internal
import           Watchdog.LokiClient.Server.Sender.Types

------------------------------------------------------------------------------
--- Sender Process
------------------------------------------------------------------------------

toStreamValue :: MonadIO m => Text -> Bool -> ByteString -> m StreamValue
toStreamValue process isStderr message =
    timestampedValueWithMetadata (decodeUtf8 message)
                                 [ ("stream", bool "stdout" "stderr" isStderr)
                                 , ("process", process)
                                 ]

senderSink :: MonadUnliftIO m => Sender -> ConduitT StreamValue o m ()
senderSink sender = sink
  where
    sink = await >>= messageSink

    messageSink Nothing = putStrLn "CONDUIT DONE"

    messageSink (Just message) = do
        putStrLn $ "CONDUIT MESSAGE: " <> tshow message
        sendMessage sender message
        sink

runWithProcess :: MonadUnliftIO m => CreateProcess -> Sender -> m ()
runWithProcess create sender = withCreateProcess process processHandler
  where
    processHandler _ (Just out) (Just err) _proc =
        concurrently_ (createConduit out False) (createConduit err True)

    processHandler _ _ _ _ = error "Failed to redirect output"

    createConduit _handle isStderr = runConduit $ sourceHandle _handle
        .| mapMC (toStreamValue prettyProcess isStderr) .| senderSink sender

    prettyProcess = case cmdspec process of
        ShellCommand cmd -> "shell:" <> pack cmd
        RawCommand file args -> pack file <> " " <> pack (unwords args)

    process = create { std_in  = NoStream
                     , std_out = CreatePipe
                     , std_err = CreatePipe
                     }


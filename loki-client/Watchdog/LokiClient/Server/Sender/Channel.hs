module Watchdog.LokiClient.Server.Sender.Channel
    ( SenderChannel
    , FilterParams
    , UploadItem(..)
    , newChannel
    , closeChannel
    , sendNormalMessage
    , sendOutOfBandMessage
    , sendStream
    , readChannelC
    , defaultFilterParams
    , filterChannelC
    ) where

import           Baulig.Prelude

import           Data.Time

import           UnliftIO.Concurrent (threadDelay)

import           Watchdog.Api

------------------------------------------------------------------------------
--- ChannelItem
------------------------------------------------------------------------------

data ChannelItem =
    SingleItem !StreamValue | OutOfBandItem !StreamValue | StreamItem !Stream

------------------------------------------------------------------------------
--- ChannelMessage
------------------------------------------------------------------------------

data ChannelMessage = NoMessage | Finished | MessageItem !ChannelItem

------------------------------------------------------------------------------
--- SenderChannel
------------------------------------------------------------------------------

newtype SenderChannel = SenderChannel (TChan (Maybe ChannelItem))

------------------------------------------------------------------------------
--- Channel API
------------------------------------------------------------------------------

newChannel :: MonadIO m => m SenderChannel
newChannel = SenderChannel <$> newTChanIO

closeChannel :: MonadIO m => SenderChannel -> m ()
closeChannel (SenderChannel chan) = atomically $ writeTChan chan Nothing

sendNormalMessage :: MonadIO m => SenderChannel -> StreamValue -> m ()
sendNormalMessage (SenderChannel chan) =
    atomically . writeTChan chan . Just . SingleItem

sendOutOfBandMessage :: MonadIO m => SenderChannel -> StreamValue -> m ()
sendOutOfBandMessage (SenderChannel chan) =
    atomically . writeTChan chan . Just . OutOfBandItem

sendStream :: MonadIO m => SenderChannel -> Stream -> m ()
sendStream (SenderChannel chan) =
    atomically . writeTChan chan . Just . StreamItem

readChannelC :: MonadIO m => SenderChannel -> ConduitT () ChannelMessage m ()
readChannelC (SenderChannel channel) = loop
  where
    loop = atomically (tryReadTChan channel) >>= \case
        Just Nothing -> yield Finished
        Nothing -> yield NoMessage >> loop
        Just (Just value) -> yield (MessageItem value) >> loop

------------------------------------------------------------------------------
--- FilterParams
------------------------------------------------------------------------------

data FilterParams = FilterParams { fpMaxItems      :: !Int
                                 , fpIterationTime :: !Int
                                 , fpGroupTime     :: !NominalDiffTime
                                 }

defaultFilterParams :: FilterParams
defaultFilterParams = FilterParams 25 100000 1

------------------------------------------------------------------------------
--- UploadItem
------------------------------------------------------------------------------

data UploadItem = UploadValue !StreamValue
                | UploadMultiple ![StreamValue]
                | UploadStream !Stream

filterChannelC
    :: MonadIO m => FilterParams -> ConduitT ChannelMessage UploadItem m ()
filterChannelC params = start
  where
    start = liftIO getCurrentTime >>= loop mempty

    loop current time = await >>= \case
        Nothing -> yieldPending
        Just message -> processMessage message
      where
        processMessage Finished = yieldPending

        processMessage NoMessage = do
            elapsed <- liftIO $ flip diffUTCTime time <$> getCurrentTime
            if elapsed > fpGroupTime params
                then yieldPending >> start
                else sleep

        processMessage (MessageItem item) = processItem item

        processItem (SingleItem value) = do
            let newItems = current <> [value]
            if length newItems > fpMaxItems params
                then yield $ UploadMultiple newItems
                else loop newItems time

        processItem (OutOfBandItem value) = do
            yieldPending
            yield $ UploadValue value
            start

        processItem (StreamItem stream) = do
            yieldPending
            yield $ UploadStream stream
            start

        sleep = do
            threadDelay $ fpIterationTime params
            loop current time

        yieldPending = unless (null current) $ yield $ UploadMultiple current

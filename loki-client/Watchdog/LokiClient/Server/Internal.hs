module Watchdog.LokiClient.Server.Internal
    ( HasSender(..)
    , askSender
    , senderDatabase
    , senderContext
    ) where

import           Watchdog.LokiClient.Server.Sender.Types

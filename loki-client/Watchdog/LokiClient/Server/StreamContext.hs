module Watchdog.LokiClient.Server.StreamContext
    ( StreamContext
    , HasStreamContext(..)
    , defaultStreamContext
    , streamContextForApp
    , mkStreamContext
    , standardLabels
    , addLabelToContext
    , addMetadataToContext
    , getMetadataFromContext
    , streamValue
    , streamFromValue
    , streamFromValues
    , streamFromText
    , streamValueWithMetadata
    , streamFromValue'
    , streamFromValues'
    ) where

import           Baulig.Prelude

import           Network.BSD

import           Watchdog.Api.Label
import           Watchdog.Api.Stream
import           Watchdog.Api.Timestamp

------------------------------------------------------------------------------
--- StreamContext
------------------------------------------------------------------------------

data StreamContext = StreamContext { scUseTimestamp :: !UseTimestamp
                                   , scMetadata     :: !(Maybe Labels)
                                   , scLabels       :: !Labels
                                   }
    deriving stock (Show, Generic)

mkStreamContext :: UseTimestamp -> Labels -> StreamContext
mkStreamContext = flip StreamContext Nothing

defaultStreamContext :: MonadIO m => m StreamContext
defaultStreamContext = StreamContext CurrentTimestamp Nothing <$> standardLabels

streamContextForApp :: MonadIO m => Text -> m StreamContext
streamContextForApp name =
    StreamContext CurrentTimestamp Nothing <$> standardLabels' name

addLabelToContext :: Label -> StreamContext -> StreamContext
addLabelToContext label ctx = ctx { scLabels = addLabel label $ scLabels ctx }

addMetadataToContext :: Label -> StreamContext -> StreamContext
addMetadataToContext label ctx =
    ctx { scMetadata = Just $ addLabel label $ fromMaybe mempty $ scMetadata ctx
        }

getMetadataFromContext :: StreamContext -> Maybe Labels
getMetadataFromContext = scMetadata

standardLabels :: MonadIO m => m Labels
standardLabels = standardLabels' "loki-client"

standardLabels' :: MonadIO m => Text -> m Labels
standardLabels' app = do
    host <- pack <$> liftIO getHostName
    pure $ Labels $ mapFromList [("app", app), ("host", host)]

------------------------------------------------------------------------------
--- MonadStreamContext
------------------------------------------------------------------------------


streamValue :: (MonadIO m, MonadReader env m, HasStreamContext env)
            => Text
            -> m StreamValue
streamValue text = do
    context <- view streamContextL
    ts <- createTimestamp $ scUseTimestamp context
    pure $ StreamValue ts text $ scMetadata context

streamFromValues :: (MonadIO m, MonadReader env m, HasStreamContext env)
                 => [StreamValue]
                 -> m Stream
streamFromValues input = flip Stream input . scLabels <$> view streamContextL

streamFromValue :: (MonadIO m, MonadReader env m, HasStreamContext env)
                => StreamValue
                -> m Stream
streamFromValue = streamFromValues . singleton

streamFromText :: (MonadIO m, MonadReader env m, HasStreamContext env)
               => [Text]
               -> m Stream
streamFromText input = do
    labels <- scLabels <$> view streamContextL
    Stream labels <$> mapM streamValue input

streamValueWithMetadata :: (MonadIO m, MonadReader env m, HasStreamContext env)
                        => Text
                        -> Labels
                        -> m StreamValue
streamValueWithMetadata text metadata = do
    context <- view streamContextL
    ts <- createTimestamp $ scUseTimestamp context
    let oldMeta = fromMaybe mempty $ scMetadata context
    pure $ StreamValue ts text $ Just $ oldMeta <> metadata

streamFromValues' :: StreamContext -> [StreamValue] -> Stream
streamFromValues' context = Stream $ scLabels context

streamFromValue' :: StreamContext -> StreamValue -> Stream
streamFromValue' context = streamFromValues' context . singleton

------------------------------------------------------------------------------
--- HasStreamContext
------------------------------------------------------------------------------

class HasStreamContext x where
    streamContextL :: Lens' x StreamContext

instance HasStreamContext StreamContext where
    streamContextL = id

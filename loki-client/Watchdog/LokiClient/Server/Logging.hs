module Watchdog.LokiClient.Server.Logging
    ( SenderLogFunc
    , makeContextLogFunc
    , askSenderLogFunc
    , runSenderLogFunc
    , senderLogFuncLog
    ) where

import           Baulig.Prelude

import           Watchdog.Api
import           Watchdog.LokiClient.Server.StreamContext

------------------------------------------------------------------------------
--- Log Function
------------------------------------------------------------------------------

newtype SenderLogFunc =
    SenderLogFunc (Loc -> LogSource -> LogLevel -> LogStr -> IO ())

senderLogFuncLog :: ToLogStr x
                 => SenderLogFunc
                 -> Loc
                 -> LogSource
                 -> LogLevel
                 -> x
                 -> IO ()
senderLogFuncLog (SenderLogFunc func) loc source level =
    func loc source level . toLogStr

runSenderLogFunc :: SenderLogFunc -> LoggingT m a -> m a
runSenderLogFunc (SenderLogFunc logFunc) func = runLoggingT func logFunc

askSenderLogFunc :: MonadLoggerIO m => m SenderLogFunc
askSenderLogFunc = SenderLogFunc <$> askLoggerIO

makeContextLogFunc :: StreamContext -> (StreamValue -> IO ()) -> SenderLogFunc
makeContextLogFunc context func = SenderLogFunc inner
  where
    inner _ source level str =
        func =<< timestampedValueWithMetadata message meta
      where
        message = decodeUtf8 $ fromLogStr str

        severityText = case level of
            LevelDebug -> "debug"
            LevelInfo -> "info"
            LevelWarn -> "warn"
            LevelError -> "error"
            LevelOther other -> other

        levelText = case level of
            LevelDebug -> "unknown"
            LevelInfo -> "trace"
            LevelOther "verbose" -> "debug"
            LevelOther "important" -> "warning"
            LevelOther _ -> "info"
            LevelWarn -> "error"
            LevelError -> "critical"

        meta = senderMeta <> [("level", levelText), ("severity", severityText)]
            <> sourceMeta

        senderMeta = toListOfKeyValue $ fromMaybe mempty
            $ getMetadataFromContext context

        sourceMeta = if null source then mempty else [("source", source)]

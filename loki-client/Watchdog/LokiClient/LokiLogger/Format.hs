module Watchdog.LokiClient.LokiLogger.Format
    ( formatLogMessage
    , formatLokiLogMessage
    ) where

import           Baulig.Prelude

import           Data.Bits
import qualified Data.ByteString.Char8                as B8
import qualified Data.ByteString.Char8                as BS
import           Data.IP

import           Network.HTTP.Types
import           Network.Socket
import           Network.Wai

import           System.Console.ANSI

import           Watchdog.Api
import           Watchdog.LokiClient.LokiLogger.Types
import           Watchdog.Settings.LokiConfig
import           Watchdog.Utils.IPUtils
import           Watchdog.Vault.ZoneDatabase

------------------------------------------------------------------------------
--- Format Loki Message
------------------------------------------------------------------------------

-- | Compute Loki [Label] list from a Network.Wai.Request and Response
lokiLabelsFromRequestAndResponse :: Request -> Response -> [Label]
lokiLabelsFromRequestAndResponse request response =
    requestLabels <> responseLabels
  where
    requestLabels = [ mkLabel "ip" (getSourceIP request)
                    , mkLabel "method" (decodeUtf8 $ requestMethod request)
                    , mkLabel "path" (decodeUtf8 $ rawPathInfo request)
                    ] ++ optionalRequestHeaders

    getSourceIP req =
        case lookup "X-Real-IP" (requestHeaders req) >>= readIPv4 . B8.unpack of
            Just ip -> tshow ip
            Nothing -> case remoteHost req of
                SockAddrInet _ host -> tshow (fromHostAddress host)
                _ -> "unknown"

    optionalRequestHeaders =
        [("user_agent", hUserAgent), ("referer", hReferer)]
        >>= \(label, header) -> case lookup header (requestHeaders request) of
            Just value -> [mkLabel label (decodeUtf8 value)]
            Nothing -> []

    optionalResponseHeaders =
        [("content_type", hContentType), ("content_length", hContentLength)]
        >>= \(label, header) -> case lookup header (responseHeaders response) of
            Just value -> [mkLabel label (decodeUtf8 value)]
            Nothing -> []

    responseLabels = [ mkLabel "status" (tshow status)
                     , mkLabel "status_group" (statusGroup status)
                     ] ++ optionalResponseHeaders

    status = statusCode $ responseStatus response

    -- Categorize status into groups: 2xx, 3xx, 4xx, 5xx
    statusGroup code
        | code >= 200 && code < 300 = "2xx"
        | code >= 300 && code < 400 = "3xx"
        | code >= 400 && code < 500 = "4xx"
        | code >= 500 = "5xx"
        | otherwise = "unknown"

-- | Format log message from WAI Request and Response.
formatLogMessage :: LokiLogger -> Bool -> Request -> Response -> ByteString
formatLogMessage logger useColor req res =
    let ipAddress = formatSourceAddress logger useColor req
        method = formatMethod useColor req
        path = rawPathInfo req <> rawQueryString req
        status = formatStatus useColor res
    in
        BS.concat [method, " ", status, ": ", path, " - ", ipAddress]

-- | Format Loki StreamValue from WAI Request and Response.
formatLokiLogMessage
    :: MonadIO m => LokiLogger -> Request -> Response -> m StreamValue
formatLokiLogMessage logger req res = do
    let logStr = formatLogMessage logger (logger ^. lokiConfigUseColor) req res

        zone = lookupZone logger req

        zoneLabels = maybe mempty lokiLabelsFromLookupResult zone

        requestLabels = lokiLabelsFromRequestAndResponse req res

    timestampedValueWithMetadata' (decodeUtf8 logStr)
        $ requestLabels <> zoneLabels

------------------------------------------------------------------------------
--- Internal
------------------------------------------------------------------------------

formatMethod :: Bool -> Request -> ByteString
formatMethod useColor req
    | useColor = BS.concat $ ansiMethod $ requestMethod req
    | otherwise = requestMethod req

formatStatus :: Bool -> Response -> ByteString
formatStatus useColor res
    | useColor = BS.concat $ ansiStatusCode code code
    | otherwise = code
  where
    code = BS.pack $ show $ statusCode $ responseStatus res

formatSourceAddress :: LokiLogger -> Bool -> Request -> ByteString
formatSourceAddress logger useColor req
    | Just result
      <- lookupZone logger req = ansiFormatLookupResult useColor result
    | otherwise = fromMaybe (formatSockAddr $ remoteHost req)
                            (lookup "X-Real-IP" (requestHeaders req))

lookupZone :: LokiLogger -> Request -> Maybe ZoneLookupResult
lookupZone logger req
    | Just ip <- getSourceIPv4 req = lokiLoggerLookupIP logger ip
    | otherwise = Nothing

getSourceIPv4 :: Request -> Maybe IPv4
getSourceIPv4 req =
    case lookup "X-Real-IP" (requestHeaders req) >>= parseIPv4 of
        Just ip -> Just ip
        Nothing -> case remoteHost req of
            SockAddrInet _ host -> Just $ fromHostAddress host
            _ -> Nothing  -- Ignore non-IPv4 addresses
  where
    -- | Try to parse an IPv4 from ByteString
    parseIPv4 :: ByteString -> Maybe IPv4
    parseIPv4 = readIPv4 . BS.unpack

formatIPv4 :: Word32 -> ByteString
formatIPv4 addr =
    let b1 = (addr `shiftR` 24) .&. 255
        b2 = (addr `shiftR` 16) .&. 255
        b3 = (addr `shiftR` 8) .&. 255
        b4 = addr .&. 255
    in
        BS.pack $ show b1 ++ "." ++ show b2 ++ "." ++ show b3 ++ "." ++ show b4

ntohl :: Word32 -> Word32
ntohl w = (w `shiftR` 24) .&. 0xFF .|. (w `shiftR` 8) .&. 0xFF00
    .|. (w `shiftL` 8) .&. 0xFF0000 .|. (w `shiftL` 24) .&. 0xFF000000

formatSockAddr :: SockAddr -> ByteString
formatSockAddr (SockAddrInet _ hostAddr) = formatIPv4 $ ntohl hostAddr
formatSockAddr (SockAddrInet6 _ _ hostAddr6 _) = BS.pack $ show hostAddr6
formatSockAddr (SockAddrUnix path) = BS.concat ["unix:", BS.pack $ show path]

------------------------------------------------------------------------------
--- Mostly copied from Network.Wai.Middleware.RequestLogger
------------------------------------------------------------------------------

ansiColor :: Color -> ByteString -> [ByteString]
ansiColor color bs = [ BS.pack $ setSGRCode [SetColor Foreground Dull color]
                     , bs
                     , BS.pack $ setSGRCode [Reset]
                     ]

ansiMethod :: ByteString -> [ByteString]
ansiMethod m = case m of
    "GET" -> ansiColor Cyan m
    "HEAD" -> ansiColor Cyan m
    "PUT" -> ansiColor Green m
    "POST" -> ansiColor Yellow m
    "DELETE" -> ansiColor Red m
    _ -> ansiColor Magenta m

ansiStatusCode :: ByteString -> ByteString -> [ByteString]
ansiStatusCode c t = case BS.take 1 c of
    "2" -> ansiColor Green t
    "3" -> ansiColor Yellow t
    "4" -> ansiColor Red t
    "5" -> ansiColor Magenta t
    _ -> ansiColor Blue t

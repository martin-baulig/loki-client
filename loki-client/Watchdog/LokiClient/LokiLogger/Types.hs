module Watchdog.LokiClient.LokiLogger.Types
    ( LokiLoggerConfig(..)
    , HasLokiLoggerConfig(..)
    , LokiLogger(..)
    , lokiLoggerSendLogMessage
    , lokiLoggerSendStreamValue
    , lokiLoggerLookupIP
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.IP

import           Network.Wai

import           Baulig.Utils.PrettyPrint

import           Watchdog.Api
import           Watchdog.LokiClient.Server.Logging
import           Watchdog.LokiClient.Server.Sender.Internal
import           Watchdog.LokiClient.Server.Sender.Types
import           Watchdog.LokiClient.Server.StreamContext
import           Watchdog.Settings.LokiConfig
import           Watchdog.Vault.Metrics
import           Watchdog.Vault.ZoneDatabase

------------------------------------------------------------------------------
--- LokiLoggerConfig
------------------------------------------------------------------------------

data LokiLoggerConfig =
    LokiLoggerConfig { llcLokiConfig     :: !LokiConfig
                     , llcColorConfig    :: !ColorConfig
                     , llcDatabase       :: !SenderDatabasePath
                     , llcLabels         :: ![MetricLabel]
                     , llcZoneConfigFile :: !(Maybe FilePath)
                     }
    deriving stock (Generic)

instance HasLokiConfig LokiLoggerConfig where
    lokiConfigL = lens llcLokiConfig (\x y -> x { llcLokiConfig = y })

instance HasColorConfig LokiLoggerConfig where
    colorConfigL = lens llcColorConfig (\x y -> x { llcColorConfig = y })

instance FromJSON LokiLoggerConfig where
    parseJSON = withObject "LokiLoggerConfig" $ \o -> do
        llcLokiConfig <- o .: "loki-config"
        llcDatabase <- o .:? "database-path"
            .!= (llcLokiConfig ^. lokiConfigDatabasePath)

        llcZoneConfigFile <- o .:? "zone-config-file"

        pure LokiLoggerConfig { llcColorConfig = def, llcLabels = mempty, .. }

------------------------------------------------------------------------------
--- HasLokiLoggerConfig
------------------------------------------------------------------------------

class HasLokiLoggerConfig x where
    lokiLoggerConfigL :: SimpleGetter x LokiLoggerConfig

instance HasLokiLoggerConfig LokiLoggerConfig where
    lokiLoggerConfigL = id

------------------------------------------------------------------------------
--- LokiLogger
------------------------------------------------------------------------------

data LokiLogger = LokiLogger { llConfig       :: !LokiLoggerConfig
                             , llContext      :: !StreamContext
                             , llLogOptions   :: !LogOptions
                             , llZoneDatabase :: !(Maybe PureZoneDatabase)
                             , llSender       :: !Sender
                             }

instance HasLokiLoggerConfig LokiLogger where
    lokiLoggerConfigL = to llConfig

instance HasLokiConfig LokiLogger where
    lokiConfigL = lens llConfig (\x y -> x { llConfig = y }) . lokiConfigL

instance HasLogOptions LokiLogger where
    logOptionsL = lens llLogOptions (\x y -> x { llLogOptions = y })

instance HasStreamContext LokiLogger where
    streamContextL = lens llContext (\x y -> x { llContext = y })

lokiLoggerSendLogMessage
    :: LokiLogger -> Loc -> LogSource -> LogLevel -> LogStr -> IO ()
lokiLoggerSendLogMessage = senderLogFuncLog . senderLogFunc . llSender

lokiLoggerSendStreamValue
    :: MonadIO m => LokiLogger -> Request -> StreamValue -> m ()
lokiLoggerSendStreamValue logger request =
    sendMessageWithStreamContext (llSender logger) hostContext
  where
    requestContext = addLabelToContext (mkLabel "service_name" "wai-request")
        $ llContext logger

    hostContext
        | Just host <- lookup "Host" (requestHeaders request) =
            addLabelToContext (mkLabel "target_host" $ decodeUtf8 host)
                              requestContext
        | otherwise = requestContext

lokiLoggerLookupIP :: LokiLogger -> IPv4 -> Maybe ZoneLookupResult
lokiLoggerLookupIP (llZoneDatabase -> db) = mapM lookupIPFromDatabase db


{-# LANGUAGE QuasiQuotes #-}

module Watchdog.LokiClient.Utils.LoremIpsum
    ( -- Conduit
      generateWordC
    , generateSentenceC
    , generateParagraphC
      -- List
    , generateSentence
    , generateParagraphs
    ) where

import           Baulig.Prelude

import           Data.Char         (isPunctuation)
import           Data.List         ((!!))
import qualified Data.Text         as T

import qualified System.Random     as R

import           Text.RawString.QQ

------------------------------------------------------------------------------
--- LoremIpsum
------------------------------------------------------------------------------

loremText :: [Text]
loremText = T.words [r|
Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium
doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore
veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim
ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia
consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque
porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur
adipiscing velit, sed quia non numquam do eius modi tempora incididunt, ut
labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam,
quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid
ex ea commodi consequatur? Quis autem vel eum irure reprehenderit, qui in ea
voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem
eum fugiat, quo voluptas nulla pariatur?
|]

generateWordC :: MonadIO m => ConduitM () Text m ()
generateWordC = forever $ do
    idx <- R.randomRIO (0, length loremText - 1)
    yield $ loremText !! idx

foldSentenceC :: Monad m => ConduitM Text o m Text
foldSentenceC = foldlC foldSentence ""
  where
    foldSentence "" word = T.toTitle word
    foldSentence current word
        | last' == "?" || last' == "." || last' == "!" =
            current <> " " <> T.toTitle word
      where
        last' = T.takeEnd 1 current

    foldSentence current word = current <> " " <> word

generateSentenceC :: MonadIO m => ConduitM () Text m ()
generateSentenceC = forever $ do
    count <- R.randomRIO (12, 19)
    sentence <- generateWordC .| takeC count .| foldSentenceC
    case T.takeEnd 1 sentence of
        "?" -> yield sentence
        "." -> yield sentence
        "!" -> yield sentence
        _ -> yield $ T.dropWhileEnd isPunctuation sentence <> "."

generateSentence :: MonadIO m => m Text
generateSentence = runConduit $ generateSentenceC .| takeC 1 .| foldSentenceC

foldParagraphC :: MonadIO m => ConduitM Text o m Text
foldParagraphC = foldlC foldParagraph ""
  where
    foldParagraph "" sentence = sentence
    foldParagraph current sentence = current <> " " <> sentence

generateParagraphC :: MonadIO m => ConduitM () Text m ()
generateParagraphC = forever $ do
    count <- R.randomRIO (3, 7)
    yield =<< generateSentenceC .| takeC count .| foldParagraphC

generateParagraphs :: MonadIO m => Int -> m [Text]
generateParagraphs count =
    runConduit $ generateParagraphC .| takeC count .| sinkList


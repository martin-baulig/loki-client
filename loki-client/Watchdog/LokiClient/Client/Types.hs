module Watchdog.LokiClient.Client.Types (ApiPath(..)) where

import           Baulig.Prelude

------------------------------------------------------------------------------
--- ApiPath
------------------------------------------------------------------------------

newtype ApiPath = ApiPath Text
    deriving stock (Eq, Generic, Show)
    deriving newtype IsString

instance Display ApiPath where
    display (ApiPath path) = path

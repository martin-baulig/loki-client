module Watchdog.LokiClient.Client.Exception
    ( ClientException(..)
    , checkResponseException
    , throwUnexpectedStatus
    ) where

import           Baulig.Prelude

import           Network.HTTP.Client
import           Network.HTTP.Types

------------------------------------------------------------------------------
--- ClientException
------------------------------------------------------------------------------

data ClientException = Timeout
                     | ConnectionException !HttpExceptionContent
                     | UnexpectedStatusCode !Status
                     | InternalError !Text
                     | OtherException !SomeException
    deriving stock (Show)

instance Exception ClientException

checkResponseException :: (MonadIO m, MonadLogger m)
                       => Either SomeException (Response x)
                       -> m (Response x)
checkResponseException eitherResponse
    | Right response <- eitherResponse = pure response
    | Left (SomeException exc) <- eitherResponse = do
        $logError $ "Got exception: " <> tshow exc
        throwIO $ case cast exc of
            Just (HttpExceptionRequest _ ConnectionTimeout) -> Timeout
            Just (HttpExceptionRequest _ content) -> ConnectionException content
            Just (InvalidUrlException url reason) -> InternalError
                $ "Invalid URL: " <> tshow url <> " " <> tshow reason
            Nothing -> OtherException $ SomeException exc

throwUnexpectedStatus
    :: (MonadIO m, MonadLogger m) => Response LazyByteString -> m Void
throwUnexpectedStatus response = do
    let status = responseStatus response

    let body = responseBody response

    $logError $ "Got unexpected HTTP Status Code " <> tshow status <> "\n"
        <> decodeUtf8 (toStrict body)

    throwIO $ UnexpectedStatusCode status

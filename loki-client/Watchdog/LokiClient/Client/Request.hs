module Watchdog.LokiClient.Client.Request
    ( ApiPath
    , displayResponse
    , performSimpleRequest
    , performApiRequest
    , performRequestWithNoResult
    , withWebSocketRequestManager
    , module Watchdog.LokiClient.Client.Exception
    ) where

import           Baulig.Prelude

import           Data.Aeson
import           Data.CaseInsensitive

import qualified Network.HTTP.Client                  as Http
import qualified Network.HTTP.Client.Internal         as Http
import           Network.HTTP.Simple                  hiding (Proxy)
import           Network.HTTP.Types.Status
import qualified Network.WebSockets                   as WS
import qualified Network.WebSockets.Stream            as WS

import           Watchdog.Api
import           Watchdog.LokiClient.Client.Exception
import           Watchdog.LokiClient.Client.Types

------------------------------------------------------------------------------
--- Client
------------------------------------------------------------------------------


displayResponse :: Response x -> Text
displayResponse response =
    "Response " <> tshow status <> " {" <> displayHeaders <> "\n}"
  where
    status = getResponseStatusCode response

    headers = getResponseHeaders response

    displayHeaders = foldl' displayHeader mempty headers

    displayHeader current (original -> header, value) =
        current <> "\n  " <> decodeUtf8 header <> " = " <> decodeUtf8 value

performSimpleRequest :: (MonadIO m, MonadLogger m) => Request -> m Text
performSimpleRequest request = do
    logInfoN $ "Simple request: " <> tshow request

    res <- liftIO $ httpBS request
    let text = decodeUtf8 $ getResponseBody res
    logInfoN $ displayResponse res
    logInfoN $ "Response body:\n" <> text
    pure text

performHttpRequest :: (MonadUnliftIO m, MonadLogger m)
                   => Request
                   -> m (Http.Response LazyByteString)
performHttpRequest request = checkResponseException
    =<< tryAny (liftIO $ httpLBS request)

performApiRequest :: (MonadUnliftIO m, MonadLogger m, FromJSON x, Show x)
                  => Proxy x
                  -> Request
                  -> m x
performApiRequest proxy request = do
    logInfoN $ "API request: " <> tshow request

    response <- performHttpRequest request

    logInfoN $ displayResponse response

    let body = getResponseBody response

    logInfoN $ "Body:\n" <> decodeUtf8 (toStrict body)

    let result = decodeApiResponse proxy body

    logInfoN $ "Result:\n" <> tshow result

    pure result

performRequestWithNoResult
    :: (MonadUnliftIO m, MonadLogger m) => Request -> m ()
performRequestWithNoResult request = do
    logDebugN $ "API request: " <> tshow request

    response <- performHttpRequest request

    logDebugN $ displayResponse response

    unless (Http.responseStatus response == status204)
        $ throwUnexpectedStatus response >>= absurd

webSocketHandler :: (MonadIO m, MonadLogger m)
                 => WS.Connection
                 -> ConduitM i LazyByteString m ()
webSocketHandler connection = loop
  where
    loop = do
        logDebugN "WebSocket handler."
        message <- liftIO $ WS.receiveData connection
        logDebugN $ "Got message: " <> tshow message
        yield message
        loop

withWebSocketRequestManager
    :: (MonadUnliftIO m, MonadThrow m, MonadLogger m, FromJSON x)
    => Http.Manager
    -> Request
    -> ConduitM x Void m ()
    -> m ()
withWebSocketRequestManager manager request sink = do
    withRunInIO $ \io -> do
        Http.withConnection request manager $ \connection -> do
            bracket (makeStream connection) WS.close (runStream io)
  where
    makeStream connection = WS.makeStream input output
      where
        input = Http.connectionRead connection
            >>= \bs -> pure $ if null bs then Nothing else Just bs

        output = maybe (Http.connectionClose connection)
                       (Http.connectionWrite connection . toStrict)

    bs2str = unpack . decodeUtf8

    host = bs2str $ Http.host request

    path = bs2str $ Http.path request <> Http.queryString request

    headers = Http.requestHeaders request

    runStream io stream = do
        io $ logInfoN "Got WebSocket stream."
        WS.runClientWithStream stream
                               host
                               path
                               WS.defaultConnectionOptions
                               headers
                               (io . clientApp)

    clientApp connection =
        runConduit $ webSocketHandler connection .| mapMC throwDecode .| sink
